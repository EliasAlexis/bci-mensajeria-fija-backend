from webhooks.models import Hooks, Headers, Params, Body
from work_orders.models import WorkOrdersSteps


HOOKS = Hooks.objects.create(
    url = "https://qa-api-supply-chain.falabella.com/trmg/api/v1/legal-delivery-orders/",
    action = "get",
    type_format = "json",
    wo_step =  WorkOrdersSteps.objects.get(pk=8)
    )

HEADERS = Headers.objects.create(
    name ="X-apiKey",
    value = "123",
    hooks =HOOKS
    )

HEADERS = Headers.objects.create(
    name ="User-Agent",
    value = "OPL/1.0.0",
    hooks =HOOKS
    )

HEADERS = Headers.objects.create(
    name ="X-country",
    value = "CL",
    hooks =HOOKS
    )

HEADERS = Headers.objects.create(
    name ="X-chRef",
    value = "OPL",
    hooks =HOOKS
    )

HEADERS = Headers.objects.create(
    name ="X-commerce",
    value = "Falabella",
    hooks =HOOKS
    )


PARAMS = Params.objects.create(
    name ="document",
    value = "140000125661",
    hooks =HOOKS
    )

BODY = Body.objects.create(
    name ="a",
    value = "a",
    default = False
    )



