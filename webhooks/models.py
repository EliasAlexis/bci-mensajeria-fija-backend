from django.db import models
from work_orders.models import WorkOrdersSteps

ACTION_CHOICE = (
    ('GET', 'GET'),
    ('POST', 'POST'),
    ('PATCH', 'PATCH'),
    ('SR', 'Senior'),
)

TYPE_F_CHOICE = (
    ('XML', 'XML'),
    ('JSON', 'JSON'),
)

class Hooks(models.Model):
    url = models.URLField()
    action = models.CharField(max_length=10, choices=ACTION_CHOICE)
    type_format = models.CharField(max_length=10, choices=TYPE_F_CHOICE)
    wo_step = models.ForeignKey(
        WorkOrdersSteps,
        on_delete=models.CASCADE,
        related_name="hooks_step"
    )

class Headers(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    hooks = models.ForeignKey(
        Hooks,
        on_delete=models.CASCADE,
        related_name="hooks_headers"
    )

class Params(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    hooks = models.ForeignKey(
        Hooks,
        on_delete=models.CASCADE,
        related_name="hooks_params"
    )

class Body(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    default = models.BooleanField()
    hooks = models.ForeignKey(
        Hooks,
        on_delete=models.CASCADE,
        related_name="hooks_body"
    )