from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from channels.routing import ProtocolTypeRouter, URLRouter
# websocket
from .socketJWTMiddleware import SocketJWTMiddleware
from work_orders.urls import websocket_urlpatterns

pchannels = ProtocolTypeRouter(
   {
       "websocket": SocketJWTMiddleware(
           URLRouter(websocket_urlpatterns)
       )
   }
)

urlpatterns = [
    path('', include('management.urls', namespace='management')),
    path('', include('servicing.urls', namespace='servicing')),
    path('', include('work_orders.urls', namespace='work_orders')),
    path('', include('billing.urls', namespace='billing')),
    # path('', include('falabella.urls', namespace='falabella')),
    path('falabella/', include('falabella.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
