from __future__ import absolute_import
import asyncio
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .asyncCelery import app as celery_app

if hasattr(asyncio, 'WindowsSelectorEventLoopPolicy'):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
