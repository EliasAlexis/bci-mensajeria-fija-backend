from rest_framework import serializers

from ..models import Transports

class TransportSerializer(serializers.ModelSerializer):
    type_display = serializers.SerializerMethodField()
    available_display = serializers.SerializerMethodField()

    class Meta:
        model = Transports
        fields = '__all__'

    def get_type_display(self, instance):
        return instance.get_type_display()

    def get_available_display(self, instance):
        return instance.get_available_display()
