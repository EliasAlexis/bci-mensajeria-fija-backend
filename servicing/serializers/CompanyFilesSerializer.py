# django
from django.db import transaction

# rest_frameword
from rest_framework import serializers

# models
from ..models import CompanyFiles, CompanyFilesLog
from management.models import TradersCompanies, User
from work_orders.models import AutoComplete

# serializers
from .PricingSerializers import PricingSerializer
from management.serializers.CompaniesSerializers import CompanyAdminSerializer, BranchesSerializer
from management.serializers.UserSerializer import UserSerializer

# jsonchema
from jsonschema import validate
from jsonschema.exceptions import ValidationError

#etc
from utils.ExtraFields import JSONSerializerField
import json
from utils.Utils import send_email_async

class CompanyFilesSerializer(serializers.ModelSerializer):
    contact_last_name = serializers.CharField(required=True)
    contact_email = serializers.CharField(required=True)
    commercial_business = serializers.CharField(required=True)
    log = serializers.SerializerMethodField()
    extra_fields = JSONSerializerField(required=True)

    class Meta:
        model = CompanyFiles
        fields = '__all__'

    FINAL_STATUSES = (
        'accepted',
    )

    def name_lower(self, el):
        el["name"] = el["name"].lower()
        return el

    def validate_extra_fields(self, value):
        schema = {
                "type": "array",
                "maxItems": 3,
                "items": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string",
                            "maxLength": 20
                        },
                        "type": {
                            "type": "string",
                            "enum": ["text", "date", "number"]
                        },
                        "additionalProperties": False
                    }
                }
            }
        try:
            validate(
                instance=value,
                schema=schema
            )
        except ValidationError:
            raise serializers.ValidationError("Formato de campos extra incorrectos")
        return value

    def validate_file_pricings(self, value):
        pricings = PricingSerializer(data=value, many=True)
        pricings.is_valid(raise_exception=True)
        return value

    # def validate_status(self, value):
    #     if self.instance is not None:
    #         if self.instance.status in self.FINAL_STATUSES:
    #             raise serializers.ValidationError(
    #                 ["No es posible modificar la ficha de cliente"]
    #             )
    #     return value

    def get_log(self, instance):
        log = CompanyFilesLog.objects.filter(company_file=instance.pk).order_by("stamp")
        return CompanyFilesLogSerializer(
            log,
            many=True,
            context={'request': self.context['request']}
            ).data

    def to_representation(self, instance):
        company_file = super().to_representation(instance)
        user_representation = {
            'first_name' : instance.owner.first_name,
            'last_name' : instance.owner.last_name
        }
        company_file["owner"] = user_representation
        return company_file


class CompanyFilesLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyFilesLog
        fields = '__all__'

    def to_representation(self, instance):
        log = super().to_representation(instance)
        log["stamp"] = instance.stamp.strftime("%d/%m/%Y %H:%M")
        log["user"] = {
            'first_name' : instance.user.first_name,
            'last_name' : instance.user.last_name
        }
        return log


class TraderCompanyFilesSerializer(CompanyFilesSerializer):

    class Meta:
        model = CompanyFiles
        fields = '__all__'
        extra_kwargs = {
            "owner": { "required": False}
        }

    def validate_status(self, value):
        ALLOWED_STATUSES = (
            'draft',
            'rejected'
        )
        if self.instance:
            if not self.instance.status in ALLOWED_STATUSES:
                return ValidationError("No es posible modificar la ficha de cliente")
        return value

    def update(self, instance, validated_data):
        ALLOWED_FIELDS = (
            'company_name',
            'commercial_business',
            'address',
            'phone',
            'contact_first_name',
            'contact_last_name',
            'contact_email',
            'comune',
            'province',
            'region',
            'lat',
            'lng',
            'billing_contacts',
            'file_pricings',
            'extra_fields',
            'status'
            )
        for attr in ALLOWED_FIELDS:
            setattr(instance, attr, validated_data.get(attr, getattr(instance, attr)))
        instance.save()
        if instance.status == "sent":
            update_log = {
                "company_file": instance.id,
                "user": self.context.get('user').pk,
                "status": instance.status
            }
            update_serializer = CompanyFilesLogSerializer(data=update_log)
            update_serializer.is_valid(raise_exception=True)
            update_serializer.save()
            file_data = self.to_representation(instance)
            file_data['status'] = instance.get_status_display()
            to = [user.email for user in User.objects.filter(is_staff=True)]
            send_email_async.delay(
                template='file_status.html',
                subject='Ficha cliente {}, {}'.format(instance.commercial_business, instance.get_status_display()),
                to=to,
                email_data={
                    'trader': UserSerializer(instance.owner).data,
                    'file': file_data,
                    'type': 'trader',
                }
            )
        return instance

class AdminCompanyFilesSerializer(CompanyFilesSerializer):

    # def __init__(self, *args, **kwargs):
    #     super(AdminCompanyFilesSerializer, self).__init__(*args, **kwargs)
    #     for field in self.fields:
    #         if field not in self.WRITABLE_FIELDS:
    #             self.fields[field].read_only = True

    class Meta:
        model = CompanyFiles
        fields = '__all__'

    def generate_company(self, instance):
        company = {
            "name": instance.company_name,
            "rut": instance.rut,
            "company_type": "client",
            "max_users": 0,
            "max_branches": 0,
            "status": "active",
            "billing_contacts": instance.billing_contacts,
            "commercial_business": instance.commercial_business,
            "contact_name": "{} {}".format(instance.contact_first_name, instance.contact_last_name),
            "email": instance.contact_email,
            "phone": instance.phone,
            "extra_fields": [{"name": el["name"].lower(), "type":el["type"]} for el in instance.extra_fields],
            "company_price": instance.file_pricings,
        }
        companySerializer = CompanyAdminSerializer(data=company)
        companySerializer.is_valid(raise_exception=True)
        companySerializer.save()

        # pricings = []
        # for price in instance.file_pricings:
        #     price["company"] = companySerializer.data["id"]
        #     pricings.append(price)
        # pricingsSerializer = PricingSerializer(data=pricings, many=True)
        # pricingsSerializer.is_valid(raise_exception=True)
        # pricingsSerializer.save()

        default_branch = {
            "name": "Casa Matriz",
            "address": instance.address,
            "address_number": instance.address_number,
            "lat": instance.lat,
            "lng": instance.lng,
            "phone": instance.phone,
            "contact_name": f"{instance.contact_first_name} {instance.contact_last_name}",
            "email": instance.contact_email,
            "comune": instance.comune,
            "province": instance.province,
            "region": instance.region,
            "max_users": 0,
            "company": companySerializer.data["id"]
        }
        branchSerializer = BranchesSerializer(data=default_branch)
        branchSerializer.is_valid(raise_exception=True)
        branchSerializer.save()

        first_user = {
            "username": instance.contact_email,
            "first_name": instance.contact_first_name,
            "last_name": instance.contact_last_name,
            "status": "disabled",
            "email": instance.contact_email,
            "branch": branchSerializer.data["id"],
            "group": "company_admins"
        }
        user = UserSerializer(data=first_user, context={"user" : self.context.get("request").user})
        user.is_valid(raise_exception=True)
        user.save()

        request = self.context.get('request', None)
        update_log = {
            "company_file": instance.id,
            "user": request.user.pk,
            "status": instance.status
        }
        update_serializer = CompanyFilesLogSerializer(data=update_log)
        update_serializer.is_valid(raise_exception=True)
        update_serializer.save()
        TradersCompanies.objects.create(
                trader=instance.owner,
                company=companySerializer.instance
            )
        autocomplete_obj = {
            "full_name" : instance.commercial_business,
            "rut": instance.rut,
            "email" : instance.contact_email,
            "address" : instance.address,
            "phone": instance.phone,
            "lat" : instance.lat,
            "lng" : instance.lng,
            "comune" : instance.comune,
            "region" : instance.region,
            "companysearch" : companySerializer.instance
        }
        AutoComplete.objects.create(**autocomplete_obj)

    @transaction.atomic
    def create(self, validated_data):
        validated_data['status'] = "accepted"
        instance = super().create(validated_data)
        self.generate_company(instance)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        # print(validated_data)
        do_generate = False
        inform_by_mail = False
        if validated_data.get("status") == "accepted" and instance.status == "sent":
            do_generate = True
        if validated_data.get("status") in ['accepted', 'rejected'] and instance.status == "sent":
            inform_by_mail = True
        super().update(instance, validated_data)
        # for attr in self.WRITABLE_FIELDS:
        #     setattr(instance, attr, validated_data.get(attr))
        # instance.save()
        if do_generate:
            self.generate_company(instance)
        if inform_by_mail:
            file_data = self.to_representation(instance)
            file_data['status'] = instance.get_status_display()
            send_email_async.delay(
                template='file_status.html',
                subject='Ficha cliente {}, {}'.format(instance.commercial_business, instance.get_status_display()),
                to=[instance.owner.email,],
                email_data={
                    'trader': UserSerializer(instance.owner).data,
                    'file': file_data,
                    'admin': UserSerializer(self.context.get("request").user).data,
                    'type': 'admin',
                }
            )
        return instance
