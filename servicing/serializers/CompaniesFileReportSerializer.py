# rest_framework
from rest_framework import serializers

# models
from ..models import CompanyFiles, CompanyFilesLog

class CompaniesReportSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()
    class Meta:
        model = CompanyFiles
        fields = (
            'created_at',
            'company_name',
            'commercial_business',
            'owner',
            'get_status_display',
            )  

    def get_created_at(self, obj):
        return CompanyFilesLog.objects.filter(company_file=obj).latest('stamp').stamp

    def get_owner(self, obj):
        return obj.owner.first_name +' '+ obj.owner.last_name
    
    
  


    

