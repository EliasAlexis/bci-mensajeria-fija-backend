from servicing.serializers.PricingSerializers import PricingSerializer

PRICES = [
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 1,
            "service": 1
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 1,
            "service": 2
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 1,
            "service": 3
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 2,
            "service": 1
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 2,
            "service": 2
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 2,
            "service": 3
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 3,
            "service": 1
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 3,
            "service": 2
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 3,
            "service": 3
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 4,
            "service": 1
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 4,
            "service": 2
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 4,
            "service": 3
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 5,
            "service": 1
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 5,
            "service": 2
        },
        {
            "default": "yes",
            "no_return": 10,
            "with_return": 20,
            "min_quantity": 10,
            "segment": 5,
            "service": 3
	    }
]

SERIALIZER = PricingSerializer(data=PRICES, many=True)
SERIALIZER.is_valid()
SERIALIZER.save()

print("Default prices created")
