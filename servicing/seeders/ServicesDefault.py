from servicing.models import Services, Segments, Products

seg0 = Segments(
    width=30,
    depth=16,
    height=10,
    name='Tramo 0',
    min_weight=0,
    max_weight=3,
    default='yes'
)
seg0.save()
seg1 = Segments(
    width=60,
    depth=32,
    height=20,
    name='Tramo 1',
    min_weight=3.10,
    max_weight=10,
    default='yes'
)
seg1.save()
seg2 = Segments(
    width=80,
    depth=80,
    height=80,
    name='Tramo 2',
    min_weight=10.1,
    max_weight=15,
    default='yes'
)
seg2.save()
seg3 = Segments(
    width=100,
    depth=100,
    height=100,
    name='Tramo 3',
    min_weight=15.1,
    max_weight=25,
    default='yes'
)
seg3.save()
seg4=Segments(
    width=200,
    depth=200,
    height=200,
    name='Tramo 4',
    min_weight=25.1,
    max_weight=999,
    default='yes'
)
seg4.save()
print('Segments created')

amPm = Services(
    name='AM/PM',
    status='active',
    default='yes'
)
amPm.save()
dSig = Services(
    name='Día Hábil Siguiente',
    status='active',
    default='yes'
)
dSig.save()
max4 = Services(
    name='Max. 4 Horas',
    status='active',
    default='yes'
)
max4.save()

print('Services created')

Products.objects.create(
    name='Cartas',
    status='active',
    default='yes'
)

Products.objects.create(
    name='Cajas',
    status='active',
    default='yes'
)
print('Products created')
