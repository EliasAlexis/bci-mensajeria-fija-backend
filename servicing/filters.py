from django.db.models import Q
from django_filters import rest_framework as filters
from management.models import Companies
from .models import Pricings, Services, Segments, FailedReasons

def filter_company_default(queryset, name, value):
    return queryset.filter(Q(**{name:value}) | Q(default='yes'))

class PricingFilters(filters.FilterSet):
    company_default = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        method=filter_company_default,
        field_name='company'
    )


    class Meta:
        model = Pricings
        fields = (
            'company',
            'default',
            'company_default',
        )

class ServiceFilters(filters.FilterSet):
    company_default = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        method=filter_company_default,
        field_name='company'
    )

    class Meta:
        model = Services
        fields = (
            'name',
            'status',
            'default',
            'company',
            'company_default',
        )

class SegmentFilters(filters.FilterSet):
    company_default = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        method=filter_company_default,
        field_name='company'
    )

    class Meta:
        model = Segments
        fields = (
            'name',
            'default',
            'company',
            'company_default',
        )

class ReasonsFilters(filters.FilterSet):
    company = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        method='filter_reasons',
        field_name='failed_reason_rel_reason__company'
    )

    def filter_reasons(self, queryset, name, value):
        return FailedReasons.objects.filter(Q(**{name:value}) | Q(default='yes'))

    class Meta:
        model = FailedReasons
        fields = (
            'company',
        )
