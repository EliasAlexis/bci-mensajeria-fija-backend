# base
from django.urls import path, include
from rest_framework_nested import routers

# viewsets
from .views.PricingViews import(
    SegmentsViewSet,
    ServicesViewSet,
    PricingsViewSet,
    ProductsViewSet,
    )
from .views.CompanyFilesVieset import CompanyFilesVieset
from .views.ExtraFieldsView import ExtraFieldsView
from .views.CompanieFilesReportsViews import CompaniesFileReports
from .views.TransportsViewSet import TransportsViewSet
from .views.ReasonsView import ReasonsViewSet

app_name = 'servicing'

SETTINGS_ROUTER = routers.SimpleRouter()
SETTINGS_ROUTER.register('segments', SegmentsViewSet, base_name='segments')
SETTINGS_ROUTER.register('services', ServicesViewSet, base_name='services')
SETTINGS_ROUTER.register('pricings', PricingsViewSet, base_name='pricings')
SETTINGS_ROUTER.register('products', ProductsViewSet, base_name='products')
SETTINGS_ROUTER.register('transports', TransportsViewSet, base_name='transports')
SETTINGS_ROUTER.register('reasons', ReasonsViewSet, base_name='reasons')

ROUTER = routers.SimpleRouter()
ROUTER.register('company_files', CompanyFilesVieset, base_name='company_files')

urlpatterns = [
    path('settings/reports/', CompaniesFileReports.as_view(), name='companies_file_report'),
    path('settings/', include(SETTINGS_ROUTER.urls)),
    path('settings/extra_fields/', ExtraFieldsView.as_view(), name="WO_extra_fields"),
    path('', include(ROUTER.urls)),
]