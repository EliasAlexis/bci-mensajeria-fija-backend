from django.db import models
from management.models import Companies, User
from django.contrib.postgres.fields import JSONField

class Products(models.Model):
    def save(self, *args, **kwargs):
        if self.company is not None and self.default == 'yes':
            raise ValueError("No es posible generar unproducto por defecto, asociado a un cliente")
        super(Products, self).save(*args, **kwargs)
            
    DEFAULT_CHOICES = (
        ('yes', 'Si'),
        ('no', 'no'),
    )
    PRODUCT_STATUS = (
        ('active', 'Activo'),
        ('disabled', 'Desactivado'),
    )
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=20, choices=PRODUCT_STATUS)
    default = models.CharField(max_length=10, choices=DEFAULT_CHOICES, default='no')
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        null=True
    )
    class Meta:
        db_table = 'products'

class Services(models.Model):
    def save(self, *args, **kwargs):
        if self.company is not None and self.default == 'yes':
            raise ValueError("No es posible generar un producto por defecto, asociado a un cliente")
        super(Services, self).save(*args, **kwargs)
            
    DEFAULT_CHOICES = (
        ('yes', 'Si'),
        ('no', 'no'),
    )
    SERVICE_STATUS = (
        ('active', 'Activo'),
        ('disabled', 'Desactivado'),
    )
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=20, choices=SERVICE_STATUS)
    default = models.CharField(max_length=10, choices=DEFAULT_CHOICES, default='no')
    details = models.CharField(max_length=200, null=True, blank=True)
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        null=True
    )
    class Meta:
        db_table = 'services'

class Segments(models.Model):
    def save(self, *args, **kwargs):
        if self.company is not None and self.default == 'yes':
            raise ValueError("No es posible generar unproducto por defecto, asociado a un cliente")
        super(Segments, self).save(*args, **kwargs)
            
    DEFAULT_CHOICES = (
        ('yes', 'Si'),
        ('no', 'no'),
    )
    name = models.CharField(max_length=100)
    height = models.PositiveSmallIntegerField(default=1)
    width = models.PositiveSmallIntegerField(default=1)
    depth = models.PositiveSmallIntegerField(default=1)
    min_weight = models.DecimalField(default=0.10, max_digits=5, decimal_places=2)
    max_weight = models.DecimalField(default=0.10, max_digits=5, decimal_places=2)
    default = models.CharField(max_length=10, choices=DEFAULT_CHOICES, default='yes')
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        null=True
    )
    class Meta:
        db_table = 'segments'

class Pricings(models.Model):
    PRICING_DEFAULT = (
        ('yes', 'Si'),
        ('no', 'No')
    )
    segment = models.ForeignKey(
        Segments,
        on_delete=models.CASCADE,
        related_name="segment_price"
    )
    service = models.ForeignKey(
        Services,
        on_delete=models.CASCADE,
        related_name="service_price"
    )
    default = models.CharField(max_length=20, choices=PRICING_DEFAULT, default='no')
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        related_name="company_price",
        null=True,
        blank=True
    )
    no_return = models.PositiveIntegerField(default=0)
    with_return = models.PositiveIntegerField(default=0)
    min_quantity = models.PositiveSmallIntegerField(default=0)
    class Meta:
        db_table = 'pricings'

class FailedReasons(models.Model):
    DEFAULT_CHOICES = (
        ('yes', 'Si'),
        ('no', 'No'),
    )
    reason = models.CharField(max_length=20)
    default = models.CharField(max_length=20, choices=DEFAULT_CHOICES)

    class Meta:
        db_table = 'servicing_failed_reasons'

class FailedReasonsCompany(models.Model):
    failed_reason = models.ForeignKey(
        FailedReasons,
        on_delete=models.CASCADE,
        related_name='failed_reason_rel_reason'
    )
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        related_name='failed_reason_rel_company'
    )

    class Meta:
        db_table = 'servicing_failed_reasons_company'

class Transports(models.Model):
    TRANSPORT_TYPES = (
        ('moto', 'Moto'),
        ('van', 'Furgón'),
        ('truck', 'Camión'),
    )
    AVAILABILITY = (
        ('yes', 'Si'),
        ('no', 'No'),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    plate = models.CharField(max_length=15, unique=True)
    type = models.CharField(max_length=15, choices=TRANSPORT_TYPES)
    available = models.CharField(max_length=15, choices=AVAILABILITY, default='yes')

    class Meta:
        db_table = 'transports'

class CompanyFiles(models.Model):
    FILE_STATUS = (
        ('draft', 'Borrador'),
        ('sent', 'Enviada'),
        ('rejected', 'Rechazada'),
        ('accepted', 'Aprobada'),
    )
    company_name = models.CharField(max_length=100)
    commercial_business = models.CharField(max_length=100, null=True)
    rut = models.CharField(max_length=15, unique=True)
    address = models.CharField(max_length=150)
    address_number = models.CharField(max_length=150, null=True, blank=True)
    lat = models.DecimalField(max_digits=10, decimal_places=7)
    lng = models.DecimalField(max_digits=10, decimal_places=7)
    phone = models.CharField(max_length=100, null=True, blank=True, default=None)
    contact_first_name = models.CharField(max_length=150)
    contact_last_name = models.CharField(max_length=150, null=True)
    contact_email = models.EmailField(max_length=150, null=True)
    comune = models.CharField(max_length=150)
    province = models.CharField(max_length=150)
    region = models.CharField(max_length=150)
    billing_contacts = JSONField(null=True)
    file_pricings = JSONField(null=True)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    status = models.CharField(max_length=20, choices=FILE_STATUS)
    extra_fields = JSONField(null=True, blank=True)    
 
    class Meta:
        db_table = 'company_files'

class CompanyFilesLog(models.Model):
    stamp = models.DateTimeField(auto_now_add=True)
    company_file = models.ForeignKey(
        CompanyFiles,
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    status = models.CharField(max_length=20, choices=CompanyFiles.FILE_STATUS)

    class Meta:
        db_table = 'company_files_log'