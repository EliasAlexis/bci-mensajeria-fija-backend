# Generated by Django 3.0.3 on 2020-10-22 01:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicing', '0015_transports'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transports',
            name='plate',
            field=models.CharField(max_length=15, unique=True),
        ),
    ]
