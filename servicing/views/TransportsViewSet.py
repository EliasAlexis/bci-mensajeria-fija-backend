# rest framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.settings import api_settings
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

# models
from ..models import Transports

# serializers
from ..serializers.TransportSerializer import TransportSerializer

#etc
from utils.Permissions import ReadOnly

class TransportsViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser|ReadOnly)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    serializer_class = TransportSerializer
    search_fields = (
        'plate',
        'type',
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff:
            return Transports.objects.all().order_by('id')
        return Transports.objects.filter(available='yes').order_by('id')

