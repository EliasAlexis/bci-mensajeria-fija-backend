# rest framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework import status, serializers, filters
from rest_framework.exceptions import PermissionDenied
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.settings import api_settings

# django
from django.db import transaction

# serializers
from ..serializers.CompanyFilesSerializer import (
    CompanyFilesSerializer, 
    CompanyFilesLogSerializer,
    AdminCompanyFilesSerializer,
    TraderCompanyFilesSerializer
    )

from ..serializers.PricingSerializers import PricingSerializer

# models
from ..models import CompanyFiles, CompanyFilesLog

# permissions
from ..permissions import CanCreateApproveFile

# Etc
import copy

class CompanyFilesVieset(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, )
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('contact_first_name', 'address', 'status', 'company_name', 'rut', 'comune', 'region',)
    ordering_fields = ('contact_first_name', 'address', 'status', 'company_name', 'rut', 'comune', 'region',)
    filterset_fields = {
        'status': ['exact', 'in'],
        'owner': ['exact'],
    }
    
    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def get_serializer_class(self, *args, **kwargs):
        if self.request.user.is_staff:
            return AdminCompanyFilesSerializer
        return TraderCompanyFilesSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff:
            return CompanyFiles.objects.all().order_by("-id")
        else:
            return CompanyFiles.objects.filter(owner=self.request.user).order_by("-id")

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        data = copy.deepcopy(request.data)
        data["owner"] = request.user.pk
        # data["status"] = "draft"
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        creation_log = {
            "company_file": serializer.data["id"],
            "user": request.user.pk,
            "status": data["status"]
        }
        creation_serializer = CompanyFilesLogSerializer(data=creation_log)
        creation_serializer.is_valid(raise_exception=True)
        creation_serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user != instance.owner:
            raise PermissionDenied
        if (instance.status in CompanyFilesSerializer.FINAL_STATUSES
            or instance.status == "sent"):
            raise serializers.ValidationError({
                'status': "No es posible eliminar la ficha de cliente"
            })
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)