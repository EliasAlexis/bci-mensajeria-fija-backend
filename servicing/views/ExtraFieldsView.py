# rest framework
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings

# models
from management.models import Companies

# serializers
from ..serializers.ExtraFieldsSerializer import ExtraFieldsSerializer

class ExtraFieldsView(ListAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = ExtraFieldsSerializer
    pagination_class = None
    
    def get_queryset(self, *args, **kwargs):
        if self.request.user.groups.filter(name='operators').exists():
            try:
                return Companies.objects.get(pk=self.request.GET.get("company")).extra_fields
            except (Companies.DoesNotExist, ValueError):
                return []
        extra_fields = self.request.user.branch.company.extra_fields
        if extra_fields is None or extra_fields == "":
            return []
        return extra_fields
        
