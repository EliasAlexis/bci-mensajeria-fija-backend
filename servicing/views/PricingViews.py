# rest framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import status, filters, serializers
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.exceptions import PermissionDenied, ValidationError
from django_filters.rest_framework import DjangoFilterBackend
from utils.Permissions import ReadOnly

# django
from django.db import transaction
from django.db.models import Q

# serializers
from ..serializers.PricingSerializers import(
    SegmentsSerializer,
    ServicesSerializer,
    PricingSerializer,
    ProductsSerializer
)

# models
from ..models import Segments, Services, Pricings, Products

from ..filters import ServiceFilters, SegmentFilters, PricingFilters

# etc
from functools import partial

def normalize_price(price, **kwargs):
    price['default'] = 'yes'
    price['company'] = None
    price['min_quantity'] = 0
    normalized_field = kwargs.get('normalizer').get('field')
    normalized_value = kwargs.get('normalizer').get('value')
    price[normalized_field] = normalized_value
    return price

class SegmentsViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser|ReadOnly)
    http_method_names = ['get', 'head', 'post', 'put', 'patch', 'delete']
    serializer_class = SegmentsSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = (
        'name',
    )
    filterset_class = SegmentFilters
    ordering_fields = (
        'name',
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('operatorts', 'traders')):
            return Segments.objects.all().order_by('id')
        return Segments.objects.filter(Q(company=self.request.user.branch.company) | Q(default='yes')).order_by('id')
    

class ServicesViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser|ReadOnly)
    http_method_names = ['get', 'head', 'post', 'put']
    serializer_class = ServicesSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = (
        'name',
    )
    filterset_class = ServiceFilters
    ordering_fields = (
        'name',
        'status',
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('operatorts', 'traders')):
            return Services.objects.all().order_by('id')
        return Services.objects.filter(Q(company=self.request.user.branch.company) | Q(default='yes'), status='active').order_by('id')


class PricingsViewSet(ModelViewSet):
    pagination_class = None
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser|ReadOnly)
    http_method_names = ['get', 'head', 'post', 'put']
    serializer_class = PricingSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = PricingFilters
    

    def get_queryset(self, *args, **kwargs):
        # agregar validacion solicitando query params
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('operatorts', 'traders')):
            return Pricings.objects.all().order_by('segment', 'service')
        return Pricings.objects.filter(Q(company=self.request.user.branch.company) | Q(default='yes'))

    def create(self, request, *args, **kwargs):
        company = request.data.get("company")
        if company is not None or not company == '':
            raise serializers.ValidationError({
                'default': "Sólo es posible generar precios por defecto"
            })
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    # def update(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     if instance.company is not None:
    #         raise serializers.ValidationError({
    #             'company': "No es posible editar el precio de una Empresa"
    #         })
    #     serializer = self.get_serializer(instance, data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(data=serializer.data, status=status.HTTP_200_OK)

class ProductsViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser|ReadOnly)
    http_method_names = ['get', 'head', 'post', 'put']
    serializer_class = ProductsSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff:
            return Products.objects.all()
        return Products.objects.filter(status='active')
