# rest_frameword
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from rest_framework.settings import api_settings

# models
from ..models import CompanyFiles, CompanyFilesLog

# serializers
from ..serializers.CompaniesFileReportSerializer import CompaniesReportSerializer

# report class
from utils.AbstractReport import AbstractReport

class CompaniesFileReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = CompaniesReportSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('stamp', 'company_name', 'commercial_business' 'owner', 'status')

    columns = (
        'Fecha creación',
        'Razón social',
        'Nombre Fantasía',
        'Ejecutivo',
        'Estado'
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff:
            return CompanyFiles.objects.all().order_by('-id')
        # if self.request.user.groups.filter(name='traders').exists():
        #     trader_companies = [item.company.pk for item in TradersCompanies.objects.filter(trader=self.request.user)]
        #     return CompanyFiles.objects.filter(pk__in=trader_companies)


