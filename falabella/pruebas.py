import csv
import os 

dir_path = os.path.dirname(os.path.realpath(__file__))
# with open(dir_path + '/' +'csvData.csv', newline='') as File:  

results = []
with open(dir_path + '/' +'csvData.csv', encoding = 'cp850') as File:
    reader = csv.DictReader(File, delimiter=';')
    count=0
    for row in reader:
        results.append(row['SOC'])
        count=count+1
        if count==3:
            break
    print(results)