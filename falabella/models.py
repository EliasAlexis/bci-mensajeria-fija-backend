from django.db import models
from work_orders.models import WorkOrdersSteps
from work_orders.models import WorkOrders

class Step(models.Model):
    wo_step = models.ForeignKey(
        WorkOrdersSteps,
        on_delete=models.CASCADE,
        related_name="step_step"
    )

class falabella_log(models.Model):
    INF_TYPES = (
        ('soap', 'SOAP'),
        ('json', 'API'),
    )
    INF_STAUS = (
        ('ok', 'OK'),
        ('error', 'Error'),
    )
    created_at = models.DateTimeField(auto_now_add=True)
    wo = models.ForeignKey(
        WorkOrders,
        on_delete=models.CASCADE
    )
    inf_type = models.CharField(max_length=20, choices=INF_TYPES)
    status = models.CharField(max_length=20, choices=INF_STAUS)
    msg = models.CharField(max_length=800)

    class Meta:
        db_table = "falabella_log"