from suds.client import Client
from suds.sax.element import Element
from suds import WebFault
import datetime
import pytz
from postalApi.asyncCelery import app
from django.conf import settings
from work_orders.models import WorkOrders, WorkOrdersLog
from .models import falabella_log
from django.db import transaction

@app.task(name="soap")
def soap(wo_pk, log_id):
    wo = WorkOrders.objects.get(id=wo_pk)
    delivered_rut = ""
    delivered_dv = ""
    last_log = WorkOrdersLog.objects.get(id=log_id)
  
    # if wo[0].delivered_to_rut !="" :
    delivered_rut = wo.delivered_to_rut[:-1] if wo.delivered_to_rut else ""
    delivered_dv = wo.delivered_to_rut[-1] if wo.delivered_to_rut else ""
  
    try:
        # url = 'https://b2b.falabella.com/b2bwsfaclpr/ws/entregaDocumentoService?wsdl' #url producción
        url = settings.URL_FALABELLA_SOAP
        client = Client(url)

        wsse = ('wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd')
        security = Element('Security', ns=wsse)

        usernametoken = Element('UsernameToken', ns=wsse)
        usernametoken.insert(Element('Username', ns=wsse).setText(settings.USER_FALABELLA_SOAP))
        usernametoken.insert(Element('Password', ns=wsse).setText(settings.PWD_FALABELLA_SOAP))

        security.insert(usernametoken)
        client.set_options(soapheaders=security)
        result = client.service.entregaDocumento('SOR', wo.sender_extra_info, 'PGRL-10', datetime.datetime.now().isoformat(), 1, 20, 99, delivered_rut, delivered_dv, 1, 1)
    except WebFault as e:
        with transaction.atomic():
            falabella_log.objects.create(
                wo=wo,
                inf_type='soap',
                status='error',
                msg=e.fault
            )
        return e.fault
    # result = client.service.entregaDocumento('SOR', wo.sender_extra_info, 'JARI-98','2019-03-21T10:00:10',1,20,99,14137367,6,1,1)
    # print("soap" + result)
    with transaction.atomic():
        falabella_log.objects.create(
            wo=wo,
            inf_type='soap',
            status='ok',
            msg=result.value
        )
    return result

# params = dict(tipo-documento='SOR', 
# folio=148000047484, 
# patente='PGRL-10',
# fecha-evento='2019-03-21T10:00:10',
# latitud=1,
# longitud=20,
# codigo-no-entrega=99,
# rut-receptor=14137367,
# parentesco=6,
# bultos=1,
# autorizacion=1,
# )