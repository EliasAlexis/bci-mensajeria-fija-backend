# base
from django.urls import path, include
# from . import views
# from . import pruebas
from .importcsv import WOUpload

app_name = 'falabella'

urlpatterns = [
     # path('', views.index, name='index'),
     # path('csv/', pruebas.holaMundoxD, name='csv'),
     path('csv/', WOUpload.as_view(), name='csv'),
]

    

