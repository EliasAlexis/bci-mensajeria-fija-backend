# django
from django.conf import settings
from django.db.models import Q
from django.core import validators
from django.core.exceptions import ValidationError

# rest_framework
from rest_framework.viewsets import generics
from rest_framework.response import Response
from rest_framework import status

# models
from work_orders.models import WorkOrdersSteps

# serializers
from work_orders.serializers.WOUploadSerializer import WOUploadSerializer

#etc
from celery import group, chain
from utils.geocoding import bulk_falabella, bulk_geocode
from utils.query_inspector import query_debugger

from django.contrib import messages
from django.http import HttpResponse

from management.models import Companies, User, Branches
from servicing.models import Products, Services

import unicodecsv as csv
import json
from openpyxl.utils.exceptions import InvalidFileException
from channels.layers import get_channel_layer
# from asgiref.sync import async_to_sync
import googlemaps
from googlemaps.exceptions import HTTPError
import unicodedata

class WOUpload(generics.CreateAPIView):
    serializer_class = WOUploadSerializer
   
    def get_serializer_context(self):
        data_sender = User.objects.select_related("branch").filter(branch__company=settings.FALABELLA_ID, groups__name='company_admins').first()
        return {'owner': data_sender}

    def get_first_step(self):
        fist_step = WorkOrdersSteps.objects.select_related('flow').filter(
            Q(flow__company=self.get_serializer_context().get('owner').branch.company) | Q(flow__company=None),
            flow__has_return='no_return',
            parent=None,
        ).order_by('-flow').first()
        return fist_step

    @query_debugger
    def create(self, request, *args, **kwargs):
        data_sender = User.objects.filter(branch__company=settings.FALABELLA_ID, groups__name='company_admins').first()
        # data_branch = Branches.objects.get(id=data_sender.branch_id)

        csv_file = request.FILES['wo_file']
        producto = Products.objects.get(pk=2)
        servicio = Services.objects.get(pk=1)
        # gmaps = googlemaps.Client(key=settings.GMAPS_KEY)

        if not csv_file.name.endswith('.csv'):
            messages_error = 'Formato csv requerido'
            return Response(data=messages_error, status=status.HTTP_400_BAD_REQUEST)
        else:
            new_wos = []
            try:
                reader = csv.DictReader(csv_file, delimiter=';')
            except Exception as e:
                messages_error = 'Archivo no válido'
                return Response(data=messages_error, status=status.HTTP_400_BAD_REQUEST)

            address_mapping = []
            current_addreses = []
            socs = []
            first_step = self.get_first_step()
            for row in reader:
                if row['SOC'] in socs:
                    continue
                socs.append(row['SOC'])
                if len(current_addreses) == 10:
                    address_mapping.append(bulk_geocode.s(current_addreses))
                    current_addreses = []
                current_addreses.append("{},{}".format(row['DIRECCION'], row['COMUNA']))
                try:
                    receiver_mail = row['EMAIL']
                    try:
                        validators.validate_email(row['EMAIL'])
                    except ValidationError:
                        receiver_mail = "email@ejemplo.com"
                    new_wos.append({
                        'sender_full_name': data_sender.branch.company.commercial_business,
                        'sender_email': data_sender.email,
                        'sender_rut': data_sender.branch.company.rut,
                        'sender_phone': "56200000000",
                        'sender_address': data_sender.branch.address,
                        'sender_comune': data_sender.branch.comune,
                        'sender_region': data_sender.branch.region,
                        'sender_lat': data_sender.branch.lat,
                        'sender_lng': data_sender.branch.lng,
                        'receiver_full_name': row['NOMBRE_CLIENTE'],
                        'receiver_email': receiver_mail,
                        'receiver_phone': row['CONTACTO'],
                        'receiver_address': row['DIRECCION'],
                        'receiver_comune': row['COMUNA'],
                        'receiver_region': 'Metropolitana',
                        'receiver_lat': 0,
                        'receiver_lng': 0,
                        'sender_extra_info': row['SOC'],
                        'height': round(float(row['ALTO'] if row['ALTO'] else 0) * 100),
                        'width': round(float(row['ANCHO'] if row['ANCHO'] else 0) * 100),
                        'depth': round(float(row['LARGO'] if row['LARGO'] else 0) * 100),
                        'weight': round(float(row['PESO'] if row['PESO'] else 0), 2),
                        'product': producto.name,
                        'service': servicio.name,
                        'segment': 'Paqueteria pequeña',
                        'extra_fields': {
                            'soc': row['SOC'],
                            'num_viaje': row['N_VIAJE']
                        }
                        # 'extra_fields': row['SOC'],
                    })
                except IndexError:
                    return Response(data='Archivo no válido', status=status.HTTP_400_BAD_REQUEST)

            address_mapping.append(bulk_geocode.s(current_addreses))


            
            # .apply_async()
            # results = [item for sublist in r.join() for item in sublist]
            # r.forget()

            # for i, row in enumerate(new_wos):
            #     receiver_location = {}
            #     if len(results[i]) > 0:
            #         components = results[i][0]['address_components']
            #         receiver_location = results[i][0]["geometry"]["location"]
            #         try:
            #             receiver_address = "{} {}".format(
            #                     next(
            #                     comp["long_name"] for comp in components
            #                     if comp["types"][0] == "route"
            #                     ),
            #                     next(
            #                     comp["long_name"] for comp in components
            #                     if comp["types"][0] == "street_number"
            #                     )
            #                 )
            #         except (HTTPError, StopIteration):
            #             pass
            #     row['receiver_lat'] = round(receiver_location.get("lat", 0), 7)
            #     row['receiver_lng'] = round(receiver_location.get("lng", 0), 7)
            ser = self.get_serializer(data=new_wos, many=True)
            ser.is_valid(raise_exception=True)
            # ser.save()
            # channel_layer = get_channel_layer()
            # async_to_sync(channel_layer.group_send)(
            #     'operators',
            #     {
            #         "type":"ot.creation"
            #     }
            # )
            r = group(address_mapping)
            (r | bulk_falabella.s(new_wos, request.user.pk, data_sender.pk)).delay()
            return Response(data={
                'wo_file': 'Cargando'
            }, status=status.HTTP_200_OK)
        
