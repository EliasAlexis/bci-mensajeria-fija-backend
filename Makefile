interprete = @python3 manage.py

install:
	@pip3 install --upgrade pip
	@pip3 install -r postalApi/requirements.pip

seeder:
	$(interprete) shell < management/seeders/CompaniesDefault.py
	$(interprete) shell < management/seeders/UsersDefault.py 
	$(interprete) shell < servicing/seeders/ServicesDefault.py 
	$(interprete) shell < servicing/seeders/PricingDefaults.py 
	$(interprete) shell < work_orders/seeders/defaults.py 
	# $(interprete) shell < work_orders/seeders/OTs.py 

migrations:
	$(interprete) makemigrations
	$(interprete) migrate


reset_db:
	$(interprete) flush --noinput
