from rest_framework import serializers
from management.serializers.CompaniesSerializers import CompanySerializer
from management.models import TradersCompanies
from django.forms.models import model_to_dict

class PreInvoiceServiceSerializer(serializers.Serializer):
    name = serializers.CharField()
    no_return_count = serializers.IntegerField(required=False)
    with_return_count = serializers.IntegerField(required=False)
    no_return_price = serializers.IntegerField(required=False)
    with_return_price = serializers.IntegerField(required=False)
    no_return_total = serializers.IntegerField(required=False)
    with_return_total = serializers.IntegerField(required=False)

class PreInvoiceSegmentSerializer(serializers.Serializer):
    name = serializers.CharField()
    services = PreInvoiceServiceSerializer(many=True)

class PreInvoiceSerializer(CompanySerializer):
    executive = serializers.SerializerMethodField()

    segments = PreInvoiceSegmentSerializer(many=True)

    def get_executive(self, instance):
        try:
            trader = TradersCompanies.objects.get(company=instance.get("id")).trader
            return model_to_dict(
                trader,
                fields=('id', 'first_name', 'last_name', 'email', 'phone')
                )
        except TradersCompanies.DoesNotExist:
            return None
        print(instance)
        return "a"