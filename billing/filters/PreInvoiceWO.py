from django_filters import rest_framework as filters
from work_orders.models import WorkOrders
from management.models import Companies

class PreInvoiceWO(filters.FilterSet):
    company = filters.ModelMultipleChoiceFilter(queryset=Companies.objects.all(), field_name='owner__branch__company')

    class Meta:
        model = WorkOrders
        fields = {
            "has_return" : ["exact",],
            "company" : ["exact",],
            #"created_at" : ["gte", "lte",]
        }