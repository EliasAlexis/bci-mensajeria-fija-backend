# base
from django.urls import path

# viesets
from .views.PreInvoiceViews import PreInvoiceView

app_name = 'billing'

urlpatterns = [
    path('billing/pre/', PreInvoiceView.as_view(), name="pre_invoice"),
]