# django
from django.db.models import Count, Q, F, Subquery, OuterRef, IntegerField, ExpressionWrapper
from django.http import HttpResponse
from django.forms.models import model_to_dict
from django.conf import settings

# rest_framework
from rest_framework.generics import ListAPIView
from rest_framework.serializers import ValidationError
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.settings import api_settings

# models
from servicing.models import Services, Products, Segments, Pricings
from management.models import Companies, TradersCompanies, Branches

# serializers
from ..serializers.PreInvoiceSerializer import PreInvoiceSerializer

# etc
from io import BytesIO
import xlsxwriter

# from billing.serializers.PreInvoiceSerializer import PreInvoiceSerializer
class PreInvoiceView(ListAPIView):
    serializer_class = PreInvoiceSerializer
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsAdminUser, )

    ot_filter_fields = (
        ('created_at__gte', 'work_order_service__created_at__date__gte'),
        ('created_at__lte', 'work_order_service__created_at__date__lte')
        )
    pagination_class = None

    def generate_excel(self, company):
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        last_column = 6
        title_format = workbook.add_format({
            "bold": True,
            "align": "center",
            "bg_color": "#012e75",
            "font_color": "white"
        })
        normal_border = border = workbook.add_format({
            "border": 1
        })
        normal_border_money = border = workbook.add_format({
            "border": 1,
            "num_format": 5
        })
        sub_title = workbook.add_format({
            "bold": True,
            "align": "center",
            "border": 1
        })
        vertical_center = workbook.add_format({
            "valign": "vcenter",
            #"align": "center",
            "border": 1
        })
        worksheet.set_column(0, 0, 20)
        worksheet.set_column(1, 1, 15)
        worksheet.set_column(2, 2, 25)
        worksheet.set_column(2, 2, 15)
        worksheet.set_column(3, 3, 14)
        worksheet.set_column(5, 5, 15)
        worksheet.set_column(6, 6, 20)

        worksheet.merge_range(0, 0, 0, last_column, "Resumen Facturación Conexxion", title_format)
        worksheet.write("A2", "Razón Social", normal_border)
        worksheet.merge_range(1, 1, 1, last_column, company.get("name"), normal_border)
        worksheet.write("A3", "RUT", normal_border)
        worksheet.merge_range(2, 1, 2, last_column, company.get("rut"), normal_border)
        worksheet.write("A4", "Nombre comercial", normal_border)
        worksheet.merge_range(3, 1, 3, last_column, company.get("commercial_business"), normal_border)
        central = Branches.objects.filter(company=company.get("id")).first()
        adressFull = central.address +", "+ central.comune
        worksheet.write("A5", "Dirección", normal_border)
        worksheet.merge_range(4, 1, 4, last_column, adressFull, normal_border)
        worksheet.write("A6", "Teléfono", normal_border)
        worksheet.merge_range(5, 1, 5, last_column, company.get("phone"), normal_border)

        try:
            executive = TradersCompanies.objects.get(company=company.get("id")).trader
            executive = model_to_dict(executive)
        except TradersCompanies.DoesNotExist:
            executive = {}

        worksheet.write("A7", "Ejecutivo", normal_border)
        worksheet.merge_range(6, 1, 6, last_column, executive.get("first_name"), normal_border)
        worksheet.write("A8", "Mail Ejecutivo", normal_border)
        worksheet.merge_range(7, 1, 7, last_column, executive.get("email"), normal_border)
        worksheet.write("A10", "Período", normal_border)
        worksheet.merge_range(
            9,
            1,
            9,
            last_column,
            "{} - {}".format(self.request.GET.get("created_at__gte"), self.request.GET.get("created_at__lte")),
            normal_border
        )

        worksheet.write("A12", "Código", sub_title)
        worksheet.write("B12", "Tramo", sub_title)
        worksheet.write("C12", "Glosa", sub_title)
        worksheet.write("D12", "Retorno", sub_title)
        worksheet.write("E12", "Cantidad", sub_title)
        worksheet.write("F12", "Valor unit.", sub_title)
        worksheet.write("G12", "Total", sub_title)
        
        current_row = 12
        total_invoice = 0
        for row_num, segment in enumerate(company.get("segments")):
            if len(segment.get("services")) == 0:
                continue
            segment_displacement = 0
            for cell_key, data in enumerate(segment.get("services")):
                service_displacement = 0
                if data.get("no_return_count") > 0:
                    worksheet.write(current_row, 0, "", normal_border)
                    worksheet.write(current_row, 3, "Sin retorno", normal_border)
                    worksheet.write(current_row, 4, data.get("no_return_count"), normal_border)
                    worksheet.write(current_row, 5, data.get("no_return_price"), normal_border_money)
                    worksheet.write(current_row, 6, data.get("no_return_total"), normal_border_money)
                    total_invoice += data.get("no_return_total")
                    service_displacement += 1

                if data.get("with_return_count") > 0:
                    worksheet.write(current_row + service_displacement, 0, "", normal_border)
                    worksheet.write(current_row + service_displacement, 3, "Con retorno", normal_border)
                    worksheet.write(current_row + service_displacement, 4, data.get("with_return_count"), normal_border)
                    worksheet.write(current_row + service_displacement, 5, data.get("with_return_price"), normal_border_money)
                    worksheet.write(current_row + service_displacement, 6, data.get("with_return_total"), normal_border_money)
                    total_invoice += data.get("with_return_total")
                    service_displacement += 1
                if service_displacement == 2:
                    worksheet.merge_range(
                        current_row,
                        2,
                        current_row + service_displacement -1,
                        2,
                        data.get("name"),
                        vertical_center
                    )
                else:
                    worksheet.write(current_row, 2, data.get("name"), normal_border)
                current_row += service_displacement
                segment_displacement += service_displacement
            if segment_displacement > 1:
                worksheet.merge_range(
                    current_row - segment_displacement,
                    1,
                    current_row -1,
                    1,
                    segment.get("name"),
                    vertical_center
                )
            else:
                worksheet.write(current_row - 1, 1, segment.get("name"), normal_border)
            
        worksheet.write(current_row, last_column -1, "Neto", normal_border)
        worksheet.write(current_row, last_column, total_invoice, normal_border_money)
        worksheet.write(current_row + 1, last_column -1, "IVA {}%".format(settings.IVA * 100), normal_border)
        invoice_iva = round(total_invoice * settings.IVA)
        worksheet.write(current_row + 1, last_column, invoice_iva, normal_border_money)
        worksheet.write(current_row + 2, last_column -1, "Total", normal_border)
        worksheet.write(current_row + 2, last_column, total_invoice + invoice_iva, normal_border_money)

        workbook.close()
        output.seek(0)
        return output

    def get_date_filters(self):
        date_filters = {}
        for k, v in self.ot_filter_fields:
            if self.request.GET.get(k) is not None and not self.request.GET.get(k) == '':
                date_filters[v] = self.request.GET.get(k)
        return date_filters

    def get_queryset(self):
        if self.request.GET.get('company') is None or self.request.GET.get('company') == "":
            return Companies.objects.none()
        pks = self.request.GET.get('company').split(',')
        companies = Companies.objects.filter(pk__in=pks).values()
        for company in companies:
            no_return = Count(
                'work_order_service',
                filter=Q(
                    work_order_service__has_return='no_return',
                    work_order_service__owner__branch__company=company["id"],
                    **self.get_date_filters()
                ) & ~Q(work_order_service__current_step__status__code=0)
            )
            with_return = Count(
                'work_order_service',
                filter=Q(
                    work_order_service__has_return='with_return',
                    work_order_service__owner__branch__company=company["id"],
                    **self.get_date_filters()
                ) & ~Q(work_order_service__current_step__status__code=0)
            )
            segments = Segments.objects.filter(
                    Q(default='yes') | Q(company=company["id"])
                ).values("name", "id")
            for el in segments:
                prices = Pricings.objects.filter(default='yes', segment=el["id"], service=OuterRef('pk'))
                el["services"] = Services.objects.filter(
                    work_order_service__segment=el["id"],
                    work_order_service__service=F("id"),
                    work_order_service__owner__branch__company=company["id"],
                    **self.get_date_filters()
                    ).annotate(
                    no_return_count=no_return,
                    with_return_count=with_return,
                    no_return_price=Subquery(prices.values('no_return')),
                    with_return_price=Subquery(prices.values('with_return')[:1]),
                    no_return_total=ExpressionWrapper(F('no_return_count')*F('no_return_price'), output_field=IntegerField()),
                    with_return_total=ExpressionWrapper(F('with_return_count')*F('with_return_price'), output_field=IntegerField())
                ).values()
            company["segments"] = segments
        return companies

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        if(request.GET.get("type") == "excel"):
            output = self.generate_excel(queryset[0])
            response = HttpResponse(
                output,
                content_type='application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename="Reporte.xlsx"'
            return response
        return super().list(request, args, kwargs)
