from celery import group
from postalApi.asyncCelery import app

# django
from django.conf import settings

from rest_framework.renderers import JSONRenderer

from work_orders.serializers.WOUploadSerializer import WOUploadSerializer

from management.models import User

# maps
import googlemaps
from googlemaps.exceptions import HTTPError

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

@app.task(name='bulk_geocode')
def bulk_geocode(adresses):
    assert isinstance(adresses, (list, tuple)), (
        'adresses debe ser una lista o una tupla'
    )
    gmaps = googlemaps.Client(key=settings.GMAPS_KEY)
    results = []
    for el in adresses:
        results.append(
            gmaps.geocode(el)
        )
    return results

@app.task(name='bulk_falabella', bind=True)
def bulk_falabella(self, adresses, new_wos, user_pk, owner_pk):
    # print(adresses)
    # print(new_wos)
    # assert isinstance(adresses, (list, tuple)), (
    #         'adresses debe ser una lista o una tupla'
    #     )
    # assert isinstance(adresses, (list, tuple)), (
    #         'new_wos debe ser una lista o una tupla'
    #     )
    # adresses_tasks = []
    # for el in adresses:
    #     adresses_tasks.append(bulk_geocode.s(el))

    # r = group(adresses_tasks).apply_async()
    results = [item for sublist in adresses for item in sublist]
    # r.forget()
    # self.update_state(state='PROGRESS', meta={'progress': 75})

    for i, row in enumerate(new_wos):
        receiver_location = {}
        if len(results[i]) > 0:
            components = results[i][0]['address_components']
            try:
                receiver_address = '{} {}'.format(
                        next(
                        comp['long_name'] for comp in components
                        if comp['types'][0] == 'route'
                        ),
                        next(
                        comp['long_name'] for comp in components
                        if comp['types'][0] == 'street_number'
                        )
                    )
                receiver_location = results[i][0]['geometry']['location']
            except (HTTPError, StopIteration):
                pass
        row['receiver_lat'] = round(receiver_location.get('lat', 0), 7)
        row['receiver_lng'] = round(receiver_location.get('lng', 0), 7)
    # print(new_wos)
    user = User.objects.get(pk=user_pk)
    owner = User.objects.get(pk=owner_pk)
    ser = WOUploadSerializer(data=new_wos, many=True, context={'owner': owner})
    ser.is_valid(raise_exception=True)
    ser.save()

    channel_layer = get_channel_layer()
    # print(user.group_name)
    async_to_sync(channel_layer.group_send)(
        user.group_name,
        {
            'type': 'send.result',
            'data': JSONRenderer().render(ser.data).decode('utf-8')
        }
    )
    async_to_sync(channel_layer.group_send)(
        'operators',
        {
            'type':'ot.creation'
        }
    )
    return ser.data