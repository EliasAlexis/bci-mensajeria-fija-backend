from rest_framework import serializers
from jwt import decode, exceptions
from rest_framework_simplejwt.settings import DEFAULTS
from management.models import Token
from postalApi.settings import SECRET_KEY


def token_validator(token, token_type):
    try:
        token_decode = decode(
            token,
            SECRET_KEY,
            algorithms=DEFAULTS['ALGORITHM']
        )
    except exceptions.DecodeError:
        raise serializers.ValidationError(detail=['Solicitud de recuperación inválida'])
    except exceptions.ExpiredSignatureError:
        raise serializers.ValidationError(detail=['Solicitud de recuperación expirada, debe realizar la petición nuevamente'])
    if token_decode['token_type'] != token_type:
        raise serializers.ValidationError(detail=['Solicitud de recuperación inválida'])
    if not Token.objects.filter(token=token).exists():
        raise serializers.ValidationError(detail=['Solicitud de recuperación inválida'])
    return token_decode

def rut_validator(raw_rut):
    rut = raw_rut[:-1]
    incoming_dv = str(raw_rut[-1]).upper()
    value = 11 - sum([ int(a)*int(b)  for a,b in zip(str(rut).zfill(8), '32765432')])%11
    calc_dv = {10: 'K', 11: '0'}.get(value, str(value))
    if incoming_dv != calc_dv:
        raise serializers.ValidationError("RUT inválido")
    return raw_rut