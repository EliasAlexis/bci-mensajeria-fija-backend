# django
from rest_framework.generics import ListAPIView
from django.http import HttpResponse

# etc
from io import BytesIO
import xlsxwriter
from datetime import datetime
from dateutil import tz

# reportlab
from reportlab.lib.pagesizes import LETTER, landscape
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib import colors
import reportlab.lib.units as units
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT

class AbstractReport(ListAPIView):
    pagination_class = None
    columns = None
    col_widths = None
    page_size = LETTER

    def generate_excel(self, rows):
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        cell_format = workbook.add_format()
        cell_format.set_bold()
        for col_num, cell_data in enumerate(self.columns):
            worksheet.write(0, col_num, cell_data, cell_format)

        datetime_format = workbook.add_format({'num_format': 'DD-MM-yyyy hh:mm:ss'})
        for row_num, row_data in enumerate(rows):
            for col_num, (cell_key, cell_data)  in enumerate(row_data.items()):
                if (isinstance(cell_data, datetime)):
                    to_zone = tz.tzlocal()
                    cell_data = cell_data.astimezone(to_zone)
                    naive_datetime = cell_data.replace(tzinfo=None)
                    worksheet.write(row_num+1, col_num, naive_datetime, datetime_format)
                else:
                    worksheet.write(row_num+1, col_num, cell_data)
        workbook.close()
        output.seek(0)
        return output
    
    def generate_pdf(self, rows, col_widths):
        buffer = BytesIO()
        document = SimpleDocTemplate(
            buffer,
            pagesize=landscape(self.page_size),
            leftMargin=5*units.mm,
            rightMargin=5*units.mm,
            topMargin=5*units.mm,
            bottomMargin=5*units.mm,
            title="Reporte",
            #showBoundary=1,
            initialFontSize=3
        )
        elements = []
        table_data = []
        current_row_data = []
        STYLES = getSampleStyleSheet()
        TEXT_LEFT = ParagraphStyle(
            name='left',
            parent=STYLES['Normal'],
            alignment=TA_LEFT,
            leading=9,
            fontSize=9,
        )

        for cell_data in self.columns:
            current_row_data.append(Paragraph("<b>{}</b>".format(cell_data), TEXT_LEFT))
        table_data.append(current_row_data)

        for row_data in rows:
            current_row_data = []
            for cell_key, cell_data in row_data.items():
                if (isinstance(cell_data, datetime)):
                    to_zone = tz.tzlocal()
                    cell_data = cell_data.astimezone(to_zone)
                    cell_data = cell_data.strftime("%d-%m-%Y %H:%M:%S")
                current_row_data.append(
                    Paragraph(
                        "" if cell_data is None else str(cell_data),
                        TEXT_LEFT
                    )
                )
            table_data.append(current_row_data)
        table = Table(table_data, colWidths=col_widths)
        table.setStyle(
            TableStyle([
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                ('FONTSIZE', (0, 0), (-1, -1), 9),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ])
        )
        elements.append(table)
        document.build(elements)

        pdf = buffer.getvalue()
        buffer.close()
        return pdf

    def get_columns(self):
        assert self.columns is not None, (
            "Se debe incluir la propiedad columnas o sobreescribir el método get_columns"
        )
        return self.columns

    def list(self, request, *args, **kwargs):
        report_type = request.GET.get("report_type", "excel")
        unfiltered = self.get_queryset()
        queryset = self.filter_queryset(unfiltered)
        serializer = self.get_serializer(queryset, many=True)
        rows = serializer.data
        self.columns = self.get_columns()
        if report_type == "pdf":
            if self.col_widths is not None:
                self.col_widths =  list(
                    map(
                        lambda el: el*units.mm,
                        self.col_widths
                    )
                )
            output = self.generate_pdf(
                rows,
                self.col_widths
            )
            response = HttpResponse(output, content_type='application/pdf')
            response['Content-Disposition'] = 'inline; filename=Reporte.pdf'

        if report_type == "excel":
            output = self.generate_excel(rows)
            response = HttpResponse(
                output,
                content_type='application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename="Reporte.xlsx"'
        return response