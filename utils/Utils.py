# token decode encode packages
from time import time
from rest_framework_simplejwt.settings import DEFAULTS
from postalApi.settings import SECRET_KEY, FRONT_URL, SUBSCRIPTION_EMAIL_SENDING
from jwt import encode
from management.models import Token
from work_orders.models import WorkOrders

# email sending
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMessage
# celery
from celery.utils.log import get_task_logger
from postalApi.asyncCelery import app

# push notifications
from push_notifications.models import APNSDevice, GCMDevice
from push_notifications.gcm import GCMError


logger = get_task_logger(__name__)

def token_encode(token_type, user):
    token_encode = encode(
        {
            'user_email': user.email,
            'exp': int(time()) + DEFAULTS['ACCESS_TOKEN_LIFETIME'].total_seconds(),
            'token_type': token_type,
        }, SECRET_KEY, algorithm=DEFAULTS['ALGORITHM']).decode("utf-8")
    Token.objects.create(token=token_encode)
    return token_encode

@app.task(name='send_email_async')
def send_email_async(template, to, cc=None, subject='New Email', email_data=None, **kwargs):
    if not email_data:
        email_data = {}
    email_data['server'] = FRONT_URL
    msg_html = render_to_string(
        template,
        email_data
    )

    msg = EmailMessage(
        subject=subject,
        body=msg_html,
        from_email=SUBSCRIPTION_EMAIL_SENDING,
        to=to,
        cc=cc
    )
    msg.content_subtype = "html"
    msg.send()

# def send_email(email_type, **kwargs):

#     msg_data = {
#         'server': FRONT_URL
#         }
#     if email_type == 'reset_password':
#         template = 'reset_password.html'
#         title = 'Recuperar Contraseña'
#         msg_data['token'] = kwargs.get('token')
#         msg_data['user'] = kwargs.get('user')
#         to = (msg_data['user'].get('email'),)
#     elif email_type == 'welcome':
#         template = 'welcome_mail.html'
#         title = 'Conexxion Bienvenido'
#         msg_data['user'] = kwargs.get('user')
#         msg_data['server'] = FRONT_URL
#         to = (msg_data['user'].get('email'),)
#     elif email_type == 'tracking':
#         template = 'tracking.html'
#         title = 'Conexxion Seguimiento'
#         to = kwargs.get('to')
#         msg_data['info'] = kwargs.get('info')
#     subject = 'Conexxion'
#     from_email = SUBSCRIPTION_EMAIL_SENDING
#     msg_html = render_to_string(
#         template,
#         msg_data
#     )
#     send_mail(
#         title,
#         subject,
#         from_email,
#         to,
#         html_message=msg_html,
#     )

# @app.task(name="send_email_async")
# def send_email_async(email_type, **kwargs):
#     send_email(email_type, **kwargs)

@app.task(name="send_notification")
def send_notification(body, title, barcode, transporter_pk):
    devices = GCMDevice.objects.filter(user__pk=transporter_pk)
    for device in devices:
        try:
            device.send_message(
                body,
                title=title,
                extra={"barcode":barcode}
            )
        except GCMError:
            continue


 
