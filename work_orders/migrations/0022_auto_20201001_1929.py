# Generated by Django 3.0.3 on 2020-10-01 22:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('work_orders', '0021_auto_20200925_1405'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='woroutes',
            options={'ordering': ('order',)},
        ),
        migrations.AlterField(
            model_name='woroutes',
            name='route',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='work_orders', to='work_orders.TransporterRoutes'),
        ),
        migrations.AlterField(
            model_name='woroutes',
            name='work_order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='transporter_route_work_order', to='work_orders.WorkOrders'),
        ),
    ]
