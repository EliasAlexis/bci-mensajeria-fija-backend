# Generated by Django 3.0 on 2020-02-25 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('work_orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workordersflow',
            name='default',
            field=models.CharField(choices=[('yes', 'Si'), ('no', 'no')], default='no', max_length=10),
        ),
    ]
