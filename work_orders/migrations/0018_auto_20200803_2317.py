# Generated by Django 3.0.3 on 2020-08-04 03:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('work_orders', '0017_auto_20200803_2324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workorders',
            name='product_description',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='receiver_address_number',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='receiver_phone',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='receiver_rut',
            field=models.CharField(blank=True, default='', max_length=20),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='sender_address_number',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='sender_extra_info',
            field=models.CharField(blank=True, default='', max_length=300),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='sender_phone',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='workorders',
            name='sender_rut',
            field=models.CharField(blank=True, default='', max_length=20),
        ),
    ]
