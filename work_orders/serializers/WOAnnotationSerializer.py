# django
from django.db import transaction
from django.conf import settings

# rest_framework
from rest_framework import serializers

# models
from ..models import WoAnnotation

# etc
from datetime import datetime, timezone

class WOAnnotationSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    class Meta:
        model = WoAnnotation
        fields = (
            'id',
            'created_at',
            'note',
            'author',
            'user',
            'wo',
        )

    def get_author(self, instance):
        return "{} {}".format(
            instance.user.first_name,
            instance.user.last_name
        )

class WOUndoSerializer(serializers.ModelSerializer):

    class Meta:
        model = WoAnnotation
        fields = (
            'created_at',
            'note',
            'user',
            'wo',
        )
        extra_kwargs = {
            'note': {'required': False},
        }

    def validate_wo(self, value):
        if not value.current_step.parent:
            raise serializers.ValidationError("No es posible revertir el primer estado de la OT")
        delta_time = datetime.now(timezone.utc) - value.work_order_logs.all().order_by('-id').first().created_at
        max_undo_delta = settings.UNDO_TIME
        if delta_time.total_seconds() > max_undo_delta:
            raise serializers.ValidationError("Ha excedido el tiempo máximo permtido para revertir la última operación")
        return value

    @transaction.atomic
    def create(self, validated_data):
        wo = validated_data.get("wo")
        last_log = wo.work_order_logs.filter(voided=False).order_by('-id').first()
        validated_data["note"] = "Estado {} revertido".format(last_log.step_detail)
        # last_log.delete()
        last_log.voided = True
        last_log.save()
        # wo.transporter = last_log.transporter
        wo.current_step = wo.current_step.parent
        wo.save()
        return super().create(validated_data)
