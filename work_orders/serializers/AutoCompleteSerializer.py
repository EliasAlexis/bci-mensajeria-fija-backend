from rest_framework import serializers
from ..models import AutoComplete

class AutoCompleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoComplete
        fields = '__all__'
        extra_kwargs = {
            'companysearch': {'required': False},
        }

    def create(self, validated_data):
        company = self.context.get("user").branch.company
        validated_data['companysearch'] = company
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data['companysearch'] = instance.companysearch
        return super().update(instance, validated_data)