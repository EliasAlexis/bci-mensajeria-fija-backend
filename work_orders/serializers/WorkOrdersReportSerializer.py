# rest_framework
from rest_framework import serializers

# models
from ..models import WorkOrders, WorkOrdersLog
from utils.query_inspector import query_debugger

def return_on_failure(func):
    def inner_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (WorkOrdersLog.DoesNotExist, AttributeError):
            return ''
    return inner_function

class WorkOrdersElevatedReport(serializers.Serializer):
    barcode = serializers.CharField(max_length=200)
    created_at = serializers.DateTimeField()
    company = serializers.CharField(max_length=200)
    sender_full_name = serializers.CharField(max_length=200)
    sender_phone = serializers.CharField(max_length=200)
    sender_address = serializers.CharField(max_length=200)
    sender_comune = serializers.CharField(max_length=200)
    receiver_full_name = serializers.CharField(max_length=200)
    receiver_phone = serializers.CharField(max_length=200)
    receiver_address = serializers.CharField(max_length=200)
    receiver_comune = serializers.CharField(max_length=200)
    service_name = serializers.CharField(max_length=200)
    product_name = serializers.CharField(max_length=200)
    segment_name = serializers.CharField(max_length=200)
    has_return = serializers.CharField(max_length=200)
    last_status = serializers.CharField(max_length=200)
    sender_extra_info = serializers.CharField(max_length=200)
    product_description = serializers.CharField(max_length=200)
    pick_up_assignation_date = serializers.DateTimeField()
    pick_up_transporter = serializers.CharField(max_length=200)
    pick_up_transporter_helper = serializers.CharField(max_length=200)
    pick_up_plate = serializers.CharField(max_length=200)
    pick_up_report_serie = serializers.IntegerField()
    pick_up_date = serializers.DateTimeField()
    central_date = serializers.DateTimeField()
    delivery_assignation_date = serializers.DateTimeField()
    delivery_assignation_transporter = serializers.CharField(max_length=200)
    delivery_assignation_transporter_helper = serializers.CharField(max_length=200)
    delivery_assignation_plate = serializers.CharField(max_length=200)
    delivery_assignation_report_serie = serializers.IntegerField()
    delivery_date = serializers.DateTimeField()
    delivered_to_name = serializers.CharField(max_length=200)
    delivered_to_rut = serializers.CharField(max_length=20)
    delivery_location = serializers.CharField(max_length=200)
    failed_delivery_date = serializers.DateTimeField()
    failed_delivery_comment = serializers.CharField(max_length=200)
    central_return_date = serializers.DateTimeField()
    delivery_return_assignation_date = serializers.DateTimeField()
    delivery_return_assignation_transporter = serializers.CharField(max_length=200)
    delivery_return_assignation_transporter_helper = serializers.CharField(max_length=200)
    delivery_return_assignation_plate = serializers.CharField(max_length=200)
    delivery_return_assignation_report_serie = serializers.IntegerField()
    delivery_return_date = serializers.DateTimeField()
    last_comment = serializers.CharField(max_length=200)
            
# class WorkOrdersElevatedReport(serializers.ModelSerializer):
#     company = serializers.SerializerMethodField()
#     segment = serializers.SerializerMethodField()
#     last_status = serializers.SerializerMethodField()
#     has_return = serializers.SerializerMethodField()
#     pick_up_assignation_date = serializers.SerializerMethodField()
#     pick_up_transporter = serializers.SerializerMethodField()
#     pick_up_date = serializers.SerializerMethodField()
#     central_date = serializers.SerializerMethodField()
#     delivery_assignation_date = serializers.SerializerMethodField()
#     delivery_assignation_transporter = serializers.SerializerMethodField()
#     delivery_date = serializers.SerializerMethodField()
#     central_return_date = serializers.SerializerMethodField()
#     delivery_return_assignation_date = serializers.SerializerMethodField()
#     delivery_return_assignation_transporter = serializers.SerializerMethodField()
#     delivery_return_date = serializers.SerializerMethodField()
#     last_comment = serializers.SerializerMethodField()

#     class Meta:
#         model = WorkOrders
#         fields = (
#             'barcode',
#             'created_at',
#             'company',
#             'sender_full_name',
#             'sender_phone',
#             'sender_address',
#             'sender_comune',
#             'receiver_full_name',
#             'receiver_phone',
#             'receiver_address',
#             'receiver_comune',
#             'service_name',
#             'product_name',
#             'segment',
#             'has_return',
#             'last_status',
#             'sender_extra_info',
#             'product_description',
#             'pick_up_assignation_date',
#             'pick_up_transporter',
#             'pick_up_date',
#             'central_date',
#             'delivery_assignation_date',
#             'delivery_assignation_transporter',
#             'delivery_date',
#             'central_return_date',
#             'delivery_return_assignation_date',
#             'delivery_return_assignation_transporter',
#             'delivery_return_date',
#             'last_comment',
#         )

#     def get_company(self, instance):
#         return instance.owner.branch.company.commercial_business

#     def get_segment(self, instance):
#         return instance.segment.name

#     def get_last_status(self, instance):
#         return instance.current_step.status.name

#     @return_on_failure
#     def get_pick_up_assignation_date(self, instance):
#         return instance.work_order_logs.get(step__status__code=2, voided=False).created_at
    
#     @return_on_failure
#     def get_pick_up_transporter(self, instance):
#         transporter = instance.work_order_logs.get(step__status__code=2, voided=False).transporter
#         return '{} {}'.format(
#             transporter.first_name,
#             transporter.last_name,
#         )

#     @return_on_failure
#     def get_pick_up_date(self, instance):
#         return instance.work_order_logs.filter(step__status__code=3, voided=False).order_by('created_at').first().created_at

#     @return_on_failure
#     def get_central_date(self, instance):
#         return instance.work_order_logs.filter(step__status__code=4, voided=False).order_by('created_at').first().created_at

#     @return_on_failure
#     def get_delivery_assignation_date(self, instance):
#         return instance.work_order_logs.filter(step__status__code=5, voided=False).order_by('created_at').first().created_at

#     @return_on_failure
#     def get_delivery_assignation_transporter(self, instance):
#         transporter = instance.work_order_logs.filter(step__status__code=5, voided=False).order_by('created_at').first().transporter
#         return '{} {}'.format(
#             transporter.first_name,
#             transporter.last_name,
#         )

#     @return_on_failure
#     def get_delivery_date(self, instance):
#         if instance.has_return == 'with_return':
#             return instance.work_order_logs.filter(step__status__code=3, voided=False).order_by('-created_at').first().created_at
#         return instance.work_order_logs.filter(step__status__code=6, voided=False).order_by('created_at').first().created_at

#     @return_on_failure
#     def get_central_return_date(self, instance):
#         if instance.has_return == 'with_return':
#             return instance.work_order_logs.filter(step__status__code=4, voided=False).order_by('-created_at').first().created_at

#     @return_on_failure
#     def get_delivery_return_assignation_date(self, instance):
#         if instance.has_return == 'with_return':
#             return instance.work_order_logs.filter(step__status__code=5, voided=False).order_by('-created_at').first().created_at

#     @return_on_failure
#     def get_delivery_return_assignation_transporter(self, instance):
#         if instance.has_return == 'with_return':
#             transporter = instance.work_order_logs.filter(step__status__code=5, voided=False).order_by('-created_at').first().transporter
#             return '{} {}'.format(
#                 transporter.first_name,
#                 transporter.last_name,
#             )

#     @return_on_failure
#     def get_delivery_return_date(self, instance):
#         if instance.has_return == 'with_return':
#         # revisar retorno
#             return instance.work_order_logs.filter(step__status__code=6).latest('-created_at').created_at

#     def get_has_return(self, instance):
#         return instance.get_has_return_display()

#     @return_on_failure
#     def get_last_comment(self, instance):
#         return instance.work_order_logs.latest('created_at').deliver_info

class WorkOrdersReportSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    segment = serializers.SerializerMethodField()
    last_status = serializers.SerializerMethodField()
    delivery_date = serializers.SerializerMethodField()
    has_return = serializers.SerializerMethodField()
    last_comment = serializers.SerializerMethodField()

    class Meta:
        model = WorkOrders
        fields = (
            'barcode',
            'created_at',
            'company',
            'sender_full_name',
            'sender_phone',
            'sender_address',
            'sender_comune',
            'receiver_full_name',
            'receiver_phone',
            'receiver_address',
            'receiver_comune',
            'service_name',
            'product_name',
            'segment',
            'has_return',
            'last_status',
            'sender_extra_info',
            'product_description',
            'delivery_date',
            'last_comment',
        )

    def get_company(self, instance):
        return instance.owner.branch.company.commercial_business

    def get_segment(self, instance):
        return instance.segment.name

    def get_last_status(self, instance):
        return instance.current_step.status.name

    def get_delivery_date(self, instance):
        try:
            return instance.work_order_logs.filter(step__status__code=6).latest('created_at').created_at
        except WorkOrdersLog.DoesNotExist:
            return ''

    def get_has_return(self, instance):
        return instance.get_has_return_display()

    def get_last_comment(self, instance):
        return instance.work_order_logs.latest('created_at').deliver_info

class WorkOrdersReportPdfSerializer(serializers.ModelSerializer):
    segment = serializers.SerializerMethodField()
    last_status = serializers.SerializerMethodField()
    delivery_date = serializers.SerializerMethodField()
    class Meta:
        model = WorkOrders
        fields = (
            'barcode',
            'created_at',
            'sender_full_name',
            'sender_address',
            'sender_comune',
            'receiver_full_name',
            'receiver_address',
            'receiver_comune',
            'service_name',
            'product_name',
            'segment',
            'last_status',
            'product_description',
            'delivery_date',
        )

    def get_segment(self, instance):
        return instance.segment.name

    def get_last_status(self, instance):
        return instance.current_step.status.name

    def get_delivery_date(self, instance):
        try:
            deliver_step = WorkOrdersLog.objects.get(work_order=instance, step__status__code=6)
            return deliver_step.created_at
        except WorkOrdersLog.DoesNotExist:
            return ''
