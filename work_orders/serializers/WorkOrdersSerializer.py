# django
from django.db.models import Q
from django.conf import settings
from django.db import transaction
from falabella.models import Step
from falabella import services, soap

# rest_framework
from rest_framework import serializers

# models
from ..models import WorkOrders, WorkOrdersSteps, Status, WorkOrdersLog, WorkOrdersLogPictures
from servicing.models import Transports
from management.models import User

# etc
from servicing.models import Segments
from utils.Utils import send_email_async, send_notification
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from ..utils import fetchChilden
from utils.ExtraFields import JSONSerializerField, Base64ImageField
import copy
# from utils.query_inspector import query_debugger

# jsonchema
from jsonschema import validate
from jsonschema.exceptions import ValidationError

class WOLogPictureSerializer(serializers.ModelSerializer):
    picture = Base64ImageField(max_length=None, use_url=True, allow_null=True, required=False)
    # log = serializers.ModelField(WorkOrdersLog()._meta.get_field('id'))
    class Meta:
        model = WorkOrdersLogPictures
        fields = ('picture',)

class WorkOrdersLogSerializer(serializers.ModelSerializer):
    status_code = serializers.SerializerMethodField()
    pictures = serializers.SerializerMethodField()

    class Meta:
        model = WorkOrdersLog
        fields = '__all__'
        extra_kwargs = {
            'step_detail': {'required': False},
        }

    def get_status_code(self, instance):
        return instance.step.status.code
    
    def get_pictures(self, instance):
        return WOLogPictureSerializer(instance.log_pictures, many=True, context=self.context).data

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['created_at'] = instance.created_at
        if instance.user is not None:
            ret['user'] = '{} {}'.format(instance.user.first_name, instance.user.last_name)
        if instance.transporter is not None:
            ret['transporter'] = '{} {}'.format(instance.transporter.first_name, instance.transporter.last_name)
        if instance.transporter_helper is not None:
            ret['transporter_helper'] = '{} {}'.format(instance.transporter_helper.first_name, instance.transporter_helper.last_name)
        if instance.transport is not None:
            ret['transport'] = {
                'plate': instance.transport.plate,
                'type': instance.transport.get_type_display(),
            }

        return ret

class WorkOrdersSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    extra_fields = JSONSerializerField(required=False, default={})
    transporter = serializers.SerializerMethodField()
    last_log = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(WorkOrdersSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = WorkOrders
        fields = '__all__'
        extra_kwargs = {
            'service_name': {'required': False},
            'current_step': {'required': False},
            'product_name': {'required': False},
            'owner': {'required': False},
        }
        read_only_fields = ('barcode',)

    def validate_extra_fields(self, value):
        if self.instance is not None:
            company = self.instance.owner.branch.company
        else:
            company = self.context.get('owner').branch.company
        try:
            validate(
                instance=value,
                schema=company.extra_fields_schema
            )
        except ValidationError:
            raise serializers.ValidationError('Campos extras incorrectos solamente se permiten {}'.format(
                [el['name'].lower() for el in company.extra_fields]))
        return value

    def get_company(self, instance):
        return instance.owner.branch.company.commercial_business
    
    def get_transporter(self, instance):
        if instance.transporter:
            return '{} {}'.format(
                instance.transporter.first_name,
                instance.transporter.last_name,
                )
        return ''

    def calculate_segment(self, validated_data):
        available_segments = Segments.objects.filter(
                max_weight__gte=validated_data['weight']
            ).filter(
                height__gte=validated_data['height'],
                width__gte=validated_data['width'],
                depth__gte=validated_data['depth'],
            ).filter(Q(company=self.context.get('owner').branch.company)|Q(default='yes')).order_by('company')
        if len(available_segments) > 0:
            return available_segments.first()
        return Segments.objects.filter(default='yes').order_by('id').first()

    def get_first_step(self, validated_data):
        fist_step = WorkOrdersSteps.objects.select_related('flow').filter(
            Q(flow__company=validated_data.get('owner').branch.company) | Q(flow__company=None),
            flow__has_return=validated_data.get('has_return'),
            parent=None,
        ).order_by('-flow').first()
        return fist_step

    def get_last_log(self, instance):
        return WorkOrdersLogSerializer(instance.work_order_logs.latest('created_at')).data

    # @query_debugger
    def create(self, validated_data):
        extra_fields = validated_data.get('extra_fields', {})
        extra_settings = {
            'print': False,
            'signature': self.context.get('owner').branch.company.needs_signature,
            'picture': self.context.get('owner').branch.company.needs_picture
        }
        # extra_fields['print'] = False
        validated_data['owner'] = self.context.get('owner')
        validated_data['extra_fields'] = {**extra_fields, **extra_settings}
        validated_data['current_step'] = self.get_first_step(validated_data)
        if self.context.get('calc_segment', True):
            validated_data['segment'] = self.calculate_segment(validated_data)
        created = super().create(validated_data)
        log_data = {
            'work_order': created.id,
            'user': created.owner.id,
            'step': created.current_step.id,
            'lat': created.sender_lat,
            'lng': created.sender_lng
        }
        log_serializer = WorkOrdersLogSerializer(data=log_data)
        log_serializer.is_valid(raise_exception=True)
        log_serializer.save()
        return created
        # return validated_data

    def update(self, instance, validated_data):
        EXCLUDED_FIELDS = (
            'transporter',
            'owner',
            'current_step',
            'notification_type',
            'service_name',
            'product_name',
            'extra_fields',
        )
        if self.context.get('calc_segment', True):
            validated_data['segment'] = self.calculate_segment(validated_data)
        validated_extra = validated_data.get('extra_fields', {})
        for el in instance.owner.branch.company.extra_fields:
            instance.extra_fields[el['name']] = validated_extra.get(el['name'], None)
        for attr, value in validated_data.items():
            if attr in EXCLUDED_FIELDS:
                continue
            setattr(instance, attr, value)
        # print(instance.extra_fields)
        instance.save()
        return instance

    def get_owner(self, user):
        return {
            'company_name': user.branch.company.name,
            'company': user.branch.company.pk,
            'branch_id': user.branch.pk,
            'user_id': user.pk
        }

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['owner'] = self.get_owner(instance.owner)
        ret['current_step'] = {
            'id' : instance.current_step.pk,
            'code': instance.current_step.status.code,
            'name': instance.current_step.status.name,
            'step_detail': instance.current_step.alt_name if instance.current_step.alt_name is not None else instance.current_step.status.name,
            'to_sender': instance.current_step.to_sender,
            'is_delivery': instance.current_step.is_delivery,
        }
        return ret

class WorkOrdersProcessSerializer(serializers.ModelSerializer):
    code = serializers.IntegerField(allow_null=True, required=False)
    transport = serializers.PrimaryKeyRelatedField(
        queryset=Transports.objects.filter(available='yes'),
        allow_null=True,
        required=False
    )
    transporter_helper = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.filter(groups__name__in=('deliverers', 'mailmans')),
        allow_null=True,
        required=False
    )
    deliver_info = serializers.CharField(write_only=True, required=False, allow_blank=True, allow_null=True)
    step = None

    class Meta:
        model = WorkOrders
        fields = (
            'transporter',
            'transporter_helper',
            'current_step',
            'code',
            'transport',
            'deliver_info',
            )
        extra_kwargs = {
            'transporter': {'required': False},
            'current_step': {'required': False}
        }

    def validate_transporter(self, value):   
        if not value.groups.filter(name__in=('deliverers', 'mailmans')).exists():
            raise serializers.ValidationError('El usuario {} no tiene permitido recibir órdenes de trabajo'.format(value.first_name))
        child_steps = fetchChilden(self.instance.current_step)
        if not child_steps.filter(status__code__in=settings.ASSIGN_CODES).exists():
            raise serializers.ValidationError('No es posible asignar un móvil')
        # Al rompe porque de la forma correcta no funciona REVISAR EN ALGUN MOMENTO
        #self.initial_data['current_step'] = child_steps.filter(status__code__in=settings.ASSIGN_CODES).first().pk
        self.step = child_steps.filter(status__code__in=settings.ASSIGN_CODES).first()
        return value
    
    def validate_code(self, value):
        try:
            status = Status.objects.get(code=value)
        except Status.DoesNotExist:
            raise serializers.ValidationError('Código de estado no válido')
        child_steps = fetchChilden(self.instance.current_step).exclude(status__code__in=settings.ASSIGN_CODES)
        if not child_steps.filter(status=status).exists():
            raise serializers.ValidationError('No es posible cambiar el estado a {}'.format(status.name))
        if not self.instance.current_step.status.code == 7 and value == 9:
            raise serializers.ValidationError('No es generar una nueva OT a partir de esta {}'.format(status.name))
        self.step = child_steps.filter(status=status).first()
        return value

    def validate_transporter_helper(self, value):
        if value is None:
            return value
        if value.pk == self.initial_data.get('transporter'):
            raise serializers.ValidationError('El peoneta no puede ser el mismo conductor')
        return value

    def recreate_wo(self):
        new_wo = self.instance
        new_wo.pk = None
        new_wo.id = None
        new_wo.barcode = ''
        new_wo.transporter = None
        new_wo.current_step = WorkOrdersSteps.objects.filter(
            Q(flow__company=new_wo.owner.branch.company) | Q(flow__company=None),
            flow__has_return=new_wo.has_return,
            status__code=4
        ).order_by('-flow__company').order_by('id').first()
        new_wo.save()
        log_data = {
            'work_order': new_wo,
            'user': self.context.get('user'),
            'lng': self.context.get('user').branch.lng,
            'lat': self.context.get('user').branch.lat,
            'step': new_wo.current_step
        }
        WorkOrdersLog.objects.create(**log_data)
        # log_serializer = WorkOrdersLogSerializer(data=log_data)
        # log_serializer.is_valid(raise_exception=True)
        # log_serializer.save()

    @transaction.atomic
    def update(self, instance, validated_data):
        try:
            transport = validated_data.pop('transport')
        except KeyError:
            transport = None
        try:
            transporter_helper = validated_data.pop('transporter_helper')
        except KeyError:
            transporter_helper = None

        validated_data['current_step'] = self.step
        if transport:
            validated_data['current_plate'] = transport.plate
        if(validated_data.get('code') and validated_data.get('transporter')):
            raise serializers.ValidationError('Debe especificar móvil o estado, no ambas')
        ret = super().update(instance, validated_data)
        if validated_data.get('transporter'):
            send_wo_notification(ret)
        log_data = {
            'work_order': ret,
            'user': self.context.get('user'),
            'lng': self.context.get('user').branch.lng,
            'lat': self.context.get('user').branch.lat,
            'transporter': ret.transporter if validated_data.get('transporter') else None,
            'transporter_helper': transporter_helper if transporter_helper else None,
            'transport': transport if transport else None,
            'step': ret.current_step,
            'deliver_info': validated_data.get('deliver_info')
        }
        log_instance = WorkOrdersLog.objects.create(**log_data)
        # log_serializer = WorkOrdersLogSerializer(data=log_data)
        # log_serializer.is_valid(raise_exception=True)
        # log_serializer.save()
        if ret.current_step.send_email:
            send_wo_email(log_instance)
        channel_layer = get_channel_layer()
        # if validated_data.get('code') == 9:
        #     self.recreate_wo()
        async_to_sync(channel_layer.group_send)(
            'operators',
            {
                'type':'ot.creation'
            }
        )
        return ret

class WorkOrdersDelivererSerializer(serializers.ModelSerializer):
    barcode = serializers.CharField(max_length=50, required=True)
    code = serializers.IntegerField(allow_null=True, required=True)
    #work_order = WorkOrdersSerializer
    signature = Base64ImageField(max_length=None, use_url=True, allow_null=True, required=False)
    pictures = WOLogPictureSerializer(many=True)
    receiver_name = serializers.CharField(max_length=200, allow_blank=True, required=False)
    receiver_rut = serializers.CharField(max_length=50, allow_blank=True, required=False)

    class Meta:
        model = WorkOrdersLog
        fields = (
            'barcode',
            'code',
            'lat',
            'lng',
            'deliver_info',
            'signature',
            'work_order',
            'step',
            'pictures',
            'receiver_name',
            'receiver_rut',
        )
        extra_kwargs = {
            'step': {'required': False},
            'lat': {'required': True},
            'lng': {'required': True},
            'work_order': {'required': False},
        }
    
    # def validate_barcode(self, value):
        # try:
        #     return WorkOrders.objects.get(barcode=value).pk
        #     # self.initial_data['work_order'] = WorkOrders.objects.get(barcode=value).pk
        #     # return value
        # except WorkOrders.DoesNotExist:
        #     raise serializers.ValidationError('Código de barras inválido')

    def validate(self, data):
        try:
            data['work_order'] = WorkOrders.objects.get(barcode=data.get('barcode'))
            # self.initial_data['work_order'] = WorkOrders.objects.get(barcode=value).pk
            # return value
        except WorkOrders.DoesNotExist:
            raise serializers.ValidationError({'barcode': 'Código de barras inválido'})
        try:
            # work_order = WorkOrders.objects.get(pk=data.get('work_order').pk)
            status = Status.objects.get(code=data.get('code'))
        except Status.DoesNotExist:
            raise serializers.ValidationError({'code': 'Código de estado no válido'})
        child_steps = fetchChilden(data['work_order'].current_step).filter(status__code__in=settings.DELIVER_CODES)
        if not child_steps.filter(status=status).exists():
            # second try
            if data.get('code') == 6:
                status = Status.objects.get(code=3)
                if not child_steps.filter(status=status).exists():
                    status = Status.objects.get(code=9)
                    if not child_steps.filter(status=status).exists():
                        raise serializers.ValidationError({'code' : 'No es posible cambiar el estado a {}'.format(status.name)})
                # else:
                #     raise serializers.ValidationError({'code' : 'No es posible cambiar el estado a {}'.format(status.name)})
            else:
                raise serializers.ValidationError({'code' : 'No es posible cambiar el estado a {}'.format(status.name)})
        data['step'] = child_steps.filter(status=status).first()
        return data
        # self.initial_data['step'] = child_steps.filter(status=status).first().pk
        # return value

    @transaction.atomic
    def create(self, validated_data):
        fields_to_pop = ['code', 'barcode']
        wo = validated_data['work_order']
        if wo.current_step.is_delivery:
            wo.delivered_to_name = validated_data.get('receiver_name')
            wo.delivered_to_rut = validated_data.get('receiver_rut')
        wo.current_step = validated_data['step']
        wo.save()
        if 'receiver_name' in validated_data:
            validated_data.pop('receiver_name')
        if 'receiver_rut' in validated_data:
            validated_data.pop('receiver_rut')
        validated_data['user'] = self.context.get('user')
        for el in fields_to_pop:
            validated_data.pop(el)
        if 'code' in validated_data:
            validated_data.pop('code')
        if 'barcode' in validated_data:
            validated_data.pop('barcode')
        pictures = validated_data.pop('pictures')
        ret = super().create(validated_data)
        for picture in pictures:
            WorkOrdersLogPictures.objects.create(log=ret, **picture)
        if wo.current_step.send_email:
            send_wo_email(ret)
        if Step.objects.filter(wo_step=wo.current_step).exists():
            # services.send_firm(wo, ret)
            transaction.on_commit(lambda: services.send_firm.delay(wo.id, ret.id))
            transaction.on_commit(lambda: soap.soap.delay(wo.id, ret.id))
        return ret
    
    def to_representation(self, instance):
        return {
            'barcode': instance.work_order.barcode
        }

class WorkOrdersUserInfoSerializer(serializers.ModelSerializer):
    log = serializers.SerializerMethodField()
    company = serializers.SerializerMethodField()
    
    class Meta:
        model = WorkOrders
        fields = (
            'id',
            'created_at',
            'sender_full_name',
            'sender_address',
            'sender_address_number',
            'sender_comune',
            'sender_extra_info',
            'receiver_full_name',
            'receiver_address',
            'receiver_address_number',
            'receiver_comune',
            'receiver_phone',
            'has_return',
            'product_name',
            'service_name',
            'barcode',
            'segment',
            'log',
            'delivered_to_name',
            'delivered_to_rut',
            'extra_fields',
            'product_description',
            'company',
        )
    
    def get_log(self, instance):
        logs = WorkOrdersLog.objects.filter(work_order=instance.pk).order_by('-created_at')
        return WorkOrdersLogSerializer(logs, many=True, context=self.context).data

    def get_company(self, instance):
        return instance.owner.branch.company.commercial_business

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['segment'] = {
            'id': instance.segment.id,
            'name': instance.segment.name
        }
        return ret

def send_wo_email(log):
    info = {
        'barcode':log.work_order.barcode,
        'receiver_full_name': log.work_order.receiver_full_name,
        'step_detail': log.step_detail
    }
    if log.work_order.notification_type != 'none':
        if log.work_order.notification_type == 'sender':
            to = (log.work_order.sender_email,)
        elif log.work_order.notification_type == 'receiver':
            to = (log.work_order.receiver_email,)
        elif log.work_order.notification_type == 'both':
            to = (log.work_order.sender_email, log.work_order.receiver_email,)
        for el in to:
            send_email_async.delay(
                template='tracking.html',
                subject='Conexxion Seguimiento',
                to=[el,],
                email_data={'info': info},
            )

# CHECK
def send_wo_notification(wo):
    if wo.current_step.status.code == 2 or wo.current_step.to_sender: #retiro
        message_addres = '{} {}, {}'.format(
            wo.sender_address,
            wo.sender_address_number if wo.sender_address_number else '',
            wo.sender_comune,
        )
    elif wo.current_step.status.code == 5: #Entrega
        message_addres = '{} {}, {}'.format(
            wo.receiver_address,
            wo.receiver_address_number if wo.receiver_address_number else '',
            wo.receiver_comune,
        )

    body = '{} en {}'.format(
        wo.current_step.alt_name if wo.current_step.alt_name
            else wo.current_step.status.name,
        message_addres
    )
    send_notification.delay(body, wo.current_step.status.name, wo.barcode, wo.transporter.id)
