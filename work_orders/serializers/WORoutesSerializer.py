# django
from django.db import transaction
# rest_framework
from rest_framework import serializers

import datetime

from ..models import TransporterRoutes, WORoutes
from .WorkOrdersSerializer import WorkOrdersSerializer

class WORoutesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = WORoutes
        fields = (
            'work_order',
            'order',
        )

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['work_order'] = WorkOrdersSerializer(instance.work_order).data
        return ret

class TransporterRoutesSerializer(serializers.ModelSerializer):
    work_orders = WORoutesSerializer(many=True)

    class Meta:
        model = TransporterRoutes
        fields = (
            'id',
            'created_at',
            'created_by',
            'transporter',
            'polyline',
            'work_orders',
        )
        extra_kwargs = {
            'created_by': {'required': False}
        }

    def validate_transporter(self, value):
        try:
            TransporterRoutes.objects.get(
                transporter=self.initial_data['transporter'],
                created_at__date=datetime.datetime.now().date()
                )
            raise serializers.ValidationError("El móvil ya posee usa ruta para el día de hoy")
        except TransporterRoutes.DoesNotExist:
            return value

    @transaction.atomic
    def create(self, validated_data):
        wos = validated_data.pop('work_orders')
        validated_data['created_by'] = self.context.get('user')
        route = TransporterRoutes.objects.create(**validated_data)
        for el in wos:
            WORoutes.objects.create(**el, route=route)
        return route
