# rest_framework
from rest_framework import serializers

# models
from ..models import AssignManifestWOs, AssignManifest, WorkOrdersLog

# serializer
from .WorkOrdersSerializer import WorkOrdersSerializer

# etc
from utils.ExtraFields import Base64ImageField

class AssignManifestWOSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssignManifestWOs
        fields = '__all__'
        extra_kwargs = {
            'manifest': {'required': False}
        }

class AssignManifestSerializer(serializers.ModelSerializer):
    manifest_wos = AssignManifestWOSerializer(many=True)
    # map_picture = Base64ImageField(write_only=True, max_length=None, use_url=True)
    # orders = serializers.ListField(write_only=True, child=serializers.IntegerField())

    class Meta:
        model = AssignManifest
        fields = '__all__'

    def create(self, validated_data):
        # validated_data.pop('map_picture')
        # validated_data.pop('orders')
        manifest_wos = validated_data.pop('manifest_wos')
        instance = super().create(validated_data)
        as_wos = []
        for el in manifest_wos:
            as_wos.append(
                AssignManifestWOs(manifest=instance, **el)
            )
        AssignManifestWOs.objects.bulk_create(as_wos)
        return instance

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['user'] = "{} {}".format(
            instance.user.first_name,
            instance.user.last_name
        )
        ret['transporter'] = "{} {}".format(
            instance.transporter.first_name,
            instance.transporter.last_name
        )
        return ret

    
class WOAssignLogListSerializer(serializers.ModelSerializer):
    work_order = WorkOrdersSerializer()

    class Meta:
        model = WorkOrdersLog
        fields = '__all__'

