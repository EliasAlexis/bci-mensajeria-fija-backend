# django
from django.db.models import Q

# rest_framework
from rest_framework import serializers

# models
from ..models import WorkOrders
from servicing.models import Services, Products, Segments

# serializers
from .WorkOrdersSerializer import WorkOrdersSerializer

# etc
from utils.FieldsValidators import rut_validator

class WOUploadSerializer(WorkOrdersSerializer):

    has_return = serializers.ChoiceField(
        choices=['Sin Retorno', 'Con Retorno'],
        default='Sin Retorno'
    )
    sender_rut = serializers.CharField(required=False, allow_blank=True, validators=[rut_validator])
    receiver_rut = serializers.CharField(required=False, allow_blank=True, validators=[rut_validator])

    def __init__(self, *args, **kwargs):
        super(WOUploadSerializer, self).__init__(*args, **kwargs)
        self.fields['product'] = serializers.SlugRelatedField(
            slug_field='name',
            queryset=Products.objects.filter(
                Q(default='yes') | Q(company=self.context.get('owner').branch.company),
                status='active'
            )
        )
        self.fields['service'] = serializers.SlugRelatedField(
            slug_field='name',
            queryset=Services.objects.filter(
                Q(default='yes') | Q(company=self.context.get('owner').branch.company),
                status='active'
            )
        )

        self.fields['segment'] = serializers.SlugRelatedField(
            slug_field='name',
            queryset=Segments.objects.filter(
                Q(default='yes') | Q(company=self.context.get('owner').branch.company)
            )
        )

    class Meta:
        model = WorkOrders
        fields = '__all__'
        extra_kwargs = {
            'service_name': {'required': False},
            'current_step': {'required': False},
            'product_name': {'required': False},
            'owner': {'required': False},
        }
        read_only_fields = ('barcode',)

    def validate_has_return(self, value):
        return 'with_return' if value == "Con Retorno" else "no_return"

    def validate_sender_address(self, value):
        if value == 'no_mach':
            raise serializers.ValidationError("Dirección inválida")
        else:
            return value

    def validate_receiver_address(self, value):
        if value == 'no_mach':
            raise serializers.ValidationError("Dirección inválida")
        else:
            return value
