from rest_framework import permissions
from rest_framework_api_key.permissions import BaseHasAPIKey
from management.models import CompanyAPIKey

class CanViewOrMofidyWO(permissions.BasePermission):
    OPERATOR_PERMS = ('GET', 'HEAD', 'OPTIONS', 'PUT', 'POST', 'PATCH')
    USER_PERMS = ('GET', 'HEAD', 'OPTIONS', 'POST')
    def has_permission(self, request, view):
        if request.user.groups.filter(name__in=['operators','company_admins']).exists():
            return request.method in self.OPERATOR_PERMS
        if (request.user.groups.filter(name='users').exists() or
            request.headers.get('X-Auth-Type') == 'external'):
            return request.method in self.USER_PERMS
        return request.method in permissions.SAFE_METHODS

class ProcessUpdateOrSign(permissions.BasePermission):
    OPERATOR_PERMS = ('HEAD', 'OPTIONS', 'PATCH', 'PUT')
    TRANSPORTER_PERMS = ('HEAD', 'OPTIONS', 'POST',)
    def has_permission(self, request, view):
        if request.user.groups.filter(name='operators').exists():
            return request.method in self.OPERATOR_PERMS
        if request.user.groups.filter(name__in=('deliverers', 'mailmans', 'operators')).exists():
            return request.method in self.TRANSPORTER_PERMS

class HasAPIKey(BaseHasAPIKey):
    model = CompanyAPIKey

class IsOperatorUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.groups.filter(name='operators').exists())