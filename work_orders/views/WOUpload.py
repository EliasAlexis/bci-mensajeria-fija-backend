# django
from django.conf import settings
from django.http import HttpResponse
from django.db.models import Q

# rest_framework
from rest_framework.viewsets import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.settings import api_settings

# models
from ..models import WorkOrders, WorkOrdersLog
from servicing.models import Products, Services, Segments
from management.models import User

# serializers
from ..serializers.WOUploadSerializer import WOUploadSerializer

# permissions
from ..permissions import CanViewOrMofidyWO
from rest_framework.permissions import IsAuthenticated

#etc
from openpyxl import load_workbook
from openpyxl.utils.exceptions import InvalidFileException
import googlemaps
from googlemaps.exceptions import HTTPError
from io import BytesIO
import xlsxwriter
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

class WOUpload(generics.CreateAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanViewOrMofidyWO)
    serializer_class = WOUploadSerializer
    

    def get_serializer_context(self):
        if self.request.user.groups.filter(name='operators').exists():
            return {'calc_segment': False, 'owner': User.objects.filter(branch__company=self.request.data.get("company"), groups__name='company_admins').first()}
        return {'owner': self.request.user, 'calc_segment': False }
    

    def create(self, request, *args, **kwargs):
        excel_file = request.FILES.get("wo_file")
        if excel_file is None or excel_file == "":
            return Response(data={'wo_file': ['Archivo vacío']}, status=status.HTTP_400_BAD_REQUEST)
        
        
        try:
            wb = load_workbook(excel_file)
            worksheet = wb["WO"]
            new_wos = []
            gmaps = googlemaps.Client(key=settings.GMAPS_KEY)
            for row in worksheet.iter_rows(min_row=2):
                if row[0].value is None or row[0].value == '':
                    break
                try:
                    print("{},{}".format(
                        row[4].value,
                        row[6].value
                        ))
                    sender_geocode = gmaps.geocode("{},{}".format(
                        row[4].value,
                        row[6].value
                        ),
                        region="CL"
                    )

                    if len(sender_geocode) >= 1:
                        components = sender_geocode[0]['address_components']
                        sender_address = "{} {}".format(
                                next(
                                comp["long_name"] for comp in components
                                if comp["types"][0] == "route"
                                ),
                                next(
                                comp["long_name"] for comp in components
                                if comp["types"][0] == "street_number"
                                )
                            )
                        sender_comune = next(
                            comp['long_name'] for comp in components
                            if comp['types'][0] == "administrative_area_level_3"
                            )
                        sender_region = next(
                            comp["long_name"] for comp in components
                            if comp["types"][0] == "administrative_area_level_1"
                            )
                        sender_location = sender_geocode[0]["geometry"]["location"]
                    else:
                        sender_address = sender_comune = sender_region = 'no_mach'
                        sender_location = {}
                except (HTTPError, StopIteration):
                    sender_address = sender_comune = sender_region = 'no_mach'
                    sender_location = {}

                try:
                    notification_type ='none'
                    if  row[20].value =='Ninguna':
                        notification_type = 'none'
                    if  row[20].value =='Remitente': 
                        notification_type = 'sender'
                    if  row[20].value =='Destinatario': 
                        notification_type = 'receiver'
                    if  row[20].value =='Ambos': 
                        notification_type = 'both'

                    receiver_geocode = gmaps.geocode("{},{}".format(
                        row[11].value,
                        row[13].value
                        ),
                        region="CL"
                    )
                    if len(receiver_geocode) >= 1:
                        components = receiver_geocode[0]['address_components']
                        receiver_address = "{} {}".format(
                                next(
                                comp["long_name"] for comp in components
                                if comp["types"][0] == "route"
                                ),
                                next(
                                comp["long_name"] for comp in components
                                if comp["types"][0] == "street_number"
                                )
                            )
                        receiver_comune = next(
                            comp['long_name'] for comp in components
                            if comp['types'][0] == "administrative_area_level_3"
                            )
                        receiver_region = next(
                            comp["long_name"] for comp in components
                            if comp["types"][0] == "administrative_area_level_1"
                            )
                        receiver_location = receiver_geocode[0]["geometry"]["location"]
                    else:
                        #receiver_address = receiver_comune = receiver_region = 'no_mach'
                        # WORKARROUND DIRECCION DE ENTREGA
                        receiver_address = row[11].value
                        receiver_comune = row[13].value
                        receiver_region = "Metropolitana"
                        receiver_location = {}
                except (HTTPError, StopIteration):
                    #receiver_address = receiver_comune = receiver_region = 'no_mach'
                    # WORKARROUND DIRECCION DE ENTREGA
                    receiver_address = row[11].value
                    receiver_comune = row[13].value
                    receiver_region = "Metropolitana"
                    receiver_location = {}

                new_wos.append({
                    'sender_full_name': row[0].value,
                    'sender_email': row[1].value,
                    'sender_rut': row[2].value if row[2].value else "",
                    'sender_phone': row[3].value,
                    'sender_address': sender_address,
                    'sender_comune': sender_comune,
                    'sender_region': sender_region,
                    'sender_lat': round(sender_location.get("lat", 0), 7),
                    'sender_lng': round(sender_location.get("lng", 0), 7),
                    'sender_address_number': row[5].value if row[5].value else "",
                    'receiver_full_name': row[7].value,
                    'receiver_email': row[8].value,
                    'receiver_rut': row[9].value if row[9].value else "",
                    'receiver_phone': row[10].value,
                    'receiver_address': receiver_address,
                    'receiver_comune': receiver_comune,
                    'receiver_region': receiver_region,
                    'receiver_lat': round(receiver_location.get("lat", 0), 7),
                    'receiver_lng': round(receiver_location.get("lng", 0), 7),
                    'receiver_address_number': row[12].value if row[12].value else "",
                    'sender_extra_info': row[14].value if row[14].value else "",
                    'has_return': row[15].value,
                    'height': 0,
                    'width': 0,
                    'depth': 0,
                    'weight': 0,
                    'product': row[16].value,
                    'service': row[17].value,
                    'segment': row[18].value,
                    'product_description': row[19].value if row[19].value else "",
                    # 'notification_type': row[20].value,
                    'notification_type': notification_type,
                    # 'segment': 1,
                    # 'extra_fields': row[],
                })
        except (InvalidFileException, IndexError):
            return Response(data={'wo_file': ['Archivo inválido']}, status=status.HTTP_400_BAD_REQUEST)
        ser = self.get_serializer(data=new_wos, many=True)
        ser.is_valid(raise_exception=True)
        ser.save()
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'operators',
            {
                "type":"ot.creation"
            }
        )
        return Response(data=ser.data, status=status.HTTP_201_CREATED)

class WOUplodTemplate(generics.GenericAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        wo_sheet = workbook.add_worksheet("WO")
        # services_sheet = workbook.add_worksheet("Servicios")
        # products_sheet = workbook.add_worksheet("Productos")
        normal = workbook.add_format()
        normal_red = workbook.add_format({
            "font_color": "red"
        })
        heads = [{
                'required': True,
                'text': 'Remitente'
            }, {
                'required': True,
                'text': 'Email remitente'
            }, {
                'required': False,
                'text': 'RUT remitente'
            }, {
                'required': True,
                'text': 'Teléfono remitente'
            }, {
                'required': True,
                'text': 'Dirección remitente'
            }, {
                'required': False,
                'text': 'Dpto/Oficina remitente'
            }, {
                'required': True,
                'text': 'Comuna remitente'
            }, {
                'required': True,
                'text': 'Destinatario'
            }, {
                'required': True,
                'text': 'Email destinatario'
            }, {
                'required': False,
                'text': 'RUT destinatario'
            }, {
                'required': True,
                'text': 'Teléfono destinatario'
            }, {
                'required': True,
                'text': 'Dirección destinatario'
            }, {
                'required': False,
                'text': 'Dpto/Oficina destinatario'
            }, {
                'required': True,
                'text': 'Comuna destinatario'
            }, {
                'required': False,
                'text': 'Comentario'
            }, {
                'required': True,
                'text': 'Retorno'
            }, 
            # {
            #     'required': True,
            #     'text': 'Alto'
            # }, {
            #     'required': True,
            #     'text': 'Ancho'
            # }, {
            #     'required': True,
            #     'text': 'Largo'
            # }, {
            #     'required': True,
            #     'text': 'Peso'
            # },
            {
                'required': True,
                'text': 'Producto'
            }, {
                'required': True,
                'text': 'Servicio'
            },
            {
                'required': True,
                'text': 'Tramo'
            },
            {
                'required': False,
                'text': 'Descripción de Producto'
            },
             {
                'required': False,
                'text': 'Notificación'
            }
            ]
        for col_num, curr_head in enumerate(heads):
            if curr_head["required"]:
                wo_sheet.write(0, col_num, curr_head["text"], normal_red)
            else:
                wo_sheet.write(0, col_num, curr_head["text"], normal)
        
        wo_sheet.data_validation('P2:P300', {
            'validate': 'list',
            'source': ['Sin Retorno', 'Con Retorno']
        })

        wo_sheet.data_validation('U2:U300', {
            'validate': 'list',
            'source': ['Ninguna', 'Remitente', 'Destinatario', 'Ambos']
            # 'source': ['none', 'sender', 'receiver', 'both']
        })

        services = Services.objects.filter(Q(default='yes')|Q(company=request.user.branch.company), status='active').values('name')
        products = Products.objects.filter(Q(default='yes')|Q(company=request.user.branch.company), status='active').values('name')

        wo_sheet.data_validation('Q2:Q300', {
            'validate': 'list',
            'source': [el["name"] for el in products]
        })
        wo_sheet.data_validation('R2:R300', {
            'validate': 'list',
            'source': [el["name"] for el in services]
        })

        segments = Segments.objects.filter(Q(default='yes')|Q(company=request.user.branch.company), ).values('name')
        wo_sheet.data_validation('S2:S300', {
            'validate': 'list',
            'source': [el["name"] for el in segments]
        })

        workbook.close()
        output.seek(0)
        response = HttpResponse(
            output,
            content_type='application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename="WO_template.xlsx"'
        return response
