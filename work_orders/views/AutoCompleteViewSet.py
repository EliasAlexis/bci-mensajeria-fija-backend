#rest_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from rest_framework.settings import api_settings
from django_filters.rest_framework import DjangoFilterBackend
#serializers
from ..serializers.AutoCompleteSerializer import AutoCompleteSerializer
#models
from ..models import AutoComplete
#permissions
from ..permissionsAutoComplete import CanViewOrMofidyAC
#filters
from ..filters.autocomplete import AutocompleteFilter

class AutoCompleteViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanViewOrMofidyAC)
    serializer_class = AutoCompleteSerializer
    # queryset = AutoComplete.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = ('full_name', 'rut', 'address', 'comune', 'region', )
    ordering_fields = ('full_name', 'rut', 'address', 'comune', 'region', )
    filterset_class = AutocompleteFilter

    def get_serializer_context(self):
        return {"user": self.request.user}

    def get_queryset(self, *args, **kwargs):
        return AutoComplete.objects.filter(companysearch=self.request.user.branch.company)
