# django
from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend

# rest framework
from rest_framework import status, filters
from rest_framework.generics import ListAPIView

# etc
from io import BytesIO
import os
import datetime
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import json

from ..filters.workOrders import WOFilter

# model
from work_orders.models import WorkOrders

# reportlab
from reportlab.pdfgen import canvas
import reportlab.lib.units as units
from reportlab.graphics.barcode import qr, code128
from reportlab.graphics.shapes import Drawing, Line, Rect
from reportlab.graphics import renderPDF
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Image, Frame, Table, TableStyle, Paragraph, Spacer
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT

class WOPrintView(ListAPIView):
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend, )
    filterset_class = WOFilter
    queryset = WorkOrders.objects.select_related('segment').all()
    ordering_fields = (
        'receiver_comune',
    )

    # def get_queryset(self):
    #     wos_query = self.request.GET.get("barcodes").split(',')
    #     return WorkOrders.objects.select_related('segment').filter(barcode__in=wos_query)

    def list(self, request, *args, **kwargs):
        # definitions
        TICKET = (90 * units.mm, 100 * units.mm)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename=WO.pdf'
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer, pagesize=TICKET)
        WIDTH, HEIGHT = TICKET
        BARCODE_HEIGHT = 15 * units.mm
        main_frame = Frame(
            0,     # x
            BARCODE_HEIGHT,     # y at bottom
            WIDTH,
            HEIGHT - BARCODE_HEIGHT,
            leftPadding=4,
            rightPadding=4,
            topPadding=4,
            #showBoundary=1
        )
        barcode_frame = Frame(
            0,     # x
            0,     # y at bottom
            WIDTH,
            BARCODE_HEIGHT,
            leftPadding=4,
            rightPadding=4,
            topPadding=1,
            bottomPadding=1,
            #showBoundary=1
        )

        # styles
        STYLES = getSampleStyleSheet()
        STYLESN = STYLES['Normal']
        TEXT_CENTER = ParagraphStyle(
            name='center',
            parent=STYLES['Normal'],
            alignment=TA_CENTER,
            fontSize=7,
            leading=8
            )
        TEXT_RIGHT = ParagraphStyle(
            name='right',
            parent=STYLES['Normal'],
            alignment=TA_RIGHT,
            fontSize=9
            )
        TEXT_LEFT = ParagraphStyle(
            name='left',
            parent=STYLES['Normal'],
            alignment=TA_LEFT,
            leading=10,
            fontSize=9
            )
        TEXT_LEFT_WHITE = ParagraphStyle(
            name='left',
            parent=STYLES['Normal'],
            alignment=TA_LEFT,
            leading=15,
            textColor=colors.white,
            fontSize=11,
            backColor=colors.black
            )
        TEXT_LEFT_BIG = ParagraphStyle(
            name='left',
            parent=STYLES['Normal'],
            alignment=TA_LEFT,
            leading=12,
            fontSize=11,
            )

        wos = self.filter_queryset(self.get_queryset())
        # print(wos)
        for wo in wos:
            # header
            conexxion_logo_path = os.path.join(settings.STATIC_ROOT, 'images/logo.jpeg')
            conexxxion_logo = Image(conexxion_logo_path, 2.7*units.cm, 6.5*units.mm)
            # qr_code = qr.QrCode(
            #     'www.conexxion.cl',
            #     height=20*units.mm,
            #     width=20*units.mm,
            #     qrBorder=0,
            #     qrLevel='M'
            #     )
            qr_code = qr.QrCodeWidget(
                json.dumps({
                    "barcode": wo.barcode,
                    "signature": True
                })
            )
            bounds = qr_code.getBounds()
            q_width = bounds[2] - bounds[0]
            q_height = bounds[3] - bounds[1]
            qr_size = 20*units.mm
            d = Drawing(qr_size, qr_size, transform=[qr_size/q_width, 0, 0, qr_size/q_height, 0, 0])
            d.add(qr_code)
            renderPDF.draw(d, pdf, WIDTH - qr_size, HEIGHT - qr_size)

            # qr_t = Table([[qr_code]])
            # qr_t.setStyle(TableStyle([
            #     ('ALIGN', (0, 0), (-1, -1), 'RIGHT'),
            #     ]))
            
            # pdf.drawImage(qr_code, 0, 0)

            elements = []
            tbl_data = [
                [
                    conexxxion_logo
                ]
            ]
            header_table = Table(tbl_data, colWidths=(90 * units.mm))
            header_style = TableStyle([
                            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                            #('BACKGROUND', (0, 0), (-1, -1), colors.aqua),
                        ])
            header_table.setStyle(header_style)
            elements.append(header_table)
            elements.append(Spacer(WIDTH, 2*units.mm))
        
            # sender
            
            # SENDER_TABLE_WIDTHS = (7 * units.cm, 3 * units.cm)
            
            MIN_PADDING = TableStyle([
                        #('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                        #('BACKGROUND', (-1, -1), (-1, -1), colors.green),
                        #('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                        ('LEFTPADDING', (-1, -1), (-1, -1), 1),
                        ('RIGHTPADDING', (0, 0), (-1, -1), 1),
                        ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                        ('TOPPADDING', (0, 0), (-1, -1), 1),
                        ('BOTTOMPADDING', (0, 0), (-1, -1), 1)
                        ])
            SQUARE = Rect(
                0,
                0,
                5*units.mm,
                5*units.mm,
                strokeWidth=1,
                fillColor=colors.white
            )
            square_drawing = Drawing(5*units.cm, 5*units.mm)
            square_drawing.add(SQUARE)

            company_table = Table([
                [
                    Paragraph("<b>Empresa:</b>", TEXT_LEFT_BIG),
                    Paragraph(wo.owner.branch.company.commercial_business, TEXT_LEFT_BIG)
                ]
            ], colWidths=(22*units.mm, 48*units.mm))
            company_table.setStyle(MIN_PADDING)

            sender_name_table = Table([
                [
                    Paragraph("<b>Remitente:</b>", TEXT_LEFT),
                    Paragraph(wo.sender_full_name, TEXT_LEFT)
                ]
            ], colWidths=(20*units.mm, 50*units.mm))
            sender_name_table.setStyle(MIN_PADDING)

            sender_address_table = Table([
                [
                    Paragraph("<b>Dirección:</b>", TEXT_LEFT),
                    Paragraph(
                        "{}, {}, {}".format(
                            wo.sender_comune,
                            wo.sender_address,
                            wo.sender_address_number
                        ),
                        TEXT_LEFT
                    )
                ]
            ], colWidths=(20*units.mm, 70*units.mm))
            sender_address_table.setStyle(MIN_PADDING)

            sender_phone_table = Table([
                [
                    Paragraph("<b>Teléfono:</b>", TEXT_LEFT),
                    Paragraph(wo.sender_phone, TEXT_LEFT)
                ]
            ], colWidths=(20*units.mm, 70*units.mm))
            sender_phone_table.setStyle(MIN_PADDING)
            renderPDF.draw(square_drawing, pdf, WIDTH - 8*units.mm, HEIGHT - qr_size -7*units.mm)
            
            product_table = Table([
                [
                    Paragraph(
                        "<b>Producto:</b> {}".format(wo.product_name),
                        TEXT_LEFT),
                    Paragraph(
                        "<b>Servicio:</b> {}".format(wo.service_name),
                        TEXT_LEFT)
                ]
            ])
            product_table.setStyle(MIN_PADDING)
            return_table = Table([
                [
                    Paragraph(
                        "<b>Retorno:</b> {}".format(wo.get_has_return_display()),
                        TEXT_LEFT_WHITE if wo.has_return == 'with_return' else TEXT_LEFT),
                    Paragraph(
                        "<b>Tramo:</b> {}".format(wo.segment.name),
                        TEXT_LEFT)
                ]
            ])
            return_table.setStyle(MIN_PADDING)
            sender_address_inner = Table([
                    [company_table],
                    [sender_name_table],
                    [sender_address_table],
                    [sender_phone_table],
                    [product_table],
                    [return_table]
                ])
            sender_address_inner.setStyle(
                TableStyle([
                        #('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                        #('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                        ('LEFTPADDING', (0, 0), (-1, -1), 1),
                        ('RIGHTPADDING', (0, 0), (-1, -1), 1),
                        ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                        ('TOPPADDING', (0, 0), (-1, -1), 0),
                        ('BOTTOMPADDING', (0, 0), (-1, -1), 0)
                        ]))
        
            # sender_elems = [
            #     [
            #         sender_address_inner
            #     ]
            # ]
            # sender_table = Table(sender_elems, colWidths=SENDER_TABLE_WIDTHS)

            # sender_styles = TableStyle([
            #                 #('BACKGROUND', (0, 0), (-1, -1), colors.aqua),
            #                 #('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            #                 ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),
            #                 ('TOPPADDING', (0, 0), (-1, -1), 1),
            #                 ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
            #                 ])

            # sender_table.setStyle(sender_styles)
            elements.append(sender_address_inner)

            line = Line(0, 0, 8 * units.cm, 0, strokeWidth=0.5)
            line_separator = Drawing(
                8 * units.cm,
                1 * units.mm,
                transform=[1, 0, 0, 1, 3*units.mm, 0]
            )
            line_separator.add(line)
            elements.append(line_separator)

            # receiver

            receiver_address_inner = Table([
                    [
                        Paragraph("<b>Destinatario:</b>", TEXT_LEFT),
                        Paragraph(wo.receiver_full_name, TEXT_LEFT)
                    ],
                    [
                        Paragraph("<b>Dirección:</b>", TEXT_LEFT),
                        Paragraph(
                            "{}, {}, {}".format(
                                wo.receiver_comune,
                                wo.receiver_address,
                                wo.receiver_address_number
                            ),
                            TEXT_LEFT
                        )
                    ],
                    [
                        Paragraph("<b>Teléfono:</b>", TEXT_LEFT),
                        Paragraph(wo.receiver_phone, TEXT_LEFT),
                    ]
                ],
            colWidths=(22*units.mm, 68*units.mm),
            hAlign='LEFT'
            )
            receiver_address_inner.setStyle(
                TableStyle([
                    #('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    ('LEFTPADDING', (0, 0), (-1, -1), 1),
                    ('RIGHTPADDING', (0, 0), (-1, -1), 1),
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
                    ]))
            # receiver_elems = [
            #     [
            #         receiver_address_inner
            #     ]
            # ]
            # receiver_table = Table(receiver_elems)
            # receiver_table.setStyle(TableStyle([
            #     ('LEFTPADDING', (0, 0), (-1, -1), 2),
            #     ('RIGHTPADDING', (0, 0), (-1, -1), 2),
            # ]))
            elements.append(receiver_address_inner)
            elements.append(line_separator)

            # extra fields
            TEXT_LEFT_EXTRA = ParagraphStyle(
                name='left',
                parent=STYLES['Normal'],
                alignment=TA_LEFT,
                leading=7,
                fontSize=7,
                )
            extra_elems = []
            #print(type(wo.extra_fields))
            for key, value in wo.extra_fields.items():
                if key in ['print', 'picture', 'signature']:
                    continue
                extra_el = Table([
                        [
                            Paragraph("<b>{}:</b>".format(key[0:9]), TEXT_LEFT_EXTRA),
                            Paragraph("<b>{}</b>".format(value), TEXT_LEFT_EXTRA)
                        ]
                    ],
                    colWidths=(15*units.mm, 35*units.mm),
                    )

                extra_el.setStyle(
                    TableStyle([
                        # ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                        ('LEFTPADDING', (0, 0), (-1, -1), 2),
                        ('RIGHTPADDING', (0, 0), (-1, -1), 2),
                        ('TOPPADDING', (0, 0), (-1, -1), 1),
                        ('BOTTOMPADDING', (0, 0), (-1, -1), 1)
                    ])
                )
                
                extra_elems.append([extra_el,])
            
            extra_elems_table = [
                extra_elems[0:2],
                extra_elems[2:4]
            ]
            if len(extra_elems) >= 1:
                extra_table = Table(
                    extra_elems_table,
                    # colWidths=(23*units.mm, 73*units.mm),
                    hAlign='LEFT'
                )
                extra_table.setStyle(
                    TableStyle([
                        # ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                        ('LEFTPADDING', (0, 0), (-1, -1), 2),
                        ('RIGHTPADDING', (0, 0), (-1, -1), 2),
                        ('TOPPADDING', (0, 0), (-1, -1), 1),
                        ('BOTTOMPADDING', (0, 0), (-1, -1), 1)
                    ])
                )
                # elements.append(Spacer(WIDTH, 0.5*units.mm))
                # Eliminada tabla de campos extras
                # elements.append(extra_table)
                # elements.append(Spacer(WIDTH, 0.5*units.mm))
                # elements.append(line_separator)

            # comment
            comment_table = Table([
                [Paragraph("<b>OBSERVACION:</b>", TEXT_LEFT)],
                [Paragraph(wo.sender_extra_info, TEXT_LEFT)]
            ])
            comment_table.setStyle(
                TableStyle([
                    ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                    ('LEFTPADDING', (0, 0), (-1, -1), 2),
                    ('RIGHTPADDING', (0, 0), (-1, -1), 2),
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
                ])
            )
            elements.append(comment_table)

            # end barcode
            barcode_elements = []
            barcode = code128.Code128(value=wo.barcode, barWidth=0.4*units.mm, barHeight=9*units.mm)
            TEXT_BARCODE = ParagraphStyle(
                name='center',
                parent=STYLES['Normal'],
                alignment=TA_CENTER,
                leading=8,
                fontSize=9
            )
            barcode_table = Table([
                [
                    [
                        [barcode],
                        [Paragraph(wo.barcode, TEXT_BARCODE)]
                    ],
                    [
                        Paragraph(
                            "Fecha impresión <br/>"+ datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"),
                            TEXT_CENTER
                        )
                    ]
                ]
            ], hAlign='LEFT')
            barcode_table.setStyle(
                TableStyle([
                    #('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    #('BACKGROUND', (0, 0), (-1, -1), colors.aqua),
                    ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 1),

                ])
            )
            barcode_elements.append(barcode_table)

            main_frame.addFromList(elements, pdf)
            barcode_frame.addFromList(barcode_elements, pdf)
            main_frame._reset()
            barcode_frame._reset()
            pdf.showPage()
            wo.extra_fields["print"] = True
            wo.save()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'operators',
            {
                "type":"ot.creation"
            }
        )
        return response