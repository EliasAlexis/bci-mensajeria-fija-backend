# django
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import OuterRef, Subquery, F, Value, Case, When, DateTimeField, CharField, Count
from django.db.models.functions import Concat

# rest_framework
from rest_framework import filters
from rest_framework.settings import api_settings

# serializers
from ..serializers.WorkOrdersReportSerializer import WorkOrdersReportSerializer, WorkOrdersReportPdfSerializer, WorkOrdersElevatedReport

# models
from work_orders.models import WorkOrders, WorkOrdersLog
from management.models import TradersCompanies

# permissions
from rest_framework.permissions import IsAuthenticated
from ..permissions import CanViewOrMofidyWO

# filters
from ..filters.workOrders import WOFilter

# reportlab
from reportlab.lib.pagesizes import LEGAL

from utils.AbstractReport import AbstractReport

#etc
from utils.query_inspector import query_debugger


class WorkOrdersReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanViewOrMofidyWO)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = WOFilter
    search_fields = (
        'sender_full_name',
        'sender_rut',
        'sender_address',
        'sender_comune',
        'receiver_full_name',
        'receiver_rut',
        'receiver_address',
        'receiver_comune',
        'barcode',
    )
    ordering_fields = (
        'barcode',
        'created_at',
        'owner__branch__company__commercial_business',
        'receiver_full_name',
        'receiver_address',
        'sender_comune',
        'current_step__status__name',
        'last_log__created_at',
    )

    def get_columns(self):
        report_type = self.request.GET.get("report_type", "excel")
        if report_type == "pdf":
            self.page_size = LEGAL
            self.col_widths = (20, 22, 26, 42, 20, 26, 42, 20, 20, 19, 18, 20, 32, 22)
            return (
                'Código de Barras',
                'Fecha/Hora solicitud',
                'Solicitante',
                'Dirección origen',
                'Comuna origen',
                'Destinatario',
                'Direccion Destino',
                'Comuna destino',
                'Servicio',
                'Producto',
                'Tramo',
                'Último Estado',
                'Descripción de Producto',
                'Fecha Entrega',
            )

        if (self.request.user.is_staff or
            self.request.user.groups.filter(name='operators').exists()
        ):
            if report_type == "excel":
                return (
                    'Código de Barras',
                    'Fecha/Hora solicitud',
                    'Empresa',
                    'Solicitante',
                    'Teléfono origen',
                    'Dirección origen',
                    'Comuna origen',
                    'Destinatario',
                    'Teléfono destino',
                    'Dirección destino',
                    'Comuna destino',
                    'Servicio',
                    'Producto',
                    'Tramo',
                    'Retorno',
                    'Último Estado',
                    'Observación',
                    'Descripción de producto',
                    'Fecha asignación retiro',
                    'Conductor asignación retiro',
                    'Peoneta asignación retiro',
                    'Patente asignación retiro',
                    'Manifiesto asignación retiro',
                    'Fecha tránsito central',
                    'Fecha Central',
                    'Fecha asignación entrega',
                    'Conductor Fecha asignación entrega',
                    'Peoneta asignación entrega',
                    'Patente asignación entrega',
                    'Manifiesto asignación entrega',
                    'Fecha Entrega',
                    'Nombre Receptor',
                    'RUT Receptor',
                    'Coordenadas de Entrega',
                    'Fecha Entrega Fallida',
                    'Comentario Entrega Fallida',
                    'Fecha Central',
                    'Fecha asignación entrega (retorno)',
                    'Conductor Fecha asignación entrega (retorno)',
                    'Peoneta asignación entrega (retorno)',
                    'Patente asignación entrega (retorno)',
                    'Manifiesto asignación entrega (retorno)',
                    'Fecha Entrega (retorno)',
                    'Última observación',
                )
        if report_type == "excel":
            return (
                'Código de Barras',
                'Fecha/Hora solicitud',
                'Empresa',
                'Solicitante',
                'Teléfono origen',
                'Dirección origen',
                'Comuna origen',
                'Destinatario',
                'Teléfono Destino',
                'Dirección Destino',
                'Comuna destino',
                'Servicio',
                'Producto',
                'Tramo',
                'Retorno',
                'Último Estado',
                'Observación',
                'Descripción de Producto',
                'Fecha Entrega',
                'Última observación',
            )

    def get_serializer_class(self):
        report_type = self.request.GET.get("report_type", "excel")
        if report_type == "pdf":
            return WorkOrdersReportPdfSerializer
        if report_type == "excel":
            if (self.request.user.is_staff or
                self.request.user.groups.filter(name='operators').exists()
            ):
                return WorkOrdersElevatedReport
            return WorkOrdersReportSerializer

    def get_queryset(self):
        if (self.request.user.is_staff or
            self.request.user.groups.filter(name='operators').exists()
        ):

            pick_up_assignation_date = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=2, voided=False).values('created_at')[:1]
            )
            pick_up_transporter = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=2, voided=False).annotate(transporter_name=Concat(F('transporter__first_name'), Value(' '), F('transporter__last_name')))
                .values('transporter_name')[:1]
            )

            pick_up_transporter_helper = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=2, voided=False).annotate(transporter_name=Concat(F('transporter_helper__first_name'), Value(' '), F('transporter_helper__last_name')))
                .values('transporter_name')[:1]
            )

            pick_up_plate = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=2, voided=False)
                .values('transport__plate')[:1]
            )

            pick_up_report_serie = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=2, voided=False).values('manifest_wo_log__manifest__serie')[:1]
            )
            
            pick_up_date = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=3, voided=False).order_by('created_at').values('created_at')[:1]
            )
            central_date = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=4, voided=False).order_by('created_at').values('created_at')[:1]
            )
            delivery_assignation_date = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('created_at').values('created_at')[:1]
            )
            delivery_assignation_transporter = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).annotate(transporter_name=Concat(F('transporter__first_name'), Value(' '), F('transporter__last_name')))
                .values('transporter_name')[:1]
            )

            delivery_assignation_transporter_helper = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).annotate(transporter_name=Concat(F('transporter_helper__first_name'), Value(' '), F('transporter_helper__last_name')))
                .values('transporter_name')[:1]
            )

            delivery_assignation_plate = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False)
                .values('transport__plate')[:1]
            )

            delivery_assignation_report_serie = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).values('manifest_wo_log__manifest__serie')[:1]
            )

            delivery_date = Case(
                When(
                    has_return='with_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=3, voided=False).order_by('-created_at').values('created_at')[:1])
                ),
                When(
                    has_return='no_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=6, voided=False).order_by('created_at').values('created_at')[:1])
                ),
                output_field=DateTimeField()
            )

            delivery_location = Case(
                When(
                    has_return='with_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=3, voided=False).order_by('-created_at').annotate(coords=Concat(F('lat'), Value(','), F('lng'))).values('coords')[:1])
                ),
                When(
                    has_return='no_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=6, voided=False).order_by('created_at').annotate(coords=Concat(F('lat'), Value(','), F('lng'))).values('coords')[:1])
                ),
                output_field=CharField()
            )

            failed_delivery_date = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=7).order_by('created_at').values('created_at')[:1]
            )

            failed_delivery_comment = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=7).order_by('created_at').values('deliver_info')[:1]
            )

            central_return_date = Case(
                When(
                    has_return='with_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=4, voided=False).order_by('-created_at').values('created_at')[:1])
                ),
                default=None,
                output_field=DateTimeField()
            )
        
            delivery_return_assignation_date = Case(
                When(
                    has_return='with_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('-created_at').values('created_at')[:1])
                ),
                default=None,
                output_field=DateTimeField()
            )

            delivery_return_assignation_transporter = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('-created_at').annotate(qty=Count('pk'), transporter_name=Concat(F('transporter__first_name'), Value(' '), F('transporter__last_name'))).filter(qty=2).values('transporter_name')[:1]
            )

            delivery_return_assignation_transporter_helper = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('-created_at').annotate(qty=Count('pk'), transporter_name=Concat(F('transporter_helper__first_name'), Value(' '), F('transporter_helper__last_name'))).filter(qty=2).values('transporter_name')[:1]
            )

            delivery_return_assignation_plate = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('-created_at').annotate(qty=Count('pk')).filter(qty=2).values('transport__plate')[:1]
            )

            delivery_return_assignation_report_serie = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=5, voided=False).order_by('-created_at').annotate(qty=Count('pk')).filter(qty=2).values('manifest_wo_log__manifest__serie')[:1]
            )

            delivery_return_date = Case(
                When(
                    has_return='with_return',
                    then=Subquery(WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), step__status__code=6, voided=False).order_by('-created_at').values('created_at')[:1])
                ),
                default=None,
                output_field=DateTimeField()
            )

            last_comment = Subquery(
                WorkOrdersLog.objects.filter(work_order=OuterRef('pk'), voided=False).order_by('created_at').values('deliver_info')[:1]
            )

            return WorkOrders.objects.all().annotate(
            # return WorkOrders.objects.filter(barcode='C13336').annotate(
                pick_up_assignation_date=pick_up_assignation_date,
                pick_up_transporter=pick_up_transporter,
                pick_up_transporter_helper=pick_up_transporter_helper,
                pick_up_plate=pick_up_plate,
                pick_up_report_serie=pick_up_report_serie,
                pick_up_date=pick_up_date,
                central_date=central_date,
                delivery_assignation_date=delivery_assignation_date,
                delivery_assignation_transporter=delivery_assignation_transporter,
                delivery_assignation_transporter_helper=delivery_assignation_transporter_helper,
                delivery_assignation_plate=delivery_assignation_plate,
                delivery_assignation_report_serie=delivery_assignation_report_serie,
                delivery_date=delivery_date,
                central_return_date=central_return_date,
                delivery_return_assignation_date=delivery_return_assignation_date,
                delivery_return_assignation_transporter=delivery_return_assignation_transporter,
                delivery_return_assignation_transporter_helper=delivery_return_assignation_transporter_helper,
                delivery_return_assignation_plate=delivery_return_assignation_plate,
                delivery_return_assignation_report_serie=delivery_return_assignation_report_serie,
                delivery_return_date=delivery_return_date,
                company=F('owner__branch__company__commercial_business'),
                segment_name=F('segment__name'),
                last_status=F('current_step__status__name'),
                last_comment=last_comment,
                failed_delivery_date=failed_delivery_date,
                failed_delivery_comment=failed_delivery_comment,
                delivery_location=delivery_location
                ).values(
                    'barcode',
                    'created_at',
                    'company',
                    'sender_full_name',
                    'sender_phone',
                    'sender_address',
                    'sender_comune',
                    'receiver_full_name',
                    'receiver_phone',
                    'receiver_address',
                    'receiver_comune',
                    'service_name',
                    'product_name',
                    'segment_name',
                    'has_return',
                    'last_status',
                    'sender_extra_info',
                    'product_description',
                    'pick_up_assignation_date',
                    'pick_up_transporter',
                    'pick_up_transporter_helper',
                    'pick_up_plate',
                    'pick_up_report_serie',
                    'pick_up_date',
                    'central_date',
                    'delivery_assignation_date',
                    'delivery_assignation_transporter',
                    'delivery_assignation_transporter_helper',
                    'delivery_assignation_plate',
                    'delivery_assignation_report_serie',
                    'delivery_date',
                    'delivered_to_name',
                    'delivered_to_rut',
                    'delivery_location',
                    'failed_delivery_date',
                    'failed_delivery_comment',
                    'central_return_date',
                    'delivery_return_assignation_date',
                    'delivery_return_assignation_transporter',
                    'delivery_return_assignation_transporter_helper',
                    'delivery_return_assignation_plate',
                    'delivery_return_assignation_report_serie',
                    'delivery_return_date',
                    'last_comment',
                )
        if self.request.user.groups.filter(name='traders_supervisor').exists():
            return WorkOrders.objects.all().order_by('-created_at')
        if self.request.user.groups.filter(name='traders').exists():
            trader_companies = TradersCompanies.objects.filter(trader=self.request.user)
            return WorkOrders.objects.filter(
                owner__branch__company__in=[el.company for el in trader_companies]
            ).order_by('-created_at')
            
        if self.request.user.groups.filter(name='company_admins').exists():
            return WorkOrders.objects.filter(
                owner__branch__company=self.request.user.branch.company
                ).order_by('-created_at')
        if self.request.user.groups.filter(name__in=['deliverers', 'mailmans']).exists():
            return WorkOrders.objects.none()
        return WorkOrders.objects.filter(owner=self.request.user).order_by('-created_at')

    # @query_debugger
    # def list(self, request, *args, **kwargs):
    #     return super().list(request, args, kwargs)