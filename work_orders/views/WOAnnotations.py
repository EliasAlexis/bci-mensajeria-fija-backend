from rest_framework.viewsets import generics, ModelViewSet
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.settings import api_settings
from ..permissions import IsOperatorUser
from ..serializers.WOAnnotationSerializer import WOAnnotationSerializer, WOUndoSerializer
from ..models import WoAnnotation, WorkOrders

class WoAnnotations(ModelViewSet):
    serializer_class = WOAnnotationSerializer
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, (IsOperatorUser | IsAdminUser))
    pagination_class = None

    def get_queryset(self):
        try:
            WorkOrders.objects.get(pk=self.kwargs.get("wo_pk"))
        except WorkOrders.DoesNotExist:
            raise NotFound
        return WoAnnotation.objects.filter(wo_id=self.kwargs.get("wo_pk"))

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data["user"] = request.user.pk
        data["wo"] = kwargs.get("wo_pk")
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class UndoLastWoOperation(generics.CreateAPIView):
    serializer_class = WOUndoSerializer
    permission_classes = (IsAuthenticated, *api_settings.DEFAULT_PERMISSION_CLASSES, IsOperatorUser | IsAdminUser)

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data["user"] = request.user.pk
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
