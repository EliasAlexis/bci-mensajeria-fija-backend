# django
from django_filters.rest_framework import DjangoFilterBackend

# rest_framework
from rest_framework.viewsets import ModelViewSet, generics
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.settings import api_settings

# serializers
from ..serializers.WorkOrdersSerializer import (
    WorkOrdersSerializer,
    WorkOrdersProcessSerializer,
    WorkOrdersUserInfoSerializer,
    WorkOrdersDelivererSerializer,
)
from ..serializers.WorkOrdersExternalSerializer import WorkOrdersExternalSerializer
from ..serializers.WorkOrdersReportSerializer import WorkOrdersReportSerializer, WorkOrdersReportPdfSerializer, WorkOrdersElevatedReport

# models
from ..models import WorkOrders, WorkOrdersLog
from management.models import TradersCompanies, User, CompanyAPIKey

# permissions
from ..permissions import CanViewOrMofidyWO, ProcessUpdateOrSign, HasAPIKey
from rest_framework.permissions import IsAuthenticated
from rest_framework_api_key.permissions import KeyParser

# filters
from ..filters.workOrders import WOFilter

# reportlab
from reportlab.lib.pagesizes import LEGAL

from utils.AbstractReport import AbstractReport

#etc
from utils.MultipleFieldLookupMixin import MultipleFieldLookupMixin
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from utils.query_inspector import query_debugger

class WorkOrdersViewSet(MultipleFieldLookupMixin, ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, HasAPIKey | IsAuthenticated, CanViewOrMofidyWO)
    # serializer_class = WorkOrdersSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = WOFilter
    lookup_fields = ["pk", "barcode"]
    search_fields = (
        'sender_full_name',
        'sender_address',
        'receiver_full_name',
        'receiver_address',
        'barcode',
        'current_step__status__name',
        'owner__branch__company__commercial_business',
        'sender_extra_info',
    )
    ordering_fields = (
        'barcode',
        'created_at',
        'owner__branch__company__commercial_business',
        'receiver_full_name',
        'receiver_address',
        'sender_comune',
        'current_step__status__name',
        'sender_address',
        'service_name',
        'work_order_logs__created_at',
        'work_order_logs__transport__plate',
        'work_order_logs__transporter__first_name',
    )
    

    def get_serializer_class(self):
        if self.request.user.is_anonymous:
            return WorkOrdersExternalSerializer
        return WorkOrdersSerializer

    def get_queryset(self):
        if (self.request.user.is_staff or
            self.request.user.groups.filter(name__in=('operators', 'traders_supervisor')).exists()
        ):
            return WorkOrders.objects.all().order_by('-created_at')
        if self.request.user.groups.filter(name='company_admins').exists():
            return WorkOrders.objects.filter(
                owner__branch__company=self.request.user.branch.company
                ).order_by('-created_at')
        if self.request.user.groups.filter(name='traders').exists():
            trader_companies = TradersCompanies.objects.filter(trader=self.request.user)
            return WorkOrders.objects.filter(
                owner__branch__company__in=[el.company for el in trader_companies]
            ).order_by('-created_at')
        if self.request.user.groups.filter(name__in=['deliverers', 'mailmans']).exists():
            return WorkOrders.objects.filter(
                transporter=self.request.user,
                current_step__status__code__in=[2, 5]
                ).order_by('-created_at')
        if self.request.user.is_anonymous:
            company = CompanyAPIKey.objects.get_from_key(KeyParser().get(self.request)).company
            return WorkOrders.objects.filter(
                owner__branch__company=company
                ).order_by('-created_at')
        return WorkOrders.objects.filter(owner=self.request.user).order_by('-created_at')

    def get_serializer_context(self):
        if self.request.user.is_anonymous:
            company = CompanyAPIKey.objects.get_from_key(KeyParser().get(self.request)).company
            # print(company)
            return {'owner': User.objects.filter(branch__company=company, groups__name='company_admins').first()}
        if self.request.user.groups.filter(name='operators').exists():
            ## falta falidacion
            return {'calc_segment': False, 'owner': User.objects.filter(branch__company=self.request.data.get("company"), groups__name='company_admins').first()}
        return {'calc_segment': False, 'owner': self.request.user}

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data["segment"] = data.get("segment", 1)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'operators',
            {
                "type":"ot.creation"
            }
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class WorkOrdersProcess(generics.CreateAPIView, generics.UpdateAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, ProcessUpdateOrSign)
    #http_method_names = ['patch', 'put', 'head']
    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["user"] = self.request.user
        return context
    
    def get_object(self):
        barcode = self.request.data.get("barcode")
        try:
            if self.request.user.groups.filter(name='operators').exists():
                return WorkOrders.objects.get(barcode=barcode)
            return WorkOrders.objects.get(transporter=self.request.user, barcode=barcode)
        except WorkOrders.DoesNotExist:
            raise NotFound

    def get_queryset(self):
        barcodes = self.request.data.get("barcode")
        try:
            wos = WorkOrders.objects.filter(transporter=self.request.user, barcode__in=barcodes)
            invalid_wos = WorkOrders.objects.filter(
                transporter=self.request.user,
                barcode__in=barcodes,
                ).exclude(current_step__status__code=2)
            if invalid_wos.count() > 0:
                raise ValidationError({'barcode': 'No es posible procesar entregas en módo múltiple'})
        except ValueError:
            raise ValidationError({'barcode': 'Formato inválido'})

    def post(self, request, *args, **kwargs):
        many = request.data.get('mode', 'single') == 'many'
        # if many and self.request.user.groups.filter(name='operators').exists():
        #     raise ValidationError({'mode': 'No se permite asignación múltiple'})
        if many:
            self.get_queryset()
            incoming_data = []
            data = {**request.data}
            barcodes = data.pop('barcode')
            for el in barcodes:
                incoming_data.append({'barcode': el, **data})
        else:
            self.get_object()
            incoming_data = request.data
        serializer = self.get_serializer(data=incoming_data, many=many)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get_serializer_class(self, *args, **kwargs):
        if self.request.user.groups.filter(name='operators').exists():
            return WorkOrdersProcessSerializer
        return WorkOrdersDelivererSerializer

class WorkOrdersLogsView(generics.RetrieveAPIView):
    serializer_class = WorkOrdersUserInfoSerializer

    def get_serializer_class(self):
        if self.request.user.is_authenticated:
            return WorkOrdersUserInfoSerializer
        return WorkOrdersUserInfoSerializer

    def get_object(self):
        barcode = self.request.query_params.get("barcode")
        try:
            return WorkOrders.objects.get(barcode=barcode)
        except WorkOrders.DoesNotExist:
            raise NotFound

class WorkOrdersReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanViewOrMofidyWO)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = WOFilter
    search_fields = (
        'sender_full_name',
        'sender_rut',
        'sender_address',
        'sender_comune',
        'receiver_full_name',
        'receiver_rut',
        'receiver_address',
        'receiver_comune',
        'barcode',
    )
    ordering_fields = (
        'barcode',
        'created_at',
        'owner__branch__company__commercial_business',
        'receiver_full_name',
        'receiver_address',
        'sender_comune',
        'current_step__status__name',
        'last_log__created_at',
    )

    def get_columns(self):
        report_type = self.request.GET.get("report_type", "excel")
        if report_type == "pdf":
            self.page_size = LEGAL
            self.col_widths = (20, 22, 26, 42, 20, 26, 42, 20, 20, 19, 18, 20, 32, 22)
            return (
                'Código de Barras',
                'Fecha/Hora solicitud',
                'Solicitante',
                'Dirección origen',
                'Comuna origen',
                'Destinatario',
                'Direccion Destino',
                'Comuna destino',
                'Servicio',
                'Producto',
                'Tramo',
                'Último Estado',
                'Descripción de Producto',
                'Fecha Entrega',
            )

        if (self.request.user.is_staff or
            self.request.user.groups.filter(name='operators').exists()
        ):
            if report_type == "excel":
                return (
                    'Código de Barras',
                    'Fecha/Hora solicitud',
                    'Empresa',
                    'Solicitante',
                    'Teléfono origen',
                    'Dirección origen',
                    'Comuna origen',
                    'Destinatario',
                    'Teléfono destino',
                    'Dirección destino',
                    'Comuna destino',
                    'Servicio',
                    'Producto',
                    'Tramo',
                    'Retorno',
                    'Último Estado',
                    'Observación',
                    'Descripción de producto',
                    'Fecha asignación retiro',
                    'Conductor fecha asignación retiro',
                    'Fecha tránsito central',
                    'Fecha Central',
                    'Fecha asignación entrega',
                    'Conductor Fecha asignación entrega',
                    'Fecha Entrega',
                    'Fecha Central',
                    'Fecha asignación entrega (retorno)',
                    'Conductor Fecha asignación entrega (retorno)',
                    'Fecha Entrega (retorno)',
                    'Última observación',
                )
        if report_type == "excel":
            return (
                'Código de Barras',
                'Fecha/Hora solicitud',
                'Empresa',
                'Solicitante',
                'Teléfono origen',
                'Dirección origen',
                'Comuna origen',
                'Destinatario',
                'Teléfono Destino',
                'Dirección Destino',
                'Comuna destino',
                'Servicio',
                'Producto',
                'Tramo',
                'Retorno',
                'Último Estado',
                'Observación',
                'Descripción de Producto',
                'Fecha Entrega',
                'Última observación',
            )

    def get_serializer_class(self):
        report_type = self.request.GET.get("report_type", "excel")
        if report_type == "pdf":
            return WorkOrdersReportPdfSerializer
        if report_type == "excel":
            if (self.request.user.is_staff or
                self.request.user.groups.filter(name='operators').exists()
            ):
                return WorkOrdersElevatedReport
            return WorkOrdersReportSerializer

    def get_queryset(self):
        if (self.request.user.is_staff or
            self.request.user.groups.filter(name='operators').exists()
        ):
            return WorkOrders.objects\
                .prefetch_related('work_order_logs')\
                .prefetch_related('work_order_logs__transporter')\
                .select_related('owner')\
                .select_related('owner__branch')\
                .select_related('owner__branch__company')\
                .select_related('current_step')\
                .select_related('current_step__status')\
                .all().order_by('-created_at')
        if self.request.user.groups.filter(name='company_admins').exists():
            return WorkOrders.objects.filter(
                owner__branch__company=self.request.user.branch.company
                ).order_by('-created_at')
        if self.request.user.groups.filter(name__in=['deliverers', 'mailmans']).exists():
            return WorkOrders.objects.none()
        return WorkOrders.objects.filter(owner=self.request.user).order_by('-created_at')

    # @query_debugger
    # def list(self, request, *args, **kwargs):
    #     return super().list(request, args, kwargs)