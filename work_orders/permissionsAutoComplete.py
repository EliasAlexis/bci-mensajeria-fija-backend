from rest_framework import permissions

class CanViewOrMofidyAC(permissions.BasePermission):
    OPERATOR_PERMS = ('GET', 'HEAD', 'OPTIONS', 'PUT', 'POST', 'DELETE')
    def has_permission(self, request, view):
        if request.user.groups.filter(name='company_admins').exists():
            return request.method in self.OPERATOR_PERMS
        if request.user.groups.filter(name='users').exists():
            return request.method in permissions.SAFE_METHODS