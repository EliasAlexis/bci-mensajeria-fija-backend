# base
from django.urls import path, include, re_path
from rest_framework_nested import routers

# viewsets and views
from .views.WorkOrdersViewSet import WorkOrdersViewSet, WorkOrdersProcess, WorkOrdersLogsView
from .views.WorkOrdersReportsView import WorkOrdersReports
from .views.AutoCompleteViewSet import AutoCompleteViewSet
from .views.WOPrint import WOPrintView
from .views.WOUpload import WOUpload, WOUplodTemplate
from .views.WOAnnotations import UndoLastWoOperation, WoAnnotations
from .views.WOAssignReport import WOAssignReport, WOAssignReportList
from .views.WORoutesViews import WORoutesView
from .views.PrintSign import SignRequest

# websocket consumers
from .consumers import WONotifier, FallabSync
app_name = 'work_orders'

ROUTER = routers.SimpleRouter()
# ROUTER.register('wo', WorkOrdersViewSet, base_name='work_orders')
#ROUTER.register('wo_process', WorkOrdersProcess, base_name='work_orders_process')
ROUTER.register('autocomplete', AutoCompleteViewSet, base_name='search')

urlpatterns = [
    path('wo/log/', WorkOrdersLogsView.as_view(), name="work_orders_log"),
    path('wo/print/', WOPrintView.as_view(), name="work_orders_print"),
    path('wo/print/sign/', SignRequest.as_view(), name="work_orders_print_sign"),
    path('wo/reports/', WorkOrdersReports.as_view(), name="work_orders_reports"),
    path('wo/reports/list/', WOAssignReportList.as_view(), name="work_orders_reports_list"),
    path('wo_process/', WorkOrdersProcess.as_view(), name="work_orders_process"),
    path('wo/upload_template/', WOUplodTemplate.as_view(), name="work_orders_upload_template"),
    path('wo/upload/', WOUpload.as_view(), name="work_orders_excel_upload"),
    path('wo/undo/', UndoLastWoOperation.as_view(), name="work_orders_undo"),
    path('wo/assign/', WOAssignReport.as_view(), name="work_orders_assign_report"),
    path('wo/routes/', WORoutesView.as_view(), name="work_orders_routes"),
    path('wo/', WorkOrdersViewSet.as_view({'get': 'list', 'post': 'create'}), name="work_orders-list"),
    re_path(r'^wo/(?P<pk>[0-9]+)/$', WorkOrdersViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch':'partial_update'}), name='work_orders-detail'),
    re_path(r'^wo/(?P<barcode>C[0-9]+)/$', WorkOrdersViewSet.as_view({'get': 'retrieve', 'put': 'update'}), name='work_orders-detail'),
    re_path(r'^wo/(?P<wo_pk>[0-9]+)/annotations/', WoAnnotations.as_view({'get': 'list', 'post': 'create'}), name="work_orders_annotations"),
    path('', include(ROUTER.urls)),
]

websocket_urlpatterns = [
    path('wo_notifier/', WONotifier, name='work_orders_notifier'),
    path('fallab_sync/', FallabSync, name='fallabella_upload_sync'),
]
