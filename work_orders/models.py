import datetime
import unicodedata
import os
from django.db import models
from django.dispatch import receiver
from django.contrib.postgres.fields import JSONField
from django.core.validators import FileExtensionValidator
from management.models import User, Companies
from servicing.models import Services, Segments, Products, Transports

class Status(models.Model):
    name = models.CharField(max_length=200)
    code = models.PositiveIntegerField(unique=True)
    class Meta:
        db_table = 'status'

class WorkOrdersFlow(models.Model):

    def save(self, *args, **kwargs):
        if self.company is not None and self.default == 'yes':
            raise ValueError("No es posible generar asiganar un flujo de OT por defecto a un cliente")
        super(WorkOrdersFlow, self).save(*args, **kwargs)
            
    DEFAULT_CHOICES = (
        ('yes', 'Si'),
        ('no', 'no'),
    )
    HAS_RETURN = (
        ('no_return', 'Sin Retorno'),
        ('with_return', 'Con Retorno')
    )
    name = models.CharField(max_length=100)
    default = models.CharField(max_length=10, choices=DEFAULT_CHOICES, default='no')
    has_return = models.CharField(max_length=20, choices=HAS_RETURN, default='no_return')
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        null=True
    )
    class Meta:
        db_table = 'wos_flow'

class WorkOrdersSteps(models.Model):
    status = models.ForeignKey(
        Status,
        to_field='code',
        on_delete=models.CASCADE
    )
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True
    )
    alt_name = models.CharField(null=True, max_length=100)
    flow = models.ForeignKey(
        WorkOrdersFlow,
        on_delete=models.CASCADE
    )
    to_sender = models.BooleanField(default=False)
    send_email = models.BooleanField(default=False)
    is_delivery = models.BooleanField(default=False)
    class Meta:
        db_table = 'wos_flow_steps'

class WorkOrders(models.Model):

    def create_code(self, pk):
        return "C{}".format(10000 + pk)

    def save(self, *args, **kwargs):
        self.service_name = self.service.name
        self.product_name = self.product.name
        super(WorkOrders, self).save(*args, **kwargs)
        self.barcode = self.create_code(self.pk)
        super(WorkOrders, self).save(update_fields=['barcode'])
    
    NOTIFICATION_TYPE = (
        ('none', 'Ninguna'),
        ('sender', 'Remitente'),
        ('receiver', 'Destinatario'),
        ('both', 'Ambos'),
    )
    HAS_RETURN = (
        ('no_return', 'Sin Retorno'),
        ('with_return', 'Con Retorno')
    )
    created_at = models.DateTimeField(auto_now_add=True)
    # sender info
    sender_full_name = models.CharField(max_length=200)
    sender_rut = models.CharField(max_length=20, blank=True, default='')
    sender_email = models.EmailField(null=True, blank=True)
    sender_phone = models.CharField(blank=True, max_length=50, default='')
    sender_address = models.CharField(null=True, blank=True, max_length=300)
    sender_comune = models.CharField(null=True, blank=True, max_length=100)
    sender_region = models.CharField(null=True, blank=True, max_length=100)
    sender_address_number = models.CharField(blank=True, max_length=100, default='')
    sender_extra_info = models.CharField(blank=True, max_length=200, default='')
    product_description = models.CharField(blank=True, max_length=200, default='')
    sender_lat = models.DecimalField(max_digits=10, decimal_places=7)
    sender_lng = models.DecimalField(max_digits=10, decimal_places=7)

    # receiver info
    receiver_full_name = models.CharField(max_length=200)
    receiver_rut = models.CharField(max_length=20, blank=True, default='')
    receiver_email = models.EmailField(null=True, blank=True)
    receiver_phone = models.CharField(blank=True, max_length=50, default='')
    receiver_address = models.CharField(max_length=300)
    receiver_comune = models.CharField(max_length=100)
    receiver_region = models.CharField(max_length=100)
    receiver_address_number = models.CharField(blank=True, max_length=100, default='')
    receiver_lat = models.DecimalField(max_digits=10, decimal_places=7)
    receiver_lng = models.DecimalField(max_digits=10, decimal_places=7)

    # service info
    has_return = models.CharField(max_length=20, choices=HAS_RETURN, default='no_return')
    height = models.PositiveSmallIntegerField()
    width = models.PositiveSmallIntegerField()
    depth = models.PositiveSmallIntegerField()
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE
    )
    product_name = models.CharField(max_length=100)
    service = models.ForeignKey(
        Services,
        on_delete=models.CASCADE,
        related_name='work_order_service'
    )
    service_name = models.CharField(max_length=100)
    segment = models.ForeignKey(
        Segments,
        on_delete=models.CASCADE,
        related_name='work_order_segment'
    )
    current_step = models.ForeignKey(
        WorkOrdersSteps,
        on_delete=models.CASCADE
    )
    notification_type = models.CharField(max_length=20, choices=NOTIFICATION_TYPE, default='none')
    extra_fields = JSONField(null=True, blank=True)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='work_orders_owner'
    )
    transporter = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='work_orders_transporter',
        null=True
    )
    barcode = models.CharField(max_length=100, unique=True)
    delivered_to_name = models.CharField(max_length=200, null=True, blank=True)
    delivered_to_rut = models.CharField(max_length=20, null=True, blank=True)
    qty = models.PositiveSmallIntegerField(default=1)
    current_plate = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        db_table = 'work_orders'

class WorkOrdersLog(models.Model):

    def picture_path(self, filename):
        pass

    def image_path(self, filename):
        step_name = self.step.status.name if self.step.alt_name is None else self.step.alt_name
        return 'wo/{0}/{1}/signatures/{2}.{3}'.format(
            self.work_order.created_at.strftime("%d-%m-%Y"),
            self.work_order.barcode,
            unicodedata.normalize('NFKD', step_name).encode('ASCII', 'ignore').decode('utf8').replace(' ', '_'),
            filename.split('.',)[1])
    
    def save(self, *args, **kwargs):
        self.step_detail = self.step.alt_name if self.step.alt_name is not None else self.step.status.name
        super(WorkOrdersLog, self).save(*args, **kwargs)

    created_at = models.DateTimeField(auto_now_add=True)
    work_order = models.ForeignKey(
        WorkOrders,
        on_delete=models.CASCADE,
        related_name='work_order_logs',
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='user_log',
    )
    # deliver info
    deliver_info = models.CharField(null=True, blank=True, max_length=300)
    lat = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=7)
    lng = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=7)
    signature = models.FileField(
        db_column='signature',
        upload_to=image_path,
        validators=[FileExtensionValidator(allowed_extensions=['png', 'jpg', 'jpeg'])],
        blank=True,
        null=True
    )
    transporter = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='transporter_log',
        null=True
    )
    transporter_helper = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='transporter_helper_log',
        null=True
    )
    step = models.ForeignKey(
        WorkOrdersSteps,
        on_delete=models.SET_NULL,
        null=True
    )
    transport = models.ForeignKey(
        Transports,
        on_delete=models.SET_NULL,
        related_name='transport_log',
        null=True
    )
    step_detail = models.CharField(max_length=200)
    voided = models.BooleanField(default=False)

    class Meta:
        db_table = 'work_orders_log'

@receiver(models.signals.post_delete, sender=WorkOrdersLog)
def auto_delete_signature_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.signature:
        if os.path.isfile(instance.signature.path):
            os.remove(instance.signature.path)

class WorkOrdersLogPictures(models.Model):
    
    def picture_path(self, filename):
        step_name = self.log.step.status.name if self.log.step.alt_name is None else self.log.step.alt_name
        return "wo/{0}/{1}/pictures/{2}.{3}".format(
            self.log.work_order.created_at.strftime("%d-%m-%Y"),
            self.log.work_order.barcode,
            unicodedata.normalize('NFKD', step_name).encode('ASCII', 'ignore').decode('utf8').replace(' ', '_'),
            filename.split('.',)[1])

    picture = models.FileField(
        db_column='picture',
        upload_to=picture_path,
        validators=[FileExtensionValidator(allowed_extensions=['png', 'jpg', 'jpeg'])],
    )
    log = models.ForeignKey(
        WorkOrdersLog,
        on_delete=models.CASCADE,
        related_name='log_pictures',
    )

    class Meta:
        db_table = 'log_pictures'

@receiver(models.signals.post_delete, sender=WorkOrdersLogPictures)
def auto_delete_picture_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.picture:
        if os.path.isfile(instance.picture.path):
            os.remove(instance.picture.path)

class AutoComplete(models.Model):
    full_name = models.CharField(max_length=200)
    rut = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(null=True, blank=True, max_length=50)
    address = models.CharField(max_length=300)
    lat = models.DecimalField(max_digits=10, decimal_places=7)
    lng = models.DecimalField(max_digits=10, decimal_places=7)
    comune = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    address_number = models.CharField(null=True, blank=True, max_length=100)
    companysearch = models.ForeignKey(Companies, on_delete=models.CASCADE)

    class Meta:
        db_table = 'autocomplete'

class WoAnnotation(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    note = models.CharField(max_length=300)
    wo = models.ForeignKey(
        WorkOrders,
        on_delete=models.CASCADE,
        related_name='work_order_annotations'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    
    class Meta:
        db_table = 'work_order_annotations'

class TransporterRoutes(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="route_author"
    )
    transporter = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="route_consumer"
    )
    polyline = models.TextField()

    class Meta:
        db_table = 'transporter_routes'

class WORoutes(models.Model):
    work_order = models.ForeignKey(
        WorkOrders,
        on_delete=models.CASCADE,
        related_name="transporter_route_work_order"
    )
    route = models.ForeignKey(
        TransporterRoutes,
        on_delete=models.CASCADE,
        related_name="work_orders"
    )
    order = models.PositiveIntegerField()

    class Meta:
        db_table = 'wo_routes'
        ordering = ('order',)

class AssignManifest(models.Model):

    def pdf_path(self, filename):
        return 'manifests/{0}/{1}.pdf'.format(
            datetime.datetime.now().strftime("%d-%m-%Y"),
            self.transporter.username)

    def save(self, *args, **kwargs):
        super(AssignManifest, self).save(*args, **kwargs)
        self.serie = self.pk + 10000
        super(AssignManifest, self).save(update_fields=['serie'])

    created_at = models.DateTimeField(auto_now_add=True)
    serie = models.PositiveIntegerField(null=True)
    transporter = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='transporter_manifest'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='user_manifest'
    )
    route_polyline = models.TextField()
    pdf = models.FileField(
        upload_to=pdf_path,
        blank=True,
        null=True
    )

    class Meta:
        db_table = 'wo_manifests'

class AssignManifestWOs(models.Model):
    manifest = models.ForeignKey(
        AssignManifest,
        on_delete=models.CASCADE,
        related_name='manifest_wos'
    )
    order = models.PositiveIntegerField()
    wo_log = models.ForeignKey(
        WorkOrdersLog,
        on_delete=models.CASCADE,
        related_name='manifest_wo_log'
    )

    class Meta:
        db_table = 'wo_manifests__logs'