from django_filters import rest_framework as filters
from django.db.models import Exists, OuterRef
from management.models import Companies
from ..models import WorkOrders, Status, WorkOrdersLog, AssignManifestWOs, AssignManifest
from management.models import User

class NumberListFilter(filters.NumberFilter, filters.BaseInFilter):
    pass
class ModelMultipleChoiceListFilter(filters.ModelChoiceFilter, filters.BaseInFilter):
    pass

class WOFilter(filters.FilterSet):
    created_at__gte = filters.DateTimeFilter(field_name="created_at", lookup_expr='date__gte')
    created_at__lte = filters.DateTimeFilter(field_name="created_at", lookup_expr='date__lte')

    company = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        field_name='owner__branch__company'
    )
    company__in = ModelMultipleChoiceListFilter(
        queryset=Companies.objects.all(),
        field_name='owner__branch__company',
        lookup_expr='in'
    )

    branch = filters.NumberFilter(
        field_name='owner__branch',
    )

    excluded_companies = filters.ModelChoiceFilter(
        queryset=Companies.objects.all(),
        field_name='owner__branch__company',
        exclude=True
    )
    excluded_companies__in = ModelMultipleChoiceListFilter(
        queryset=Companies.objects.all(),
        lookup_expr='in',
        field_name='owner__branch__company',
        exclude=True
    )
    excluded_status = filters.NumberFilter(
        field_name='current_step__status__code',
        exclude=True
    )
    excluded_status__in = NumberListFilter(
        field_name='current_step__status__code',
        lookup_expr='in',
        exclude=True
    )

    status = filters.NumberFilter(
        field_name='current_step__status__code'
    )
    status__in = NumberListFilter(
        field_name='current_step__status__code',
        lookup_expr='in'
    )
    log_status = filters.ModelChoiceFilter(
        queryset=Status.objects.all(),
        field_name='work_order_logs__step__status'
    )
    # log_status = ModelMultipleChoiceListFilter(
    #     queryset=Status.objects.all(),
    #     lookup_expr='in',
    #     field_name='work_order_logs__step__status'
    # )
    # log_date = filters.DateTimeFilter(
    #     field_name='work_order_logs__created_at',
    #     lookup_expr='date',
    #     method='filter_log_date'
    # )
    transporter = filters.ModelChoiceFilter(
        queryset=User.objects.filter(groups=6),
        field_name='work_order_logs__transporter',
        distinct=True
    )
    transporter_all = filters.ModelChoiceFilter(
        queryset=User.objects.filter(groups=6),
        field_name='work_order_logs__transporter'
    )

    soc = filters.CharFilter(
        field_name='extra_fields__soc'
    )

    barcodes = filters.CharFilter(
        field_name='barcode',
        lookup_expr='in',
        method='filter_barcodes'
    )

    def filter_barcodes(self, queryset, name, value):
        return queryset.filter(
            barcode__in=value.split(',')
        )
    # # REVISAR PARA MULRIPLES ASIGNACIONES
    # def filter_log_date(self, queryset, name, value):
    #     return queryset.annotate(
    #         no_manifest=~Exists(AssignManifestWOs.objects.filter(wo_log__work_order=OuterRef('pk')))
    #     ).filter(
    #         work_order_logs__created_at__date=value,
    #         no_manifest=True
    #         )

    class Meta:
        model = WorkOrders
        fields = [
            'created_at__gte',
            'created_at__lte',
            'company',
            'company__in',
            'excluded_companies',
            'excluded_companies__in',
            'excluded_status',
            'excluded_status__in',
            'branch',
            'log_status',
            # 'log_date',
            'transporter',
            'transporter_all',
            'soc',
            'barcodes',
        ]

class CompanyFilter(filters.FilterSet):
    id = filters.NumberFilter(field_name="id")
    id__in = NumberListFilter(field_name="id", lookup_expr='in')
    id__exclude = NumberListFilter(field_name="id", lookup_expr='in', exclude=True)

    class Meta:
        model = Companies
        fields = ['id', 'id__in', 'id__exclude']


class WOLogFilter(filters.FilterSet):
    log_date = filters.DateTimeFilter(
        field_name='created_at',
        lookup_expr='date',
        method='filter_log_date'
    )
    transporter = filters.ModelChoiceFilter(
        queryset=User.objects.filter(groups=6),
        field_name='transporter'
    )

    def filter_log_date(self, queryset, name, value):
        return queryset.annotate(
            no_manifest=~Exists(AssignManifestWOs.objects.filter(wo_log=OuterRef('pk')))
        ).filter(
            created_at__date=value,
            no_manifest=True,
            voided=False,
            )

    class Meta:
        model = WorkOrdersLog
        fields = [
            'log_date',
            'transporter',
        ]

class ManifestFilter(filters.FilterSet):
    created_at = filters.DateTimeFilter(field_name="created_at", lookup_expr='date__gte')

    class Meta:
        model = AssignManifest
        fields = [
            'created_at',
            'transporter',
        ]