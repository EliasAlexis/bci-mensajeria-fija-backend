from work_orders.models import Status, WorkOrdersFlow, WorkOrdersSteps

# statuses
E_CANCELLED = Status.objects.create(
    name='Anulado',
    code=0
)
E_QUEUED = Status.objects.create(
    name='Solicitado',
    code=1
)
E_PICK_UP = Status.objects.create(
    name='Asignación retiro',
    code=2
)
E_ON_WAY = Status.objects.create(
    name='Tránsito a central',
    code=3
)
E_CENTRAL = Status.objects.create(
    name='Central',
    code=4
)
E_DELIVER = Status.objects.create(
    name='Asignación entrega',
    code=5
)
E_SUCCESS = Status.objects.create(
    name='Exitoso',
    code=6
)
E_FAILED = Status.objects.create(
    name='Fallido',
    code=7
)
E_CLOSED = Status.objects.create(
    name='Cerrado',
    code=9
)


## OT flows
## Estandar without return
EST_NO_RETURN = WorkOrdersFlow.objects.create(
    name='Estandar sin retorno',
    default='yes'
)

# queued
queued = WorkOrdersSteps.objects.create(
    status=E_QUEUED,
    flow=EST_NO_RETURN
)
WorkOrdersSteps.objects.create(
    status=E_CANCELLED,
    flow=EST_NO_RETURN,
    parent=queued
)

# assigned, pick up success/failed, last moment cancel
pick_up = WorkOrdersSteps.objects.create(
    status=E_PICK_UP,
    flow=EST_NO_RETURN,
    parent=queued
)
on_way = WorkOrdersSteps.objects.create(
    status=E_ON_WAY,
    flow=EST_NO_RETURN,
    parent=pick_up
)
pick_up_failed = WorkOrdersSteps.objects.create(
    status=E_FAILED,
    flow=EST_NO_RETURN,
    alt_name='Retiro fallido',
    parent=pick_up
)
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_NO_RETURN,
    parent=pick_up_failed
)
WorkOrdersSteps.objects.create(
    status=E_CANCELLED,
    flow=EST_NO_RETURN,
    parent=pick_up
)

# central, deliver
central = WorkOrdersSteps.objects.create(
    status=E_CENTRAL,
    flow=EST_NO_RETURN,
    parent=on_way
)
deliver = WorkOrdersSteps.objects.create(
    status=E_DELIVER,
    flow=EST_NO_RETURN,
    parent=central
)

# success/failed
WorkOrdersSteps.objects.create(
    status=E_SUCCESS,
    flow=EST_NO_RETURN,
    parent=deliver
)
failed = WorkOrdersSteps.objects.create(
    status=E_FAILED,
    flow=EST_NO_RETURN,
    alt_name='Entrega fallida, retorno a central',
    parent=deliver
)

# closed, return to sender
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_NO_RETURN,
    parent=failed
)
return_sender = WorkOrdersSteps.objects.create(
    status=E_DELIVER,
    flow=EST_NO_RETURN,
    alt_name='Devolución a solicitante',
    parent=failed,
    to_sender=True
)
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_NO_RETURN,
    parent=return_sender
)


EST_WITH_RETURN = WorkOrdersFlow.objects.create(
    name='Estandar con retorno',
    default='yes',
    has_return='with_return'
)
# queued
queued = WorkOrdersSteps.objects.create(
    status=E_QUEUED,
    flow=EST_WITH_RETURN
)
WorkOrdersSteps.objects.create(
    status=E_CANCELLED,
    flow=EST_WITH_RETURN,
    parent=queued
)

# assigned, pick up success/failed, last moment cancel
pick_up = WorkOrdersSteps.objects.create(
    status=E_PICK_UP,
    flow=EST_WITH_RETURN,
    parent=queued
)
on_way = WorkOrdersSteps.objects.create(
    status=E_ON_WAY,
    flow=EST_WITH_RETURN,
    parent=pick_up
)
pick_up_failed = WorkOrdersSteps.objects.create(
    status=E_FAILED,
    flow=EST_WITH_RETURN,
    alt_name='Retiro fallido',
    parent=pick_up
)
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_WITH_RETURN,
    parent=pick_up_failed
)
WorkOrdersSteps.objects.create(
    status=E_CANCELLED,
    flow=EST_WITH_RETURN,
    parent=pick_up
)

# central, deliver
central = WorkOrdersSteps.objects.create(
    status=E_CENTRAL,
    flow=EST_WITH_RETURN,
    parent=on_way
)
deliver = WorkOrdersSteps.objects.create(
    status=E_DELIVER,
    flow=EST_WITH_RETURN,
    parent=central
)

# central return success/fail
on_way = WorkOrdersSteps.objects.create(
    status=E_ON_WAY,
    flow=EST_WITH_RETURN,
    alt_name='Entrega exitosa, retorno a central',
    parent=deliver
)
failed = WorkOrdersSteps.objects.create(
    status=E_FAILED,
    flow=EST_WITH_RETURN,
    alt_name='Entrega fallida, retorno a central',
    parent=deliver
)
central = WorkOrdersSteps.objects.create(
    status=E_CENTRAL,
    flow=EST_WITH_RETURN,
    parent=on_way
)

# closed, return to sender
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_WITH_RETURN,
    parent=failed
)
return_sender = WorkOrdersSteps.objects.create(
    status=E_DELIVER,
    flow=EST_WITH_RETURN,
    alt_name='Devolución a solicitante',
    parent=failed,
    to_sender=True
)
WorkOrdersSteps.objects.create(
    status=E_CLOSED,
    flow=EST_WITH_RETURN,
    parent=return_sender
)

# success/failed
deliver = WorkOrdersSteps.objects.create(
    status=E_DELIVER,
    flow=EST_WITH_RETURN,
    alt_name='Entrega retorno',
    parent=central,
    to_sender=True
)
WorkOrdersSteps.objects.create(
    status=E_SUCCESS,
    flow=EST_WITH_RETURN,
    parent=deliver
)

print("Work orders flow created")
