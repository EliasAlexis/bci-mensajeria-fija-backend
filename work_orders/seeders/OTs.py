import random
from random import randint
from faker import Faker
from faker.providers import phone_number, internet, lorem
from work_orders.serializers.WorkOrdersSerializer import WorkOrdersSerializer, WorkOrdersProcessSerializer
from management.models import User

faker = Faker('es_ES')
faker.add_provider(internet)
faker.add_provider(phone_number)
faker.add_provider(lorem)
operator = User.objects.get(pk=2)

ots = []
for i in range(75):
    ots.append(
        {
            "sender_full_name": faker.name(),
            "sender_rut": str(randint(10, 99)) + str(randint(100, 999))
                                        + str(randint(100, 999))
                                        + str(randint(0, 9)),
            "sender_email": faker.ascii_free_email(),
            "sender_phone": faker.phone_number(),
            "sender_address": faker.address(),
            "sender_comune": "Puerto Montt",
            "sender_region": "Llanquihue",
            "sender_extra_info": faker.text(max_nb_chars=50),
            "receiver_full_name": faker.name(),
            "receiver_rut": str(randint(10, 99)) + str(randint(100, 999))
                                        + str(randint(100, 999))
                                        + str(randint(0, 9)),
            "receiver_email": faker.ascii_free_email(),
            "receiver_phone": faker.phone_number(),
            "receiver_address": faker.address(),
            "receiver_comune": "La Cisterna",
            "receiver_region": "Santiago",
            "has_return": random.choice(['no_return', 'with_return']),
            "height":randint(1, 10),
            "width":randint(1, 10),
            "depth":randint(1, 10),
            "weight":randint(1, 100),
            "product":"1",
            "service":random.choice([1, 2, 3]),
            "notification_type":"none",
            # "owner":  random.choice([5, 8]),
            "segment": random.choice([1, 2, 3, 4, 5])
        }
    )

wo_s = WorkOrdersSerializer(data=ots, many=True, context={'owner': random.choice([User.objects.get(pk=5), User.objects.get(pk=8)])})
wo_s.is_valid(raise_exception=True)
wo_s.save()

for ot in wo_s.instance:
    wop_s =  WorkOrdersProcessSerializer(
        ot,
        data={"barcode": ot.barcode, "transporter": 6 },
        context={"user": operator }
    )
    wop_s.is_valid(raise_exception=True)
    wop_s.save()