from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from rest_framework_api_key.models import AbstractAPIKey
from utils.OverwriteFileStorage import OverwriteFileStorage

class Companies(models.Model):
    # all left string while fixing weird frontend behavior
    TYPES = {
        "text": "string",
        "number": "string",
        "date": "string"
    }
    EXTRAS = [
        {"name": "print", "type": "text"},
        {"name": "signature", "type": "text"},
    ]

    def save(self, *args, **kwargs):
        schema = {
            "type": "object",
            "additionalProperties": False,
            "properties": {el["name"].lower() : {"type" : self.TYPES[el["type"]]} for el in (self.extra_fields + self.EXTRAS)}
        }
        self.extra_fields_schema = schema
        super(Companies, self).save(*args, **kwargs)

    COMPANY_TYPE = (
        ('client', 'Cliente'),
        ('owner', 'Propietario'),
    )
    COMPANY_STATUS = (
        ('active', 'Activo'),
        ('disabled', 'Desactivado'),
    )
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    commercial_business = models.CharField(max_length=100)
    rut = models.CharField(unique=True, max_length=15)
    company_type = models.CharField(max_length=15, choices=COMPANY_TYPE)
    max_users = models.PositiveIntegerField()
    max_branches = models.PositiveIntegerField()
    phone = models.CharField(max_length=100, null=True, blank=True)
    contact_name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    status = models.CharField(
        max_length=10,
        choices=COMPANY_STATUS,
        default='disabled'
        )
    billing_contacts = JSONField(null=True)
    extra_fields = JSONField(default=list)
    extra_fields_schema = JSONField(default=dict)
    needs_signature = models.BooleanField(default=False)
    needs_picture = models.BooleanField(default=False)

    class Meta:
        db_table = 'companies'

class CompanyAPIKey(AbstractAPIKey):
    company = models.OneToOneField(
        Companies,
        on_delete=models.CASCADE,
        related_name="api_keys",
    )

class Branches(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=150)
    address_number = models.CharField(max_length=150, null=True, blank=True)
    lat = models.DecimalField(max_digits=10, decimal_places=7)
    lng = models.DecimalField(max_digits=10, decimal_places=7)
    phone = models.CharField(max_length=100, null=True, blank=True, default=None)
    contact_name = models.CharField(max_length=150)
    email = models.EmailField(max_length=100, unique=True, null=True, blank=True)
    comune = models.CharField(max_length=150)
    province = models.CharField(max_length=150)
    region = models.CharField(max_length=150)
    max_users = models.PositiveIntegerField()
    company = models.ForeignKey(
        Companies,
        on_delete=models.CASCADE,
        related_name="company_branches"
    )

    class Meta:
        db_table = 'company_branches'

def validate_image(image):
    print(image.size)
    file_size = image.size
    limit_kb = 500
    limit_w = 800
    limit_h = 800
    if file_size > (limit_kb * 1024):
        raise ValidationError("Tamaño máximo de imágen {} KB".format(limit_kb))
    w, h = get_image_dimensions(image)
    if w > limit_w or h > limit_h:
        raise ValidationError("La imagen no debe ser mayor a {}px * {}px".format(limit_w, limit_h))

class User(AbstractUser):

    def profile_picture_name(self, filename):
        return "profiles/profile_{}.jpg".format(self.pk)

    USER_STATUS = (
        ('active', 'Activo'),
        ('disabled', 'Desactivado'),
        ('locked', 'Bloqueado'),
    )
    rut = models.CharField(unique=True, max_length=15, null=True)
    status = models.CharField(max_length=10, choices=USER_STATUS)
    phone = models.CharField(max_length=100, null=True, blank=True, default=None)
    email = models.EmailField(max_length=100, unique=True)
    profile_picture = models.ImageField(
        upload_to=profile_picture_name,
        storage=OverwriteFileStorage(),
        blank=True,
        null=True,
        validators=[validate_image,]
    )
    branch = models.ForeignKey(
        Branches,
        on_delete=models.CASCADE,
        related_name="branches_users"
    )

    @property
    def group_name(self):
        return "user_{}".format(self.id)

    class Meta:
        db_table = 'users'

class Token(models.Model):
    token = models.CharField(max_length=255)

    class Meta:
        db_table = 'tokens'

    def __str__(self):
        return 'Token: {}'.format(self.token)

class SidebarMenuElems(models.Model):
    path = models.CharField(max_length=500, blank=False, null=False)
    title = models.CharField(max_length=500, blank=False, null=False)
    icon = models.CharField(max_length=500, blank=False, null=False)
    css_class = models.CharField(max_length=500, null=True, blank=True)
    extralink = models.BooleanField(blank=False, null=False, default=False)
    #permission = models.ForeignKey(
    #    Permission,
    #    on_delete=models.CASCADE
    #)
    parent = models.ForeignKey(
        'self',
        null=True,
        on_delete=models.SET_NULL
    )
    parent_available = models.BooleanField(blank=False, null=False, default=False)
    class Meta:
        db_table = 'sidebar_menu'

class GroupsHierarchy(models.Model):
    
    parent_group = models.ForeignKey(
        Group,
        related_name='parent_group',
        on_delete=models.CASCADE
    )
    child_group = models.ForeignKey(
        Group,
        related_name='child_group',
        on_delete=models.CASCADE
    )
    company_type = models.CharField(max_length=20, choices=Companies.COMPANY_TYPE, default='client')
    class Meta:
        db_table = 'groups_hierarchy'

class TradersCompanies(models.Model):
    trader = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    company = models.OneToOneField(
        Companies,
        on_delete=models.CASCADE,
        related_name='trader_company'
    )

    class Meta:
        db_table = 'traders_companies'

class AuditEntry(models.Model):
    action = models.CharField(max_length=64)
    ip = models.GenericIPAddressField(null=True)
    username = models.CharField(max_length=256, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '{0} - {1} - {2}'.format(self.action, self.username, self.ip)

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.action, self.username, self.ip)


@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):
    if request.META.get('HTTP_X_FORWARDED_FOR'):
        ip = request.META.get('HTTP_X_FORWARDED_FOR').split(",", 1)[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    AuditEntry.objects.create(action='user_logged_in', ip=ip, username=user.username)

@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):  
    ip = request.META.get('REMOTE_ADDR')
    AuditEntry.objects.create(action='user_logged_out', ip=ip, username=user.username)

@receiver(user_login_failed)
def user_login_failed_callback(sender, credentials, request, **kwargs):
    if request.META.get('HTTP_X_FORWARDED_FOR'):
        ip = request.META.get('HTTP_X_FORWARDED_FOR').split(",", 1)[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    AuditEntry.objects.create(action='user_login_failed', ip=ip, username=credentials.get('username', None))
