# django
from django.contrib.auth.models import Group
# rest framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, ListAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework import status, filters
from rest_framework.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from django_filters.rest_framework import DjangoFilterBackend
from guardian.shortcuts import get_objects_for_user

# models
from ..models import User, Branches, SidebarMenuElems, GroupsHierarchy, Companies

# serializers
from ..serializers.UserSerializer import (
        UserSerializer,
        ChangePasswordSerializer,
        UserRenderInfoSerializer,
        UserSelfSerializer,
        ActivateUserSerializer,
        GroupsHierarchySerializer,
        UserSelfPictureSerializer,
        UserOperatorSerializer
    )

# etc
from utils.Permissions import ReadOnly
from ..permissions import CanAddModifyUsers

class UserViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanAddModifyUsers,)
    http_method_names = ['get', 'head', 'post', 'patch']
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = ('username', 'first_name', 'last_name', 'rut', 'email')
    ordering_fields = ('username', 'first_name', 'last_name', 'rut', 'email')
    filterset_fields = ['groups', 'branch', 'status', ]

    def get_serializer_context(self):
        return {'user': self.request.user}

    def get_queryset(self, *args, **kwargs):
        try:
            company = int(self.kwargs['company_pk'])
        except ValueError:
            return User.objects.none()
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('traders', 'operators', )).exists():
            return User.objects.filter(branch__company=company).order_by('-id')
        elif self.request.user.branch.company.id == company:# \
                #and self.request.user.has_perm('management.change_user'):
            if self.request.user.groups.filter(name='company_admins').exists():
                return User.objects.filter(branch__company=company).order_by('-id')
            else:
                return User.objects.filter(
                    branch=self.request.user.branch
                    ).order_by('-id')
        else:
            return User.objects.none()

    def get_serializer_class(self, *args, **kwargs):
        if self.request.user.groups.filter(name='operators').exists():
            return UserOperatorSerializer
        return UserSerializer

    def can_belong_to_branch(self, branch_pk, user=None):
        company = Companies.objects.get(pk=self.kwargs['company_pk'])
        users_qty = User.objects.filter(branch__company=company).exclude(status='locked').count()
        if user:
            if not user.status == 'locked':
                users_qty -= 1
                if branch_pk == user.branch.pk:
                    return
        try:
            Branches.objects.get(pk=branch_pk, company=self.kwargs['company_pk'])
        except Exception:
            raise ValidationError(detail={'branch': ['La sucursal no pertenece a la compañía']})
        if (company.max_users == users_qty) and company.max_users != 0:
            raise ValidationError(detail={'max_users': ['Ha alcanzado el número máximo de usuarios']})

    # def create_new_user(self, data):
    #     data['status'] = 'disabled'
    #     serializer = self.get_serializer(data=data, context={'user': self.request.user})
    #     serializer.is_valid(raise_exception=True)
    #     self.can_belong_to_branch(data['branch'])
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    def create(self, request, *args, **kwargs):
        request.data['status'] = 'disabled'
        serializer = self.get_serializer(data=request.data, context={'user': self.request.user})
        serializer.is_valid(raise_exception=True)
        self.can_belong_to_branch(request.data['branch'])
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def partial_update(self, request, *args, **kwargs):
        self.can_belong_to_branch(
            request.data['branch'],
            user=self.get_object())
        serializer = self.get_serializer(
            self.get_object(),
            data=request.data,
            partial=True,
            context={'user': request.user}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )

class UserUpdatePassword(APIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)

    def patch(self, request):
        serializer = ChangePasswordSerializer(data=request.data, context={'user': request.user})
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(pk=request.user.id)
        user.set_password(serializer.data["password"])
        user.save()
        return Response(
            data={'success': ['Contraseña actualizada con éxito']},
            status=status.HTTP_200_OK
        )

class UserSelfView(RetrieveUpdateAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = UserSelfSerializer

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

class UserSelfPictureUpload(UpdateAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = UserSelfPictureSerializer

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

class UserRenderInfo(ListCreateAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = UserRenderInfoSerializer

    def get_queryset(self):
        return get_objects_for_user(self.request.user, 'management.view_sidebarmenuelems', with_superuser=False).order_by('id')

    def fetch_childen(self, parent_pk):
        childs = self.get_queryset().filter(parent=parent_pk)
        if len(childs) == 0:
            return []
        childs_dict = self.get_serializer(childs, many=True).data
        for current in childs_dict:
            current["children"] = self.fetch_childen(current["id"])
        return childs_dict

    def list(self, request):
        objects = self.get_queryset()
        # Fist level
        render_list = self.get_serializer(objects.filter(parent=None), many=True).data
        # Iterate over levels
        for current in render_list:
            current["children"] = self.fetch_childen(current["id"])
        return Response(render_list)

    def create(self, request):
        try:
            obj = SidebarMenuElems.objects.filter(path=request.data.get("segment")).first()
            if not request.user.has_perm('management.view_sidebarmenuelems', obj):
                raise PermissionDenied
            return Response(status=status.HTTP_200_OK)
        except SidebarMenuElems.DoesNotExist:
            raise PermissionDenied

class ActivateUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = ActivateUserSerializer(
                data=request.data,
                context={
                    "user": request.user
                }
            )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        refresh = RefreshToken.for_user(request.user)
        return Response(
            data={'access': str(refresh.access_token)},
            status=status.HTTP_200_OK
        )

class GroupsHierarchyViews(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = GroupsHierarchySerializer
    pagination_class = None

    def get_queryset(self, *args, **kwargs):
        available_groups = GroupsHierarchy.objects.filter(
            parent_group__in=self.request.user.groups.all()
        )
        return [group_hierarchy.child_group for group_hierarchy in available_groups]