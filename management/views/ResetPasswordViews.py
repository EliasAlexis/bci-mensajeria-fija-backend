# rest framework
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
# models
from ..models import User, Token
# Serializers
from ..serializers.ResetPasswordSerializers import (
    ResetPasswordSerializer,
    ResetPasswordCompleteSerializer,
    )
from ..serializers.UserSerializer import UserSerializer
# Permissions
from rest_framework.permissions import AllowAny
# Others
from utils.Utils import token_encode, send_email_async


class ResetPasswordView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(email=serializer.data['email'])
        token = token_encode('reset_password', user)
        send_email_async.delay(
            template='reset_password.html',
            subject='Recuperar Contraseña',
            to=[user.email,],
            email_data={
                'token':token,
                'user':UserSerializer(user).data
            }
            )
        return Response(
            data={'success':['Se ha enviado un enlace de recuperación a su correo']},
            status=status.HTTP_200_OK
        )

    def put(self, request):
        serializer = ResetPasswordCompleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = Token.objects.get(token=serializer.validated_data['token'])
        user = User.objects.get(email=serializer.validated_data['token_decode']['user_email'])
        user.set_password(serializer.validated_data['password'])
        user.save()
        token.delete()
        return Response(
            data={'success':['Su contraseña ha sido actualizada con éxito']},
            status=status.HTTP_200_OK
        )