# rest_frameword
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from rest_framework.settings import api_settings

# models
from ..models import Companies, Branches, TradersCompanies, User

# serializers
from ..serializers.CompaniesReportSerializer import CompaniesReportSerializer, BranchesReportSerializer, UsersReportSerializer

# report class
from utils.AbstractReport import AbstractReport

class CompaniesReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = CompaniesReportSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'rut', 'company_type', 'commercial_business',)

    columns = (
        'RUT',
        'Razón Social',
        'Nombre de Fantansía',
        'Tipo Empresa'
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff:
            return Companies.objects.all().order_by('-id')
        if self.request.user.groups.filter(name='traders').exists():
            trader_companies = [item.company.pk for item in TradersCompanies.objects.filter(trader=self.request.user)]
            return Companies.objects.filter(pk__in=trader_companies)
        return Companies.objects.filter(pk=self.request.user.branch.company_id)

class BranchesReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = BranchesReportSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'address', )

    columns = (
        'Nombre',
        'Dirección',
        'Comuna',
        'Región',
        'Nombre Contacto',
        'Teléfono',
        'E-mail'
    )

    def get_queryset(self, *args, **kwargs):
        try:
            company = int(self.kwargs['company_pk'])
        except ValueError:
            return Branches.objects.none()
        if self.request.user.is_staff or self.request.user.groups.filter(name='traders').exists():
            return Branches.objects.filter(company=company).order_by('-id')
        elif self.request.user.branch.company.id == company:
            return Branches.objects.filter(company=company).order_by('-id')
        else:
            return Branches.objects.none()

class UsersReports(AbstractReport):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = UsersReportSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('username', 'first_name', 'last_name', 'rut', 'email')

    columns = (
        'Rut',
        'Nombre',
        'Apellido',
        'Usuario',
        'Teléfono',
        'E-mail',
        'Sucursal'
    )

    def get_queryset(self, *args, **kwargs):
        try:
            company = int(self.kwargs['company_pk'])
        except ValueError:
            return User.objects.none()
        if self.request.user.is_staff or self.request.user.groups.filter(name='traders').exists():
            return User.objects.filter(branch__company=company).order_by('-id')
        elif self.request.user.branch.company.id == company:
            if self.request.user.groups.filter(name__in=('company_admins', 'operators')).exists():
                return User.objects.filter(branch__company=company).order_by('-id')
            else:
                return User.objects.filter(
                    branch=self.request.user.branch
                    ).order_by('-id')
        else:
            return User.objects.none()