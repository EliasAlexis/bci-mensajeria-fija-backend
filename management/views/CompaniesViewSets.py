# rest framework
from rest_framework.viewsets import ModelViewSet, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import status, filters
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.settings import api_settings
# models
from ..models import Companies, Branches, TradersCompanies
# serializers
from ..serializers.CompaniesSerializers import (
        CompanySerializer,
        CompanyAdminSerializer,
        BranchesSerializer,
        CompanyOperatorSerializer,
        BranchesOperatorSerializer
    )
# permissions
from ..permissions import CanModifyCompanies
# etc
from functools import reduce


class CompaniesViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanModifyCompanies)
    http_method_names = ['get', 'head', 'post', 'put', 'patch']
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'rut', 'company_type', 'commercial_business')
    ordering_fields = ('name', 'rut', 'company_type', 'commercial_business')

    def get_serializer_class(self, *args, **kwargs):
        if self.request.user.is_staff:
            return CompanyAdminSerializer
        if self.request.user.groups.filter(name='operators').exists():
            return CompanyOperatorSerializer
        return CompanySerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('operators', 'traders_supervisor')).exists():
            return Companies.objects.all().order_by('-id')
        if self.request.user.groups.filter(name='traders').exists():
            trader_companies = [item.company.pk for item in TradersCompanies.objects.filter(trader=self.request.user)]
            return Companies.objects.filter(pk__in=trader_companies)
        return Companies.objects.filter(pk=self.request.user.branch.company_id)

    def create(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            raise PermissionDenied
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            raise PermissionDenied
        serializer = self.get_serializer(
            self.get_object(),
            data=request.data,
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )

class BranchesViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanModifyCompanies)
    http_method_names = ['get', 'head', 'post', 'put']
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'address', )
    ordering_fields = ('name', 'address', 'comune', 'region')

    def get_serializer_class(self, *args, **kwargs):
        if self.request.user.groups.filter(name='operators').exists():
            return BranchesOperatorSerializer
        return BranchesSerializer

    def can_save_branch(self, request):
        branches = self.get_queryset()
        branch_pk = self.kwargs.get("pk")
        company = Companies.objects.get(pk=self.kwargs['company_pk'])
        if (branches.count() >= company.max_branches
                and branch_pk is None) and not company.max_branches == 0:
            raise ValidationError(
                detail={'max_branches': ['Ha alcanzado su número máximo de sucursales']})
        # if branch_pk is not None:
        #     branches = branches.exclude(pk=branch_pk)
        # users_qty = reduce(
        #     lambda x, y: x + y, map((lambda x: x.max_users), branches), 0)
        # incoming_users = int(self.request.data.get("max_users"))
        # if (users_qty + incoming_users > company.max_users) and company.max_users != 0:
        #     raise ValidationError(detail={'max_users': ['Ha alcanzado su número máximo de usuarios']})

    def get_queryset(self, *args, **kwargs):
        try:
            company = int(self.kwargs['company_pk'])
        except ValueError:
            return Branches.objects.none()
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('traders', 'operators')).exists():
            return Branches.objects.filter(company=company).order_by('-id')
        elif self.request.user.branch.company.id == company:
            return Branches.objects.filter(company=company).order_by('-id')
        else:
            return Branches.objects.none()

    def create(self, request, *args, **kwargs):
        request.data['company'] = self.kwargs['company_pk']
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.can_save_branch(request)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        self.can_save_branch(request)
        request.data['company'] = self.kwargs['company_pk']
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
