# base
from django.urls import path, include
# rest framework
# from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers
# views
from .views.ResetPasswordViews import (
    ResetPasswordView,
)
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenVerifyView,
)
from .simple_jwt.tokenPairs import UserTokenObtainPairView
from .views.CompaniesViewSets import CompaniesViewSet, BranchesViewSet
from .views.UserViewSet import (
    UserViewSet,
    UserUpdatePassword,
    UserRenderInfo,
    UserSelfView,
    ActivateUserView,
    GroupsHierarchyViews,
    UserSelfPictureUpload
)
from .views.CompaniesReportsViews import CompaniesReports, BranchesReports, UsersReports
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet, GCMDeviceAuthorizedViewSet

app_name = 'management'

router = routers.SimpleRouter()
router.register('companies', CompaniesViewSet, base_name='companies')

branches_router = routers.NestedSimpleRouter(router, 'companies', lookup='company')
branches_router.register('branches', BranchesViewSet, base_name='company_branches')

user_router = routers.NestedSimpleRouter(router, 'companies', lookup='company')
user_router.register('users', UserViewSet, base_name='company_users')

urlpatterns = [
    path('companies/reports/', CompaniesReports.as_view(), name='companies_report'),
    path('companies/<int:company_pk>/branches/reports/', BranchesReports.as_view(), name='branches_report'),
    path('companies/<int:company_pk>/users/reports/', UsersReports.as_view(), name='users_report'),
    path('', include(router.urls)),
    path('', include(branches_router.urls)),
    path('', include(user_router.urls)),
    path('activate/', ActivateUserView.as_view(), name='activate_user'),
    path('user/', UserSelfView.as_view(), name='user_update_info'),
    path('user/picture/', UserSelfPictureUpload.as_view(), name='user_profile_picture'),
    path('login/', UserTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('password/', UserUpdatePassword.as_view(), name='change_password'),
    path('reset/password/', ResetPasswordView.as_view(), name='reset_password'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('render/', UserRenderInfo.as_view(), name='render_info'),
    path('render/groups/', GroupsHierarchyViews.as_view(), name='render_groups'),
    path('verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('notifications/gcm/', GCMDeviceAuthorizedViewSet.as_view({'post': 'create'}), name='create_gcm_device'),
    path('notifications/apns/', APNSDeviceAuthorizedViewSet.as_view({'post': 'create'}), name='create_apns_device'),
]
