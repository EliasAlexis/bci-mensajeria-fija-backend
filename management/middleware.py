import json
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from rest_framework_simplejwt import authentication
User = get_user_model()

class UserStatusMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_anonymous:
            if (request.path == '/login/' and request.method == 'POST'):
                if(len(request.POST) > 0):
                    data = request.POST
                else:
                    try:
                        data = json.loads(request.body.decode('utf8'))
                    except json.JSONDecodeError:
                        return self.get_response(request)
                try:
                    user = User.objects.get(username=data['username'])
                    if user.status == 'active' or user.status == 'disabled':
                        return self.get_response(request)
                    msg = 'El usuario {} se encuentra bloqueado'.format(user.username)
                    return JsonResponse({'username': [msg,]}, status=401)
                except (User.DoesNotExist, KeyError):
                    return self.get_response(request)
            else:
                return self.get_response(request)
        else:
            if request.user.status == 'active':
                return self.get_response(request)
            msg = 'El usuario {} se encuentra bloqueado'.format(request.user.username)
            msg_response = {'user': [msg,]}
            return JsonResponse(msg_response, status=401)
     
