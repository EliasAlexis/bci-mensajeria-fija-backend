# Generated by Django 3.0.3 on 2020-07-31 17:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0021_auto_20200715_1922'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuditEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.CharField(max_length=64)),
                ('ip', models.GenericIPAddressField(null=True)),
                ('username', models.CharField(max_length=256, null=True)),
            ],
        ),
    ]
