# django
from django.contrib.auth.models import Group, AnonymousUser
from django.contrib.auth import authenticate

# rest_framework
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.exceptions import PermissionDenied

# models
from ..models import User, SidebarMenuElems, Branches, GroupsHierarchy

# serializers
from .ResetPasswordSerializers import ConfirmPassword
from .CompaniesSerializers import BranchesSerializer

# etc
from utils.Utils import send_email_async
import random

class UserSerializer(serializers.ModelSerializer):
    group = serializers.CharField(default='users')

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'rut',
            'status',
            'phone',
            'email',
            'branch',
            'group',
            )

    def validate_group(self, value):  
        if self.instance is not None:
            company_type = self.instance.branch.company.company_type
        else:
            try:
                company_type = Branches.objects.get(
                    pk=self.initial_data.get("branch")
                ).company.company_type
            except Branches.DoesNotExist:
                raise serializers.ValidationError(detail="Sucursal Inválida")
        try:
            GroupsHierarchy.objects.get(
                child_group__name=value,
                company_type=company_type,
                parent_group=self.context.get('user').groups.first()
            )
        except GroupsHierarchy.DoesNotExist:
            raise serializers.ValidationError(detail="El tipo de usuario seleccionado no puede pertenecer a la empresa")
        return value

    def validate_status(self, value):
        if self.instance is not None:
            if self.instance.status == 'disabled' and value != 'disabled':
                return 'disabled'
                # raise ValidationError(detail={'status': ['El usuario está deshabilitado']})
        return value

    def create(self, validated_data):
        ascii_ch = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        group = validated_data.pop('group')
        validated_data["status"] = 'disabled'
        user = User(**validated_data)
        tmp_pwd = ''.join((random.choice(ascii_ch) for i in range(10)))
        user.set_password(tmp_pwd)
        inital_group = Group.objects.get(name=group)
        if group == 'admins':
            user.is_staff = True
        else:
            user.is_staff = False
        user.save()
        user.groups.add(inital_group)
        mail_data = UserSelfSerializer(user).data
        mail_data['tmp_pwd'] = tmp_pwd
        send_email_async.delay(
            template='welcome_mail.html',
            subject='Conexxion Bienvenido',
            to=[user.email,],
            email_data={'user': mail_data},
            cc=['logistica@conexxion.cl'] if user.branch.company.company_type == 'owner' else [],
        )
        return user

    def update(self, instance, validated_data, *args, **kwargs):
        group = validated_data.pop('group')
        super().update(instance, validated_data)
        instance.groups.clear()
        new_group = Group.objects.get(name=group)
        instance.groups.add(new_group)
        if group == 'admins':
            instance.is_staff = True
        else:
            instance.is_staff = False
        instance.save()
        return instance

    def to_representation(self, instance):
        user = super().to_representation(instance)
        user['branch'] = BranchesSerializer(instance.branch).data
        # print(instance.groups.all())
        user['group'] = instance.groups.first().name
        return user

class UserSelfSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'rut',
            'phone',
            'email',
            'branch',
            'profile_picture'
        )
        read_only_fields = ('branch', 'rut', 'username', 'email', 'profile_picture',)

    def to_representation(self, instance):
        user = super().to_representation(instance)
        
        user['branch'] = {
            'branch_id': instance.branch.id,
            'company_id': instance.branch.company.id,
            'name': instance.branch.name,
            'address': instance.branch.address
        }
        return user

class UserSelfPictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('profile_picture',)

class UserOperatorSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ('id', 'name', )
    
    def get_name(self, instance):
        return "{} {}".format(instance.first_name, instance.last_name)

class ChangePasswordSerializer(ConfirmPassword):

    old_password = serializers.CharField(
        required=True,
        allow_blank=False,
        trim_whitespace=True,
        min_length=8,
        max_length=50,
    )

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError(detail=['Las contraseñas no coinciden'])
        if not self.context['user'].check_password(data['old_password']):
            raise serializers.ValidationError(detail={'password_old': ['Las contraseña actual es errónea']})
        return data

class ActivateUserSerializer(ConfirmPassword):
    # old_password = serializers.CharField(
    #     required=True,
    #     allow_blank=False
    # )
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    rut = serializers.CharField()
    phone = serializers.CharField()
    email = serializers.EmailField()

    def validate_rut(self, value):
        user = self.context.get('user')
        if User.objects.exclude(pk=user.pk).filter(rut=value):
            raise serializers.ValidationError('Ya existe un usuario con este rut')
        return value

    def validate(self, data):
        user = self.context.get('user')
        if user.status != 'disabled':
            raise PermissionDenied
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError(detail=['Las contraseñas no coinciden'])
        # authenticated_user = authenticate(username=user.username, password=data['old_password'])
        # if authenticated_user is not None:
        return data
        # else:
            # raise serializers.ValidationError(detail=['Usuario o contraseña inválidos'])
    
    def create(self, validated_data):
        user = self.context.get('user')
        user.set_password(validated_data['password'])
        user.rut = validated_data['rut']
        user.status = 'active'
        user.save()
        serializer = UserSelfSerializer(user, validated_data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return user

class UserRenderInfoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = SidebarMenuElems
        fields = '__all__'

class GroupsHierarchySerializer(serializers.ModelSerializer):
    human_name = serializers.SerializerMethodField()
    HUMAN_GROUP_NAMES = {
        'admins': 'Administrador',
        'company_admins': 'Administrador de Empresa',
        'operators': 'Operario',
        'traders': 'Comercial',
        'deliverers': 'Repartidor',
        'users': 'Usuario',
        'mailmans': 'Cartero',
        'traders_supervisor': 'Supervisor de Comerciales',
    }

    def get_human_name(self, instance):
        return self.HUMAN_GROUP_NAMES.get(instance.name, instance.name)

    class Meta:
        model = Group
        fields = ('id', 'name', 'human_name',)

