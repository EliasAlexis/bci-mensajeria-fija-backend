from rest_framework import serializers
from django.forms.models import model_to_dict
from django.db import transaction
from ..models import Companies, Branches, TradersCompanies

from servicing.serializers.PricingSerializers import PricingSerializer
from servicing.models import Pricings

class CompanyTraderSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = TradersCompanies
        # fields = '__all__'
        fields = ('trader', 'first_name', 'last_name', 'email', 'phone')

    def get_first_name(self, instance):
        return "{} {}".format(
            instance.trader.first_name,
            instance.trader.last_name
        )

    def get_last_name(self, instance):
        return instance.trader.last_name

    def get_email(self, instance):
        return instance.trader.email

    def get_phone(self, instance):
        return instance.trader.phone

class CompanySerializer(serializers.ModelSerializer):
    trader = serializers.SerializerMethodField()

    class Meta:
        model = Companies
        fields = "__all__"
        read_only_fields = (
            "rut",
            "company_type",
            "max_users",
            "max_branches",
            "status",
            "name",
        )

    def get_trader(self, instance):
        if hasattr(instance, 'trader_company'):
            return CompanyTraderSerializer(instance.trader_company).data
        return None

class CompanyAdminSerializer(serializers.ModelSerializer):
    trader = CompanyTraderSerializer(source="trader_company", required=False)
    company_price = PricingSerializer(many=True)

    class Meta:
        model = Companies
        fields = (
            "id",
            "created_at",
            "name",
            "commercial_business",
            "rut",
            "company_type",
            "max_users",
            "max_branches",
            "phone",
            "contact_name",
            "email",
            "status",
            "billing_contacts",
            "extra_fields",
            "extra_fields_schema",
            "needs_signature",
            "needs_picture",
            "trader",
            "company_price",
        )
        read_only_fields = ('id',)
    
    def validate_trader(self, value):
        return value

    @transaction.atomic
    def update(self, instance, validated_data, *args, **kwargs):
        fields_to_skip = ('name', 'rut', 'trader_company')
        company_price = validated_data.pop('company_price')
        for field in validated_data:
            if field in fields_to_skip:
                continue
            setattr(instance, field, validated_data.get(field))
        instance.save()
        instance.trader_company.trader = validated_data.get("trader_company").get('trader')
        instance.trader_company.save()
        for el in company_price:
            el_id = el.get('id', None)
            if el_id:
                price = Pricings.objects.get(pk=el_id)
                price.no_return = el.get('no_return')
                price.with_return = el.get('with_return')
                price.save()
        return instance
    
    @transaction.atomic
    def create(self, validated_data):
        pricings_data = validated_data.pop('company_price')
        ret = super().create(validated_data)
        new_pricings = []
        for el in pricings_data:
            new_pricings.append(Pricings(company=ret, **el))
        Pricings.objects.bulk_create(new_pricings)
        return ret


class CompanyOperatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Companies
        fields = ('id', 'name', 'rut')
        read_only_fields = ('name', 'rut')

class BranchesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branches
        fields = "__all__"

    # def validate_max_users(self, value):
    #     if self.instance is not None:
    #         if self.instance.company.max_users != 0 and value == 0:
    #             raise serializers.ValidationError(
    #         "No es posible tener usuarios ilimitados en la sucursal")
    #     return value

class BranchesOperatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Companies
        fields = ('id', 'name')
