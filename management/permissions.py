from rest_framework import permissions
from rest_framework.exceptions import NotAuthenticated
from management.models import Branches, TradersCompanies

class UserStatusIsValid(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return True
        if request.user.status == 'locked':
            raise NotAuthenticated
        return True

class CanModifyCompanies(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        if request.user.groups.filter(name='traders').exists():
            return True
        if request.user.groups.filter(name='operators').exists():
            return request.method in permissions.SAFE_METHODS
        if request.user.has_perm('management.change_companies'):
            if any(k in view.kwargs for k in ['company_pk', 'pk']):
                if request.user.branch.company.id == int(view.kwargs.get('company_pk', view.kwargs.get('pk'))):
                    return True
                return request.method in permissions.SAFE_METHODS
            return request.method in permissions.SAFE_METHODS

class CanAddModifyUsers(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        elif request.user.groups.filter(name='operators').exists():
            return request.method in permissions.SAFE_METHODS
        elif request.user.groups.filter(name='company_admins').exists() \
            and Branches.objects.filter(
                company=request.user.branch.company,
                pk=request.data.get('branch')).exists():
            return True
        elif view.kwargs.get('company_pk'):
            if TradersCompanies.objects.filter(trader=request.user, company=view.kwargs.get('company_pk')).exists():
                return True
        else:
            return False