import random
from management.models import Companies, Branches
from random import randint
from faker import Faker

faker = Faker('es_ES')
conexxion = Companies(
    name='Conexxion',
    rut='76233769k',
    company_type='owner',
    max_users=100,
    max_branches=100,
    billing_contacts=[]
)
conexxion.save()
print('Conexxion created')

maSantander = Branches(
    name='Conexxion',
    address='Maria Luisa Santander 565',
    comune='Providencia',
    province='Santiago',
    region='Metropolitana',
    lat=0,
    lng=0,
    phone='+56226567900',
    contact_name='Javier Vallenilla',
    max_users=100,
    company=conexxion
)
maSantander.save()
print('Maria Luisa Santander created')

for i in range(1, 5):
    company = Companies.objects.create(
        name=faker.name(),
        rut=str(randint(10, 99)) + str(randint(100, 999))
                                 + str(randint(100, 999))
                                 + str(randint(0, 9)),
        company_type='client',
        max_users=randint(1, 99),
        max_branches=randint(1, 99),
        billing_contacts = [],
    )
    company.save()
print('Companies created')

for i in range(1, 100):
    branch = Branches(
        name=faker.name(),
        address=faker.address(),
        comune='Providencia',
        province='Santiago',
        lat=0,
        lng=0,
        region='Metropolitana',
        phone='+56' + random.choice(['2', '58', '51', '9'])
                    + str(randint(11111111, 99999999)),
        contact_name=faker.name(),
        max_users=randint(1, 99),
        company=Companies.objects.exclude(pk=1).order_by("?").first()
    )
    branch.save()
print('Branches created')
