import random
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from random import randint
from management.models import Branches, SidebarMenuElems, GroupsHierarchy
from guardian.shortcuts import assign_perm

User = get_user_model()

admin = User(
    username='admin',
    email='admin@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Admin Firstname',
    last_name='Admin Lastname',
    status='active',
    is_staff=True,
    is_superuser=True,
    branch=Branches.objects.get(pk=1),
)

admin.set_password('12345678')
admin.save()
print('Admin created')

operator = User(
    username='operator',
    email='operator@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Operator Firstname',
    last_name='Operator Lastname',
    status='active',
    branch=Branches.objects.get(pk=1),
)
operator.set_password('12345678')
operator.save()
print('Operator created')

trader = User(
    username='trader',
    email='trader@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Trader Firstname',
    last_name='Trader Lastname',
    status='active',
    branch=Branches.objects.get(pk=1),
)
trader.set_password('12345678')
trader.save()
print('Trader created')

companyAdm = User(
    username='company_adm',
    email='company_adm@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Company_adm Firstname',
    last_name='Company_adm Lastname',
    status='active',
    branch=Branches.objects.get(pk=2),
)
companyAdm.set_password('12345678')
companyAdm.save()
print('Company Admin created')

user = User(
    username='user',
    email='user@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='User Firstname',
    last_name='User Lastname',
    status='active',
    branch=Branches.objects.get(pk=2),
)
user.set_password('12345678')
user.save()
print('User created')

deliverer = User(
    username='moto',
    email='moto@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Moto Firstname',
    last_name='Moto Lastname',
    status='active',
    branch=Branches.objects.get(pk=1),
)
deliverer.set_password('12345678')
deliverer.save()
print('Moto created')

mailman = User(
    username='cartero',
    email='cartero@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='Cartero Firstname',
    last_name='Cartero Lastname',
    status='active',
    branch=Branches.objects.get(pk=1),
)
mailman.set_password('12345678')
mailman.save()
print('Cartero created')

userr = User(
    username='ouser',
    email='uoser@example.com',
    rut=(str(randint(10, 99)) + str(randint(100, 999))
                              + str(randint(100, 999))),
    phone='+56' + random.choice(['2', '58', '51', '9'])
                + str(randint(11111111, 99999999)),
    first_name='User Firstname',
    last_name='User Lastname',
    status='active',
    branch=Branches.objects.get(pk=23),
)
userr.set_password('12345678')
userr.save()
print('User created')

# Create admin group
admins, created = Group.objects.get_or_create(name='admins')
admin.groups.add(admins)

# Create operator group
operators, created = Group.objects.get_or_create(name='operators')
operator.groups.add(operators)

# Create trader group
traders, created = Group.objects.get_or_create(name='traders')
permissions = Permission.objects.filter(codename__in=['change_companies', 'view_companies', 'change_user', 'view_user'])
for perm in permissions:
    traders.permissions.add(perm)
trader.groups.add(traders)

# Create trader supervisor group
traders_supervisor, created = Group.objects.get_or_create(name='traders_supervisor')
permissions = Permission.objects.filter(codename__in=['change_companies', 'view_companies', 'change_user', 'view_user'])
for perm in permissions:
    traders_supervisor.permissions.add(perm)

# Create company admin group
companyAdmins, created = Group.objects.get_or_create(name='company_admins')

for perm in permissions:
    companyAdmins.permissions.add(perm)

companyAdm.groups.add(companyAdmins)

# Create user group
users, created =  Group.objects.get_or_create(name='users')
user.groups.add(users)
print('Groups created')

# Create deliverers group
deliverers, created = Group.objects.get_or_create(name='deliverers')
deliverer.groups.add(deliverers)

# Create mailmans group
mailmans, created = Group.objects.get_or_create(name='mailmans')
mailman.groups.add(mailmans)

GroupsHierarchy.objects.bulk_create([
    GroupsHierarchy(parent_group=admins, child_group=admins, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=companyAdmins, company_type='client'),
    GroupsHierarchy(parent_group=admins, child_group=operators, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=traders, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=deliverers, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=users, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=users, company_type='client'),
    GroupsHierarchy(parent_group=admins, child_group=mailmans, company_type='owner'),
    GroupsHierarchy(parent_group=admins, child_group=traders_supervisor, company_type='owner'),
    # GroupsHierarchy(parent_group=companyAdmins, child_group=companyAdmins, company_type='client'),
    GroupsHierarchy(parent_group=companyAdmins, child_group=users, company_type='client'),
    # GroupsHierarchy(parent_group=traders, child_group=companyAdmins, company_type='client'),
    GroupsHierarchy(parent_group=traders, child_group=users, company_type='client'),
    GroupsHierarchy(parent_group=operators, child_group=deliverers, company_type='owner'),
    GroupsHierarchy(parent_group=operators, child_group=mailmans, company_type='owner'),
])
# Admin URIS

admin_companies_parent = SidebarMenuElems(
    path='/adm/companies',
    title='Empresas',
    css_class='has-arrow',
    icon='mdi mdi-domain'
    )
admin_companies_parent.save()
assign_perm('view_sidebarmenuelems', admins, admin_companies_parent)

admin_companies = SidebarMenuElems(
    path='/adm/companies',
    title='Listado Empresas',
    icon='mdi mdi-domain',
    parent_available=True,
    css_class='',
    parent=admin_companies_parent
)
admin_companies.save()
assign_perm('view_sidebarmenuelems', admins, admin_companies)

userElem = SidebarMenuElems(
    path='/adm/companies/files',
    title='Ficha Empresas',
    icon='mdi mdi-file-document',
    css_class='',
    parent_available=True,
    parent=admin_companies_parent
)
userElem.save()
assign_perm('view_sidebarmenuelems', admins, userElem)


admin_settings_parent = SidebarMenuElems(
    path='/adm/settings',
    title='Configuraciones',
    css_class='has-arrow',
    icon='mdi mdi-settings'
    )
admin_settings_parent.save()
assign_perm('view_sidebarmenuelems', admins, admin_settings_parent)

admin_segments = SidebarMenuElems(
    path='/adm/settings/segments',
    title='Tramos',
    icon='mdi mdi-folder',
    parent_available=True,
    css_class='',
    parent=admin_settings_parent
)
admin_segments.save()
assign_perm('view_sidebarmenuelems', admins, admin_segments)

admin_services = SidebarMenuElems(
    path='/adm/settings/services',
    title='Servicios',
    icon='mdi mdi-folder',
    parent_available=True,
    css_class='',
    parent=admin_settings_parent
)
admin_services.save()
assign_perm('view_sidebarmenuelems', admins, admin_services)

admin_products = SidebarMenuElems(
    path='/adm/settings/products',
    title='Productos',
    icon='mdi mdi-folder',
    parent_available=True,
    css_class='',
    parent=admin_settings_parent
)
admin_products.save()
assign_perm('view_sidebarmenuelems', admins, admin_products)

admin_transports = SidebarMenuElems(
    path='/adm/settings/transports',
    title='Vehículos',
    icon='mdi mdi-car',
    parent_available=True,
    css_class='',
    parent=admin_settings_parent
)
admin_transports.save()
assign_perm('view_sidebarmenuelems', admins, admin_transports)

admin_reasons = SidebarMenuElems(
    path='/adm/settings/reasons',
    title='Razones de Fallidos',
    icon='mdi mdi-comment-text',
    parent_available=True,
    css_class='',
    parent=admin_settings_parent
)
admin_reasons.save()
assign_perm('view_sidebarmenuelems', admins, admin_reasons)

admin_pre_billing = SidebarMenuElems(
    path='/adm/pre/',
    title='Prefacturación',
    icon='mdi mdi-file',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
admin_pre_billing.save()
assign_perm('view_sidebarmenuelems', admins, admin_pre_billing)

# Trader URIS
trader_companies = SidebarMenuElems(
    path='/trader/companies',
    title='Listado Empresas',
    icon='mdi mdi-domain',
    css_class='',
)
trader_companies.save()
assign_perm('view_sidebarmenuelems', traders, trader_companies)
assign_perm('view_sidebarmenuelems', traders_supervisor, trader_companies)

trader_files = SidebarMenuElems(
    path='/trader/files',
    title='Ficha Empresas',
    icon='mdi mdi-file-document',
    css_class='',
)
trader_files.save()
assign_perm('view_sidebarmenuelems', traders, trader_files)
assign_perm('view_sidebarmenuelems', traders_supervisor, trader_files)

trader_list = SidebarMenuElems(
    path='/trader/wolist/',
    title='Listado de Solicitudes',
    icon='mdi mdi-view-list',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
trader_list.save()
assign_perm('view_sidebarmenuelems', traders, trader_list)
assign_perm('view_sidebarmenuelems', traders_supervisor, trader_list)

# company_admins URIS

company_admin_branches = SidebarMenuElems(
    path='/supervisor/companies/%company_id/',
    title='Sucursales',
    icon='mdi mdi-domain',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
company_admin_branches.save()
assign_perm('view_sidebarmenuelems', companyAdmins, company_admin_branches)

company_admin_contacts = SidebarMenuElems(
    path='/supervisor/contacts/',
    title='Libreta de Direcciones',
    icon='mdi mdi-account-box',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
company_admin_contacts.save()
assign_perm('view_sidebarmenuelems', companyAdmins, company_admin_contacts)

company_admin_list = SidebarMenuElems(
    path='/supervisor/wolist/',
    title='Listado de Solicitudes',
    icon='mdi mdi-view-list',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
company_admin_list.save()
assign_perm('view_sidebarmenuelems', companyAdmins, company_admin_list)

# User URIS
user_work_order = SidebarMenuElems(
    path='/users/solicitudot/new/',
    title='Nueva solicitud',
    icon='mdi mdi-motorbike',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
user_work_order.save()
assign_perm('view_sidebarmenuelems', users, user_work_order)
assign_perm('view_sidebarmenuelems', companyAdmins, user_work_order)

user_work_orders_list = SidebarMenuElems(
    path='/users/wolist/',
    title='Listado de solicitudes',
    icon='mdi mdi-view-list',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
user_work_orders_list.save()
assign_perm('view_sidebarmenuelems', users, user_work_orders_list)

# Operator URIS
operator_dashboard = SidebarMenuElems(
    path='/operator/dashboard/',
    title='Dashboard',
    icon='mdi mdi-view-dashboard',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
operator_dashboard.save()
assign_perm('view_sidebarmenuelems', operators, operator_dashboard)

operator_list = SidebarMenuElems(
    path='/operator/wolist/',
    title='Dashboard',
    icon='mdi mdi-view-list',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
operator_list.save()
assign_perm('view_sidebarmenuelems', operators, operator_list)

operator_wo = SidebarMenuElems(
    path='/operator/solicitudot/new/',
    title='Nueva Solicitud',
    icon='mdi mdi-motorbike',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
operator_wo.save()
assign_perm('view_sidebarmenuelems', operators, operator_wo)

falab_upload = SidebarMenuElems(
    path='/operator/falabella/',
    title='Carga Falabella',
    icon='mdi mdi-file-delimited',
    # parent_available=True,
    css_class='',
    # parent=company_admin_company_parent
)
falab_upload.save()
assign_perm('view_sidebarmenuelems', operators, falab_upload)